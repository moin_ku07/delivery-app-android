package com.devtoyou.devtoyou.Fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.devtoyou.devtoyou.Activities.TabMenuActivity;
import com.devtoyou.devtoyou.Adapters.StoreItemsListViewAdapter;
import com.devtoyou.devtoyou.Adapters.StorePhotosAdapter;
import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.Store;
import com.devtoyou.devtoyou.models.StoreItem;
import com.devtoyou.devtoyou.models.StorePhoto;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by motaz on 11/5/15.
 */
public class AllStoresFragment extends Fragment {
    public View view;
    private GridAdapter adapter;
    private StoreItemsListViewAdapter listadapter;
    GridView gridView;
    ListView listView;
    ProgressDialog loadingProgressDialog;
    public TwoWayView storePhotosList;

    public BroadcastReceiver receiver;
    public int lastClickedPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new AllStoresFragmentBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_STORES_DONE);
        filter.addAction(Actions.GET_ALL_STORE_ITEMS_DONE);
        filter.addAction(Actions.GET_STORE_PHOTOS_DONE);
        getActivity().getApplicationContext().registerReceiver(receiver, filter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.all_store_fragment, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (AppData.getInstance(getActivity()).allStores != null)
            redraw();
        return view;
    }

    private void redraw() {
        if (AppData.getInstance(getActivity()).allStores != null && view != null) {
            gridView = (GridView) view.findViewById(R.id.gridview);
            adapter = new GridAdapter(getActivity(), AppData.getInstance(getActivity()).allStores);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (AppData.getInstance(getActivity()).allStoresItems != null) {
                        showStoreItems(position);
                    } else {
                        lastClickedPosition = position;
                        loadingProgressDialog = ProgressDialog.show(getActivity(), "", "Loading store items", true);
                    }
                }


            });
        }
    }


    public void showStoreItems(int position) {
        Context context = getActivity();
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_screen_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Store chosenStore = AppData.getInstance(getActivity()).allStores[position];

        ImageView headerImg = (ImageView) dialog.findViewById(R.id.headerImg);

        if (chosenStore.getThumb() != null) {
            headerImg.setBackgroundResource(0);
            Picasso.with(getActivity().getApplicationContext()).load(chosenStore.getThumb()).into(headerImg);
        }
        TextView storeName = (TextView) dialog.findViewById(R.id.name);
        storeName.setText(chosenStore.getName());

        LinearLayout storePhotos = (LinearLayout) dialog.findViewById(R.id.store_photos_layout);
        String userType = AppData.getInstance(getActivity()).user_type;

        storePhotos.setVisibility(View.GONE);
        if (userType != null && userType.equals("store_owner")) {
            if (AppData.getInstance(getActivity().getApplicationContext()).store_id == chosenStore.getId()) {
                storePhotos.setVisibility(View.VISIBLE);
                storePhotosList = (TwoWayView) dialog.findViewById(R.id.store_photos_list_view);
                storePhotosList.setVisibility(View.VISIBLE);
                Intent i = new Intent(getActivity(), AppNetworkService.class);
                i.setAction(Actions.GET_STORE_PHOTOS);
                i.putExtra(Constants.STORE_ID, AppData.getInstance(getActivity()).store_id);
                getActivity().startService(i);
            }
        }

        Button cross = (Button) dialog.findViewById(R.id.cross);
        TabMenuActivity.halfSizingButton(getActivity(), cross, R.drawable.cross_home, 20, 20, "top");
        cross.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        listView = (ListView) dialog.findViewById(R.id.list);
        listadapter = new StoreItemsListViewAdapter(dialog.getContext(), getStoreItems(chosenStore.getId()));
        listView.setAdapter(listadapter);
    }

    public class GridAdapter extends ArrayAdapter<String> {
        private Context context;
        //        private final String[] values;
//        private int[] images;
        private Store[] stores;
        private TextView text;
        private ImageView img;
        public ProgressBar progressBar;

        public GridAdapter(Context context, Store[] stores) {
            super(context, R.layout.grid_row);
            if (context != null && stores != null && stores.length > 0) {
                this.context = context;
                this.stores = stores;
            }
        }

        @Override
        public int getCount() {
            return stores.length;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.grid_row, parent, false);
            text = (CustomPlainTextView) rowView.findViewById(R.id.text);
            img = (ImageView) rowView.findViewById(R.id.image);

            text.setText(stores[position].getName());
            progressBar = (ProgressBar) rowView.findViewById(R.id.image_progress_bar);

            if (convertView == null)
                Picasso.with(getContext()).load(stores[position].getThumb()).into(img, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                        img.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        Log.d("Error in loading an image", "here");
                    }
                });
            else {
                Picasso.with(getContext()).load(stores[position].getThumb()).into(img);
            }
            return rowView;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Exception e) {
            Log.d("onDestroy Error: ", e.toString());
        }
    }

    public StoreItem[] getStoreItems(int storeId) {
        HashMap<Integer, CartItem> cartItemsMap = new HashMap<>();
        ArrayList<CartItem> cartItemsList = AppData.getInstance(getActivity()).cartItems;
        for (int i = 0; i < cartItemsList.size(); i++) {
            cartItemsMap.put(cartItemsList.get(i).getStoreItem(), cartItemsList.get(i));
        }
        List<StoreItem> storeItems = new ArrayList<>();
        for (StoreItem storeItem : AppData.getInstance(getActivity()).allStoresItems) {
            if (storeItem.getStore() == storeId) {
                if (cartItemsMap.containsKey(storeItem.getId())) {
                    storeItem.inCart = true;
                }
                storeItems.add(storeItem);
            }
        }
        return storeItems.toArray(new StoreItem[storeItems.size()]);
    }

    private void drawStorePhotos(StorePhoto[] storePhotosArray) {
        if (storePhotosList != null && storePhotosArray != null) {
            StorePhotosAdapter adapter = new StorePhotosAdapter(getActivity(), 0, storePhotosArray);
            storePhotosList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public class AllStoresFragmentBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_STORES_DONE)) {
                AppData appData = AppData.getInstance(getActivity());
                if (appData != null && appData.allStores != null && appData.allStores.length > 0)
                    redraw();
            } else if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
                    loadingProgressDialog.cancel();
                    showStoreItems(lastClickedPosition);
                }
            } else if (action.equals(Actions.GET_STORE_PHOTOS_DONE)) {
                String storePhotos = AppCache.cache.get(Actions.GET_STORE_PHOTOS);
                StorePhoto storePhotosArray[] = new Gson().fromJson(storePhotos, StorePhoto[].class);
                drawStorePhotos(storePhotosArray);
            }
        }
    }

}
