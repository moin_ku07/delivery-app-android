package com.devtoyou.devtoyou.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.devtoyou.devtoyou.Adapters.PopularItemsHorizontalListArrayAdapter;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Utils.ArabicNumberFormatter;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.FavoriteItem;
import com.squareup.picasso.Picasso;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by motaz on 11/1/15.
 */
public class PopularItemsFragment extends Fragment {
    public View view;
    public TwoWayView popularItemsHorizontalList;
    public ImageView favorite_item_details_image;
    public TextView favoriteItemName;
    public TextView favoriteItemStoreName;
    public RatingBar favoriteItemRating;
    public TextView favoriteItemDetails;
    public TextView favoriteItemDetailsPrice;
    public Button favoriteItemAddToCart;
    public int index;
    public PopularItemsBroadCastReceiver receiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new PopularItemsBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_FAVORITE_ITEMS);
        filter.addAction(Actions.SHOW_FAVORITE_ITEM_DETAILS);
        filter.addAction(Actions.SHOW_FAVORITE_ITEMS_FRAGMENT);
        filter.addAction(Actions.GET_ALL_FAVORITE_ITEMS_DONE);
        filter.addAction(Actions.REMOVE_ITEM_FROM_CART);
        getActivity().registerReceiver(receiver, filter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.popular_items_fragment, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        popularItemsHorizontalList = (TwoWayView) view.findViewById(R.id.popular_items_horizontal_list_view);
        favorite_item_details_image = (ImageView) view.findViewById(R.id.favorite_item_details_image);
        favoriteItemName = (TextView) view.findViewById(R.id.favorite_item_name);
        favoriteItemStoreName = (TextView) view.findViewById(R.id.favorite_item_store_name);
        favoriteItemRating = (RatingBar) view.findViewById(R.id.favorite_item_rating);
        favoriteItemDetails = (TextView) view.findViewById(R.id.favorite_item_details);
        favoriteItemDetailsPrice = (TextView) view.findViewById(R.id.favorite_item_details_price);
        favoriteItemAddToCart = (Button) view.findViewById(R.id.favorite_item_add_to_cart);

        redraw();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        redraw();
    }

    private void redraw() {
        if (AppData.getInstance(getActivity().getApplicationContext()).favoriteItems != null) {
            updateFavoriteItemIfItWasAddedToCartBefore();
            PopularItemsHorizontalListArrayAdapter adapter =
                    new PopularItemsHorizontalListArrayAdapter(getActivity(), 0, AppData.getInstance(getActivity().getApplicationContext()).favoriteItems);
            popularItemsHorizontalList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            updateItemDetails(index);
            favoriteItemAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FavoriteItem favoriteItem = AppData.getInstance(getActivity()).favoriteItems[index];
                    if (favoriteItem != null && !favoriteItem.inCart) {
                        CartItem cartItem = new CartItem();
                        cartItem.setStoreItem(favoriteItem.getId());
                        cartItem.setPrice(favoriteItem.getPrice());
                        cartItem.setQuantity(1);

                        favoriteItem.inCart = true;

                        AppData.getInstance(getActivity()).cartItems.add(cartItem);

                        Intent i = new Intent(Actions.ADD_ITEM_TO_CART);
                        getActivity().sendBroadcast(i);
                        favoriteItemAddToCart.setText("مضاف للسلة");
                    }
                }
            });
        } else {
            AppData.getInstance(getActivity().getApplicationContext()).getFavouriteItems();
        }
    }

    private void updateFavoriteItemIfItWasAddedToCartBefore() {
        HashMap<Integer, CartItem> cartItemsMap = new HashMap<>();
        ArrayList<CartItem> cartItemArrayList = AppData.getInstance(getActivity()).cartItems;
        if (cartItemArrayList != null)
            for (int i = 0; i < cartItemArrayList.size(); i++) {
                cartItemsMap.put(cartItemArrayList.get(i).getStoreItem(), cartItemArrayList.get(i));
            }
        FavoriteItem favoriteItems[] = AppData.getInstance(getActivity()).favoriteItems;
        for (int i = 0; i < favoriteItems.length; i++) {
            FavoriteItem item = favoriteItems[i];
            if (cartItemsMap.containsKey(item.getId())) {
                item.inCart = true;
            } else {
                item.inCart = false;
            }
        }
    }

    public class PopularItemsBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_FAVORITE_ITEMS_DONE)) {
                redraw();
                updateItemDetails(0);
            } else if (action.equals(Actions.SHOW_FAVORITE_ITEM_DETAILS)) {
                index = intent.getIntExtra(Constants.FAVORITE_ITEM_POSITION, 0);
                updateItemDetails(index);
            } else if (action.equals(Actions.SHOW_FAVORITE_ITEMS_FRAGMENT)) {
                AppData.getInstance(getActivity().getApplicationContext()).getFavouriteItems();
            } else if (action.equals(Actions.REMOVE_ITEM_FROM_CART)) {
                redraw();
            }
        }
    }

    private void updateItemDetails(int i) {
        FavoriteItem favoriteItem = AppData.getInstance(getActivity()).favoriteItems[i];
        if (favoriteItem != null) {
            Picasso.with(getActivity()).load(favoriteItem.getPhoto()).into(favorite_item_details_image);
            favoriteItemName.setText(favoriteItem.getName());
            favoriteItemStoreName.setText(favoriteItem.getStoreName());
            favoriteItemRating.setRating(favoriteItem.getAvgRating());
            favoriteItemDetails.setText(favoriteItem.getDescription());
            favoriteItemDetailsPrice.setText(ArabicNumberFormatter.formatNumberInArabic(favoriteItem.getPrice()));
            if (favoriteItem.inCart) {
                favoriteItemAddToCart.setText("مضاف للسلة");
            } else {
                favoriteItemAddToCart.setText("اضافه للسلة");
            }
            index = i;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }
}
