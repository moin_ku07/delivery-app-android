package com.devtoyou.devtoyou.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Activities.AccountSettingsActivity;
import com.devtoyou.devtoyou.Activities.NavigatorActivity;
import com.devtoyou.devtoyou.Activities.HtmlViewActivity;
import com.devtoyou.devtoyou.Activities.RegisterActivity;
import com.devtoyou.devtoyou.Activities.StoreItemsActivity;
import com.devtoyou.devtoyou.Activities.UserDashboardActivity;
import com.devtoyou.devtoyou.CustomViews.CircleTransform;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.UserHistoryActivty;
import com.devtoyou.devtoyou.Activities.ViewAddressActivty;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.User;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    private TextView accSettings, title;
    private ListView list;
    private ListAdapter listAdapter;
    private ImageView profileImage;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    JSONObject authObj;
    private OnFragmentInteractionListener mListener;
    Dialog loading;
    AlertDialog.Builder alert;
    TextView userName, phoneNum, email, balance, loyalty, header2;
    public MyProfileFragmentReceiver receiver;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.UPDATE_PROFILE_IMAGE_DONE);
        filter.addAction(Actions.GET_USER_INFO_DONE);
        receiver = new MyProfileFragmentReceiver();
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        String auth = RegisterActivity.readData("auth", getActivity());
        try {
            authObj = new JSONObject(auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        loading = RegisterActivity.loadingView(getActivity());
        alert = new AlertDialog.Builder(getActivity());
        list = (ListView) rootView.findViewById(R.id.listView);
        String[] values = new String[]{"عن برنامج الولاء", "لوحه المعلومات", "سجل عملياتي", "عرض العنوان", "اعدادات الحساب"};
        String[] store_user_values = new String[]{"عن برنامج الولاء", "قائمة المنتجات", "لوحه المعلومات", "سجل المتجر", "عرض العنوان", "اعدادات الحساب"};
        String user_type = AppData.getInstance(getActivity()).user_type;
        if (user_type != null && user_type.equals("store_owner")) {
            listAdapter = new ListAdapter(getActivity(), store_user_values);
        } else {
            listAdapter = new ListAdapter(getActivity(), values);
        }
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.profile_footer, list,
                false);

        Button logout = (Button) footer.findViewById(R.id.logoutBtn);
        logout.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          RegisterActivity.removeData(getActivity(), "auth");
                                          AppData.getInstance(getActivity()).user = null;
                                          AppData.getInstance(getActivity()).user_type = "";
                                          AppData.getInstance(getActivity()).user_id = 0;
                                          AppData.getInstance(getActivity()).store_id = 0;
                                          startActivity(new Intent(getActivity().getApplicationContext(), NavigatorActivity.class
                                          ));
                                      }
                                  }
        );
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.profile_header, list,
                false);
        Button pic = (Button) header.findViewById(R.id.picBtn);

        userName = (TextView) header.findViewById(R.id.username);
        phoneNum = (TextView) header.findViewById(R.id.phnNo);
        email = (TextView) header.findViewById(R.id.email);
        balance = (TextView) header.findViewById(R.id.balance);
        loyalty = (TextView) header.findViewById(R.id.loyalty_amount);
        header2 = (TextView) header.findViewById(R.id.header2);

        profileImage = (ImageView) header.findViewById(R.id.proImg);
        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageCaptureDialog();
            }
        });

        dataLoad();
        list.addHeaderView(header);
        list.addFooterView(footer);
        list.setAdapter(listAdapter);
        String token = null;
        //SharedPreferences pref = getActivity().getPreferences(0);
        //String userType = pref.getString("user_type", "empty");
        Log.d("****User Type new******", AppData.getInstance(getActivity()).user_type);
        String getURL = RegisterActivity.rootURL(getActivity()) + "auth/profile";
        Log.d("***URL", getURL);
        try {
            token = "Token " + authObj.getString("auth_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (AppData.getInstance(getActivity()).user_type.equals("customer") || AppData.getInstance(getActivity()).user_type.equals("driver")) {
            Ion.with(getActivity()).load("GET", getURL).setHeader("Authorization", token)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            // TODO
                            if (result != null) {
                                Log.d("*****Balance*****", result.toString());
                                if (result.get("balance").toString().equals("null")) {
                                    balance.setText("0.00");
                                } else {
                                    balance.setText(result.get("balance").toString());
                                }
                                if (result.get("loyalty_amount") != null) {
                                    loyalty.setText(result.get("loyalty_amount").toString());
                                } else {
                                    loyalty.setText("0.0");
                                }
                            }


                        }
                    });
        }

        if (AppData.getInstance(getActivity()).user_type.equals("store_owner")) {
            int storeId = AppData.getInstance(getActivity()).store_id;
            Log.d("*******LOG URL", String.valueOf(storeId));
            getURL = RegisterActivity.rootURL(getActivity()) + "api/stores/" + storeId + "/?include_credit=true";

            Ion.with(getActivity()).load("GET", getURL).setHeader("Authorization", token)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result.get("store_balance").toString().equals("null")) {
                                balance.setText("0.00");
                            } else {
                                balance.setText(result.get("store_balance").toString());
                            }
                            header2.setVisibility(View.GONE);
                            loyalty.setVisibility(View.GONE);
                        }

                    });
        }

        return rootView;
    }

    private void dataLoad() {
        User user = AppData.getInstance(getActivity()).user;
        Log.d("*****User***", String.valueOf(user));
        if (user != null) {
            String first_last_name = "";
            if (user.getFirstName() != null && !user.getFirstName().isEmpty() && !user.getFirstName().equals("N/A")) {
                userName.setVisibility(View.VISIBLE);
                first_last_name = user.getFirstName();
            } else {
                userName.setVisibility(View.GONE);
            }
            if (user.getLastName() != null && !user.getLastName().isEmpty() && !user.getLastName().equals("N/A")) {
                if (first_last_name != null && !first_last_name.equals(""))
                    first_last_name += " ";
                first_last_name += user.getLastName();
            }
            if (first_last_name != null && !first_last_name.equals("")) {
                userName.setText(first_last_name);
            }

            if (user.getUsername() != null && !user.getUsername().isEmpty() && !user.getUsername().equals("N/A")) {
                phoneNum.setVisibility(View.VISIBLE);
                phoneNum.setText(user.getUsername());
            } else {
//                phoneNum.setText("");
                phoneNum.setVisibility(View.GONE);
            }
            if (user.getEmail() != null && !user.getEmail().isEmpty() && !user.getEmail().equals("N/A")) {
                email.setVisibility(View.VISIBLE);
                email.setText(user.getEmail());
            } else {
                email.setVisibility(View.GONE);
//                email.setText("");
            }

        }
        try {
            if (user.getPhoto() != null) {
                Log.d("User Photo: ", user.getPhoto());
                Picasso.with(getActivity()).load(user.getPhoto()).transform(new CircleTransform()).into(profileImage);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean nullChecking(JsonObject jsonObject, String name) {
        if (jsonObject.get(name).getAsString().equals("") || jsonObject.get(name).getAsString().equals("null") || jsonObject.get(name).getAsString() == null || jsonObject.get(name).getAsString().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String user_type = AppData.getInstance(getActivity()).user_type;
        if (!user_type.equals("store_owner"))
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 1) {
                        startActivity(new Intent(getActivity(), HtmlViewActivity.class));
                    } else if (position == 2) {
                        startActivity(new Intent(getActivity(), UserDashboardActivity.class));
                    } else if (position == 3) {
                        startActivity(new Intent(getActivity(), UserHistoryActivty.class));
                    } else if (position == 4) {
                        startActivity(new Intent(getActivity(), ViewAddressActivty.class));
                    } else if (position == 5) {
                        startActivity(new Intent(getActivity(), AccountSettingsActivity.class));
                    }
                }
            });
        else
            list.setOnItemClickListener(storeOnItemClickListener);
    }


    AdapterView.OnItemClickListener storeOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position == 1) {
                startActivity(new Intent(getActivity(), HtmlViewActivity.class));
            } else if (position == 2) {
                startActivity(new Intent(getActivity(), StoreItemsActivity.class));
            } else if (position == 3) {
                startActivity(new Intent(getActivity(), UserDashboardActivity.class));
            } else if (position == 4) {
                startActivity(new Intent(getActivity(), UserHistoryActivty.class));
            } else if (position == 5) {
                startActivity(new Intent(getActivity(), ViewAddressActivty.class));
            } else if (position == 6) {
                startActivity(new Intent(getActivity(), AccountSettingsActivity.class));
            }
        }
    };



    /* Show alert to select / take images*/

    private void showImageCaptureDialog() {

        String title = "Open Photo";
        CharSequence[] itemlist = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, 1);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                        startActivityForResult(intent, 1);
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
                        Intent intent2 = new Intent(Intent.ACTION_PICK);
                        intent2.setType("image/*");
                        intent2.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent2,
                                "Select Picture"), 2);
                        break;
                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap adjustedBitmap = null;
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("#######resultCode:", String.valueOf(resultCode));
        if (resultCode == -1) {
            if (data != null) {
                Bitmap selectedBitmap = null;
                if (requestCode == 1) {
                    Bundle extras = data.getExtras();
                    selectedBitmap = extras.getParcelable("data");
                } else if (requestCode == 2) {
                    try {
                        selectedBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        Log.d("#######selectedBitmap:", String.valueOf(selectedBitmap));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (selectedBitmap != null) {
                    adjustedBitmap = ThumbnailUtils.extractThumbnail(selectedBitmap,
                            100, 100);


                    ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
                    // path to /data/data/yourapp/app_data/imageDir
                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                    // Create imageDir
                    long millis = System.currentTimeMillis() % 1000;
                    String imageName = "profileImg" + millis + ".png";
                    File mypath = new File(directory, imageName);

                    adjustedBitmap = RotateBitmap(adjustedBitmap, 90);
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(mypath);

                        // Use the compress method on the BitMap object to write image to
                        // the OutputStream
                        adjustedBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Picasso.with(getActivity()).load(mypath).transform(new CircleTransform()).into(profileImage);
                    image_upload(mypath);
                }
            }
        }
    }

    private void image_upload(final File file) {

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {

        loading.show();
        String token = null;
        try {
            String imageChangeUrl = RegisterActivity.rootURL(getActivity()) + authObj.getString("user_type") + "/" + authObj.getString("id");
            token = "Token " + authObj.getString("auth_token");
            Log.e("photo image ", file + "");
            if (RegisterActivity.netWorkChecking(getActivity())) {
                Intent i = new Intent(getActivity(), AppNetworkService.class);
                i.setAction(Actions.UPDATE_PROFILE_IMAGE);
                i.putExtra("photo", file + "");
                getActivity().startService(i);
            } else {
                loading.dismiss();
                alert.setTitle("Alert");
                alert.setMessage("No Network Connection Available. Please Connect to Internet to use TCL App!");
                alert.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class ListAdapter extends ArrayAdapter<String[]> {
        public View.OnClickListener listener;
        private final Context context;
        private String[] values;


        public ListAdapter(Context context, String[] values) {
            super(context, R.layout.profile_row);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.profile_row, parent, false);
            TextView text = (TextView) rowView.findViewById(R.id.text);
            text.setText(values[position]);
            return rowView;
        }

    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public class MyProfileFragmentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.UPDATE_PROFILE_IMAGE_DONE)) {
                loading.dismiss();
                /*ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                // Create imageDir
                long millis = System.currentTimeMillis() % 1000;
                String imageName = "profileImg.png?"+ millis;
                File mypath = new File(directory, imageName);
                Picasso.with(getActivity()).load(mypath).transform(new CircleTransform()).into(profileImage);*/
                dataLoad();
            } else if (action.equals(Actions.GET_USER_INFO_DONE)) {
                dataLoad();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }
}
