package com.devtoyou.devtoyou.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Activities.PlaceOrderActivity;
import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.ViewAddressActivty;
import com.devtoyou.devtoyou.Utils.ArabicNumberFormatter;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.StoreItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CartFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ListView lv;
    private View rootView;
    private ListAdapter listadapter;
    String[] values = {"abc", "def", "ghi"};
    public View footer;
    private OnFragmentInteractionListener mListener;
    public CartFragmentBroadCastReceiver receiver;
    public TextView emptyMsg;
    public boolean emptyFlag;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     *               gg* @return A new instance of fragment PlaceOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        receiver = new CartFragmentBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.ADD_ITEM_TO_CART);
        filter.addAction(Actions.PLACE_ORDER_DONE);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.cart, container, false);
        lv = (ListView) rootView.findViewById(R.id.list);
        footer = inflater.inflate(R.layout.cart_footer, null);
        emptyMsg = (TextView) rootView.findViewById(R.id.empty_msg);
        emptyMsg.setVisibility(View.GONE);

        ArrayList<CartItem> cartItems = AppData.getInstance(getActivity()).cartItems;
        if (cartItems != null && cartItems.size() != 0)
            redraw();
        else
            hideListView();

        return rootView;
    }

    private void hideListView() {
        lv.setVisibility(View.GONE);
        emptyMsg.setVisibility(View.VISIBLE);
    }

    private void changeQuantity() {
        TextView totalPrice = (TextView) footer.findViewById(R.id.total_price);
//        NumberFormat format = new DecimalFormat("#.00");
        totalPrice.setText(ArabicNumberFormatter.formatNumberInArabic(getTotalPrice()));
    }

    private void redraw() {
        List<CartItem> cartItems = AppData.getInstance(getActivity()).cartItems;
        if (cartItems.size() > 0) {
            lv.setVisibility(View.VISIBLE);
            emptyMsg.setVisibility(View.GONE);


            listadapter = new ListAdapter(getActivity(), cartItems.toArray(new CartItem[cartItems.size()]));
            lv.setAdapter(listadapter);
            listadapter.notifyDataSetChanged();

            if (footer != null)
                lv.removeFooterView(footer);
            lv.addFooterView(footer);

            TextView totalPrice = (TextView) footer.findViewById(R.id.total_price);
//            NumberFormat format = new DecimalFormat("#.00");
            totalPrice.setText(ArabicNumberFormatter.formatNumberInArabic(getTotalPrice()));

            Button placeOrderBtn = (Button) footer.findViewById(R.id.orderBtn);
            placeOrderBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkLocationFoundOrNot()) {
                        Intent i = new Intent(getActivity(), PlaceOrderActivity.class);
                        startActivity(i);
                    } else {
                        View titleView = LayoutInflater.from(getContext()).inflate(R.layout.no_location_alert, null, false);
                        titleView.setMinimumHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics()));
                        AlertDialog alertDialog = new AlertDialog.Builder(
                                getActivity()).setCustomTitle(titleView).create();
//                        alertDialog.setTitle("تحذير");
//                        alertDialog.setContentView(R.layout.no_location_alert);
                        alertDialog.setMessage("الرجاء اضافة عنوانك قبل اكمال الطلب.");
                        alertDialog.setButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AppData.getInstance(getActivity()).from_place_order_flag = true;
                                Intent i = new Intent(getActivity(), ViewAddressActivty.class);
                                startActivity(i);
                            }
                        });
                        alertDialog.show();
                    }
                }
            });

        } else {
            lv.setVisibility(View.INVISIBLE);
            emptyMsg.setVisibility(View.VISIBLE);
            emptyFlag = true;
        }
    }

    private double getTotalPrice() {
        double total_price = 0;
        List<CartItem> cartItems = AppData.getInstance(getActivity()).cartItems;
        for (CartItem cartItem : cartItems) {
            total_price += cartItem.getPrice() * cartItem.getQuantity();
        }
        return total_price;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public class ListAdapter extends ArrayAdapter<String[]> {
        public View.OnClickListener listener;
        private final Context context;
        private CartItem[] values;

        public ListAdapter(Context context, CartItem[] values) {
            super(context, R.layout.cart_row);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.cart_row, parent, false);

            final CartItem cartItem = values[position];
            final StoreItem storeItem = getStoreItemFromCartItem(cartItem);

            final ImageView itemImage = (ImageView) rowView.findViewById(R.id.img);
            Picasso
                    .with(getActivity().getApplicationContext())
                    .load(storeItem.getPhoto())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(itemImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(getContext()).load(storeItem.getPhoto()).into(itemImage);
                        }
                    });

            TextView itemTitle = (TextView) rowView.findViewById(R.id.title);
            itemTitle.setText(storeItem.getName());

            TextView storeName = (TextView) rowView.findViewById(R.id.storeName);
            storeName.setText(storeItem.getStoreName());

            TextView itemPrice = (TextView) rowView.findViewById(R.id.price);
            itemPrice.setText(ArabicNumberFormatter.formatNumberInArabic(cartItem.getPrice()));

            final CustomPlainTextView quantity = (CustomPlainTextView) rowView.findViewById(R.id.item_quantity);
            quantity.setText(ArabicNumberFormatter.formatNumberInArabic(cartItem.getQuantity()));

//            quantity.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                }
//            });

            ImageView plusButton = (ImageView) rowView.findViewById(R.id.increase_quantity_button);
            plusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartItem.setQuantity((cartItem.getQuantity() + 1));
                    quantity.setText(ArabicNumberFormatter.formatNumberInArabic(cartItem.getQuantity()));
                    changeQuantity();
                }
            });

            ImageView minusButton = (ImageView) rowView.findViewById(R.id.decrease_quantity_button);
            minusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cartItem.getQuantity() > 1) {
                        cartItem.setQuantity((cartItem.getQuantity() - 1));
                        quantity.setText(ArabicNumberFormatter.formatNumberInArabic(cartItem.getQuantity()));
                        changeQuantity();
                    }
                }
            });


            FrameLayout container = (FrameLayout) rowView.findViewById(R.id.mainConrainer);
            Button cross = (Button) rowView.findViewById(R.id.crossBtn);

            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppData.getInstance(getActivity()).cartItems.remove(position);
                    storeItem.inCart = false;
                    redraw();
                    Intent i = new Intent(Actions.REMOVE_ITEM_FROM_CART);
                    getActivity().sendBroadcast(i);
                }
            });

            return rowView;
        }
    }

    private StoreItem getStoreItemFromCartItem(CartItem cartItem) {
        StoreItem storeItems[] = AppData.getInstance(getActivity()).allStoresItems;
        for (StoreItem storeItem : storeItems)
            if (cartItem.getStoreItem() == storeItem.getId())
                return storeItem;

        return null;
    }


    public class CartFragmentBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.ADD_ITEM_TO_CART)) {
                redraw();
            } else if (action.equals(Actions.PLACE_ORDER_DONE)) {
                AppData.getInstance(getActivity()).cartItems = new ArrayList<>();
                redraw();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (rootView != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromInputMethod(rootView.getWindowToken(), 0);
        }
    }

    private boolean checkLocationFoundOrNot() {
        if (AppData.getInstance(getActivity()).user.getLocations() != null) {
            if (AppData.getInstance(getActivity()).user.getLocations().size() != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }
}
