package com.devtoyou.devtoyou.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.Activities.PlaceOrderDoneActivity;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Location;
import com.devtoyou.devtoyou.models.PlaceOrderRequest;
import com.devtoyou.devtoyou.models.PlaceOrderResponse;
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class PlaceOrderFragment extends Fragment {
    public View view;
    public TextView deliveryTime;
    public Spinner spinner;
    public EditText orderDescp;
    public RadioGroup radioGroup;
    public Button submitBtn;
    public PlaceOrderRequest placeOrderRequest;
    public PlaceOrderFragmentReceiver receiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        placeOrderRequest = new PlaceOrderRequest();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.PLACE_ORDER_DONE);
        receiver = new PlaceOrderFragmentReceiver();
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {

            view = inflater.inflate(R.layout.place_order_fragment, container, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<CartItem> cartItems = AppData.getInstance(getActivity()).cartItems;
        redraw();
        return view;
    }

    private boolean hasEnteredDeliveryDate;
    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
            java.text.DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getDefault());
            deliveryTime.setText(df.format(date));
            deliveryTime.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            deliveryTime.setTextDirection(View.TEXT_DIRECTION_LTR);
            deliveryTime.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

            java.text.DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            placeOrderRequest.setPickup(utcFormat.format(date));
            hasEnteredDeliveryDate = true;
        }
    };

    private void redraw() {
        orderDescp = (EditText) view.findViewById(R.id.orderDes);

        deliveryTime = (TextView) view.findViewById(R.id.delivery_time);
        deliveryTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.
                        Builder(getActivity().getSupportFragmentManager()).
                        setListener(listener).setInitialDate(new Date()).setIs24HourTime(false).build().show();
            }
        });

        spinner = (Spinner) view.findViewById(R.id.spinner);

        List<Location> userLocations = AppData.getInstance(getActivity().getApplicationContext()).user.getLocations();
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_dropdown_item, userLocations.toArray(new Location[userLocations.size()]));
        spinner.setAdapter(spinnerAdapter);

        radioGroup = (RadioGroup) view.findViewById(R.id.cash_choices);

        submitBtn = (Button) view.findViewById(R.id.orderBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFormData()) {
                    placeOrderRequest();
                    Gson gson = new Gson();
                    AppCache.cache.put(Actions.PLACE_ORDER, gson.toJson(placeOrderRequest));
                    Log.d("place order json", gson.toJson(placeOrderRequest));

                    Intent i = new Intent(getActivity(), AppNetworkService.class);
                    i.setAction(Actions.PLACE_ORDER);
                    getActivity().startService(i);
                    //Intent iinent= new Intent(getActivity(),PlaceOrderDoneActivity.class);
                    //startActivity(iinent);
                }
            }
        });
    }

    private boolean checkFormData() {
        if (spinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getActivity(), "قم بأختيار العنوان قبل الطلب.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!hasEnteredDeliveryDate) {
            Toast.makeText(getActivity(), "قم بأدخال ميعاد التسليم قبل الطلب .", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void placeOrderRequest() {
        placeOrderRequest.setAdditionalNotes(orderDescp.getText().toString());
        placeOrderRequest.setStatus("REQ");
        placeOrderRequest.setItems(AppData.getInstance(getActivity()).cartItems);
        placeOrderRequest.setUser(AppData.getInstance(getActivity()).user_id);
        placeOrderRequest.setUserLocation(getLocation().getId());
        placeOrderRequest.setTotal(getTotalPriceOfCartItems());
        placeOrderRequest.setPaymentMethod(getPaymentMethod());
    }

    private int getPaymentMethod() {
        if (radioGroup.getCheckedRadioButtonId() != -1)
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.cashDel:
                    return 1;
                case R.id.payCredit:
                    return 2;
                case R.id.payPoints:
                    return 3;
                case R.id.bankTransfer:
                    return 4;
                case R.id.span:
                    return 5;
            }
        return 0;
    }

    private Double getTotalPriceOfCartItems() {
        ArrayList<CartItem> cartItems = AppData.getInstance(getActivity()).cartItems;
        double total = 0;
        for (CartItem cartItem : cartItems)
            total += (cartItem.getPrice() * cartItem.getQuantity());
        return total;
    }

    private Location getLocation() {
        if (spinner.getSelectedItemPosition() != 0) {
            return AppData.getInstance(getActivity()).user.getLocations().get(spinner.getSelectedItemPosition() - 1);
        } else {
            return null;
        }
    }

    public class SpinnerAdapter extends ArrayAdapter<Location> {
        public Location values[];

        public SpinnerAdapter(Context context, int resource, Location[] objects) {
            super(context, resource, objects);
            values = new Location[objects.length + 1];
            for (int i = 0; i < values.length; i++) {
                if (i == 0) {
                    values[0] = new Location();
                } else {
                    values[i] = objects[i - 1];
                }
            }
        }

        @Override
        public int getCount() {
            return values.length;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, parent, false);
        }


        @NonNull
        private View getCustomView(int position, ViewGroup parent, boolean isdropDownView) {
            LayoutInflater inflater = LayoutInflater.from(getActivity().getApplicationContext());
            View dropDownItem;
            if (position == 0) {
                if (isdropDownView) {
                    dropDownItem = inflater.inflate(R.layout.spinner_dropdown_item, parent, false);
                } else {
                    dropDownItem = inflater.inflate(R.layout.spinner_selected_item, parent, false);
                }
                TextView title = (TextView) dropDownItem.findViewById(R.id.spinner_item);
                title.setText("اختر العنوان");
            } else {
                dropDownItem = inflater.inflate(R.layout.spinner_dropdown_item, parent, false);
                TextView item_name = (TextView) dropDownItem.findViewById(R.id.spinner_item);
                String str = values[position].getName();
                item_name.setText(str);
            }
            return dropDownItem;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, parent, true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }

    public class PlaceOrderFragmentReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.PLACE_ORDER_DONE)) {
                Gson gson = new Gson();
                PlaceOrderResponse response = gson.fromJson(AppCache.cache.get(Actions.PLACE_ORDER_DONE), PlaceOrderResponse.class);
                Intent placeOrderDoneActivityIntent = new Intent(getActivity(), PlaceOrderDoneActivity.class);
                startActivity(placeOrderDoneActivityIntent);

                AppData.getInstance(getActivity()).init();
                getActivity().finish();
            }
        }
    }
}

