package com.devtoyou.devtoyou.Fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.devtoyou.devtoyou.Activities.AddLocationActivityOnMap;
import com.devtoyou.devtoyou.Adapters.DistrictSpinnerAdapter;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.ViewAddressActivty;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.District;
import com.devtoyou.devtoyou.models.Location;
import com.google.gson.Gson;

/**
 * Created by motaz on 11/8/15.
 */
public class AddLocationFragment extends Fragment {
    public View view;
    public EditText location_name;
    public EditText location_phone;
    public EditText location_home_number;
    public EditText location_door_no;
    public EditText location_door_color;
    public Spinner location_area;
    public EditText location_notes;
    public TextView sendButton;
    public double latitude;
    public double longitude;
    public AddLocationFragmentBraodCastReceiver receiver;
    public Location location;
    public boolean editState;
    public ImageView mapActionButton;
    public ProgressDialog progressDialog;

    public DistrictSpinnerAdapter districtSpinnerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new AddLocationFragmentBraodCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_lOCATION_FROM_USER);
        filter.addAction(Actions.EDIT_LOCATION);
        filter.addAction(Actions.EDIT_LOCATION_DONE);
        getActivity().registerReceiver(receiver, filter);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Edit Location");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        view = inflater.inflate(R.layout.add_location_fragment, container, false);
        redraw();
        return view;
    }

    private void redraw() {
        ImageView backBtn = (ImageView) view.findViewById(R.id.backIcon);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Actions.NAVIGATE_TO_LIST_OF_ADDRESS);
                getActivity().sendBroadcast(i);
            }
        });

        mapActionButton = (ImageView) view.findViewById(R.id.map_action_button);
        mapActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddLocationActivityOnMap.class);
                if (location.getLatitude() != null && location.getLongitude() != null) {
                    i.putExtra(Actions.LATITUDE, location.getLatitude());
                    i.putExtra(Actions.LONGITUDE, location.getLongitude());
                }
                startActivityForResult(i, ViewAddressActivty.PLACE_PICKER_REQUEST);
            }
        });

        // SCROLL VIEW HACK
        ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        scrollView.setFocusable(true);
        scrollView.setFocusableInTouchMode(true);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.requestFocusFromTouch();
                return false;
            }
        });

        location_name = (EditText) view.findViewById(R.id.location_name);
        location_phone = (EditText) view.findViewById(R.id.location_phone);
        location_home_number = (EditText) view.findViewById(R.id.location_home_nummber);
        location_door_no = (EditText) view.findViewById(R.id.location_door_no);
        location_door_color = (EditText) view.findViewById(R.id.location_door_color);
        location_area = (Spinner) view.findViewById(R.id.location_area);

        //TODO: on select item from spinner adapter.
//        location_area.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final District[] allDistricts = AppData.getInstance(getActivity().getApplicationContext()).allDistricts;
//                if (allDistricts != null && allDistricts.length > 0) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                    builder.setTitle("الحي").setItems(getAllDistricstsString(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            location_area.setText(allDistricts[which].getName());
//                        }
//                    }).create().show();
//                } else {
//                    Toast.makeText(getActivity(), "Loading all districts", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        final District[] allDistricts = AppData.getInstance(getActivity().getApplicationContext()).allDistricts;
        if (allDistricts != null && allDistricts.length > 0) {
            districtSpinnerAdapter = new DistrictSpinnerAdapter(getActivity(), 0, allDistricts);
            location_area.setAdapter(districtSpinnerAdapter);
            districtSpinnerAdapter.notifyDataSetChanged();
        }

        location_notes = (EditText) view.findViewById(R.id.location_notes);
        sendButton = (TextView) view.findViewById(R.id.location_send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLocation();
            }
        });

        if (editState && location != null) {
            location_name.setText(location.getName());
            location_phone.setText(location.getPhone());
            location_home_number.setText(location.getHouseNo());
            location_door_no.setText(location.getDoorNo());
            location_door_color.setText(location.getDoorColor());
            String regionName = getRegionName(location.getDistrict());
            location_area.setSelection(getSelectedRow(regionName), true);
            location_notes.setText(location.getNote());
            mapActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ViewAddressActivty.PLACE_PICKER_REQUEST) {
            if (data != null) {
                if (data.getDoubleExtra(Actions.LATITUDE, -1000.0) != -1000) {
                    latitude = data.getDoubleExtra(Actions.LATITUDE, -1000);
                    Log.d("Latidude: ", latitude + "");
                }
                if (data.getDoubleExtra(Actions.LONGITUDE, -1000.0) != -1000) {
                    longitude = data.getDoubleExtra(Actions.LONGITUDE, -1000);
                    Log.d("Longitude: ", longitude + "");
                }
            }
//            data.setAction(Actions.GET_lOCATION_FROM_USER_DONE);
//            getActivity().sendBroadcast(data);
        }

    }

    private String getRegionName(Integer districtId) {
        District[] allDistricts = AppData.getInstance(getActivity()).allDistricts;
        if (allDistricts != null) {
            for (int i = 0; i < allDistricts.length; i++) {
                if (districtId == allDistricts[i].getId()) {
                    return allDistricts[i].getName();
                }
            }
        }
        return null;
    }

    private CharSequence[] getAllDistricstsString() {
        CharSequence[] allDistrictsStrings = new CharSequence[AppData.getInstance(getActivity()).allDistricts.length];
        for (int i = 0; i < allDistrictsStrings.length; i++) {
            allDistrictsStrings[i] = AppData.getInstance(getActivity()).allDistricts[i].getName();
        }
        return allDistrictsStrings;
    }

    private void sendLocation() {
        Location location = new Location();
        String latStr = latitude + "";
        if (latStr.length() >= 15) {
            latStr = (latitude + "").substring(0, 15);
        }
        location.setLatitude(latStr);
        String longStr = longitude + "";
        if (longStr.length() >= 15) {
            longStr = longStr.substring(0, 15);
        }
        location.setLongitude(longStr);
        location.setName(location_name.getText().toString());
        location.setPhone(location_phone.getText().toString());
        location.setHouseNo(location_home_number.getText().toString());
        location.setDoorNo(location_door_no.getText().toString());
        location.setDoorColor(location_door_color.getText().toString());
        location.setDistrict(((District) location_area.getSelectedItem()).getId());
        location.setNote(location_notes.getText().toString());
        location.setUser(AppData.getInstance(getActivity()).user_id);
        Gson gson = new Gson();
        String locationStr = gson.toJson(location);

        Intent i = new Intent(getActivity(), AppNetworkService.class);
        if (editState) {
            AppCache.cache.put(Actions.EDIT_LOCATION, locationStr);
            i.setAction(Actions.EDIT_LOCATION);
            i.putExtra(Constants.LOCATION_ID, this.location.getId());
            progressDialog.show();
        } else {
            AppCache.cache.put(Actions.ADD_NEW_LOCATION, locationStr);
            i.setAction(Actions.ADD_NEW_LOCATION);
        }
        getActivity().startService(i);
    }

    public int getSelectedRow(String districtName) {
        if (districtName != null && !districtName.equals("")) {
            District[] allDistricts = AppData.getInstance(getActivity().getApplicationContext()).allDistricts;
            for (int i = 0; i < allDistricts.length; i++) {
                if (districtName.equals(allDistricts[i])) {
                    return i;
                }
            }
        }
        return 0;
    }

    public class AddLocationFragmentBraodCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_lOCATION_FROM_USER)) {
                longitude = intent.getDoubleExtra(Actions.LONGITUDE, 0.0);
                latitude = intent.getDoubleExtra(Actions.LATITUDE, 0.0);
            } else if (action.equals(Actions.EDIT_LOCATION)) {
                editState = true;
                location = new Gson().fromJson(intent.getStringExtra(Constants.SELECTED_LOCATION), Location.class);
                redraw();
            } else if (action.equals(Actions.EDIT_LOCATION_DONE)) {
                if (progressDialog.isShowing()) {
                    progressDialog.hide();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
        if (progressDialog.isShowing()) {
            progressDialog.hide();
            progressDialog = null;
        }
    }
}
