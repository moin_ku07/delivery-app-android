package com.devtoyou.devtoyou.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devtoyou.devtoyou.R;

/**
 * Created by motaz on 11/2/15.
 */
public class PopularItemViewInSliderFragmentWithDownPopWindow extends Fragment {
    public View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.popular_item_view_in_slider_with_popwindow_down, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
}
