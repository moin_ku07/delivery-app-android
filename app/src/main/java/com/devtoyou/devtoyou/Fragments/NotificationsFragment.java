package com.devtoyou.devtoyou.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.AppData;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.TimeZone;

import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.UserNotification;
import com.devtoyou.devtoyou.models.StoreItem;
import com.squareup.picasso.Picasso;

/**
 * Created by motaz on 11/6/15.
 */
public class NotificationsFragment extends Fragment {
    public View view;
    //String [] values = {"abc","def","ghi"};
    ListView list;
    public BroadcastReceiver receiver;
    public View rootView;
    public ListView lv;
    public TextView emptyMsg;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new AllNotificationsFragmentBroadCastReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_NOTIFICATIONS_DONE);
        getActivity().getApplicationContext().registerReceiver(receiver, filter);
    }

    /* public NotificationsFragment() {

     }

     public static NotificationsFragment newInstance() {

         Bundle args = new Bundle();

         NotificationsFragment fragment = new NotificationsFragment();
         fragment.setArguments(args);
         return fragment;
     }
 */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.car_listview_rubel, container, false);
            lv = (ListView) view.findViewById(R.id.list);
            emptyMsg = (TextView) view.findViewById(R.id.empty_msg);
            if (AppData.getInstance(getActivity()).allNotifications != null
                    && AppData.getInstance(getActivity()).allNotifications.length > 0) {
                ListAdapter listadapter = new ListAdapter(getActivity(), AppData.getInstance(getActivity()).allNotifications);
                lv.setAdapter(listadapter);
            } else {
                if (lv != null && emptyMsg != null) {
                    lv.setVisibility(View.GONE);
                    emptyMsg.setVisibility(View.VISIBLE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public class ListAdapter extends ArrayAdapter<String[]> {
        public View.OnClickListener listener;
        private final Context context;
        private String[] values;
        private UserNotification[] notifications;

        public ListAdapter(Context context, UserNotification[] notifications) {
            super(context, R.layout.car_listview_row_rubel);
            this.context = context;
            this.notifications = notifications;
        }

        @Override
        public int getCount() {
            return notifications.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.car_listview_row_rubel, parent, false);
            if (AppData.getInstance(getActivity()).allNotifications != null) {
                TextView headertxt = (TextView) rowView.findViewById(R.id.title);
                TextView description = (TextView) rowView.findViewById(R.id.subtitle);
                TextView time = (TextView) rowView.findViewById(R.id.time);
                headertxt.setText("Order Update");
                description.setText(notifications[position].getMessage());

                //time.setText(notifications[position].getCreatedAt());

                String toyBornTime = notifications[position].getCreatedAt();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                try {
                    TextView time1 = (TextView) rowView.findViewById(R.id.time);
                    Date oldDate = dateFormat.parse(toyBornTime);
                    Date currentDate = new Date();
                    long duration = currentDate.getTime() - oldDate.getTime();

                    long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
                    long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);
                    long month = 0;
                    long year = 0;
                    if (diffInDays > 0)
                        month = diffInDays / 30;
                    if (month > 0)
                        year = month / 12;
                    if (year > 0) {
                        if (year == 1) {
                            String Time = year + " year ago";
                            time1.setText(Time);

                        } else {
                            String Time = year + " years ago";
                            time1.setText(Time);
                        }
                    } else if (month > 0) {
                        if (month == 1) {
                            String Time = month + " month ago";
                            time1.setText(Time);
                        } else {
                            String Time = month + " months ago";
                            time1.setText(Time);
                        }
                    } else if (diffInDays > 0) {
                        if (diffInDays == 1) {
                            String Time = diffInDays + " day ago";
                            time1.setText(Time);
                        } else {
                            String Time = diffInDays + " days ago";
                            time1.setText(Time);
                        }
                    } else if (diffInHours > 0) {
                        if (diffInHours == 1) {
                            String Time = diffInHours + " hour ago";
                            time1.setText(Time);
                        } else {
                            String Time = diffInHours + " hours ago";
                            time1.setText(Time);
                        }
                    } else if (diffInMinutes > 0) {
                        if (diffInMinutes == 1) {
                            String Time = diffInMinutes + " minute ago";
                            time1.setText(Time);
                        } else {
                            String Time = diffInMinutes + " minutes ago";
                            time1.setText(Time);
                        }
                    } else if (diffInSeconds > 0) {
                        if (diffInSeconds == 1) {
                            String Time = diffInSeconds + " second ago";
                            time1.setText(Time);
                        } else {
                            String Time = diffInSeconds + " seconds ago";
                            time1.setText(Time);
                        }
                    }
                    //String Time = diffInHours + " Hours" + diffInMinutes + " minutes Ago";
                    //time1.setText(Time);
                    //Log.d("Date Time",time2);

                } catch (ParseException e) {

                    e.printStackTrace();
                }


                // long duration  = currentDateandTime.getTime() - getRemoteDateandTime.getTime();

                //long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
                //long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                //long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
                //time.setText(getRemoteDateandTime);
            }

            Button trackOrder = (Button) rowView.findViewById(R.id.trackBtn);
            /*trackOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), TrackOrderActivity.class));
                }
            });*/
            return rowView;
        }


    }

    public class AllNotificationsFragmentBroadCastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_NOTIFICATIONS_DONE)) {
                if (rootView != null) {
                    emptyMsg = (TextView) rootView.findViewById(R.id.empty_msg);
                    lv = (ListView) rootView.findViewById(R.id.list);
                    if (AppData.getInstance(getActivity()).allNotifications.length > 0) {
                        ListAdapter listadapter = new ListAdapter(getActivity(), AppData.getInstance(getActivity()).allNotifications);
                        lv.setAdapter(listadapter);
                        lv.setVisibility(View.VISIBLE);
                        emptyMsg.setVisibility(View.GONE);
                    } else {
                        lv.setVisibility(View.GONE);
                        emptyMsg.setVisibility(View.VISIBLE);
                    }
                }
            } /*else if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
                    loadingProgressDialog.cancel();
                    showStoreItems(lastClickedPosition);
                }
            }*/
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().getApplicationContext().unregisterReceiver(receiver);
    }
}
