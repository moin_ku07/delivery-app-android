package com.devtoyou.devtoyou.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.TabMenuActivity;
import com.devtoyou.devtoyou.Utils.ArabicNumberFormatter;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Store;
import com.devtoyou.devtoyou.models.StoreItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 02/03/16.
 */
public class PopularStoresFragment extends Fragment {
    public View view;
    GridView gridView;
    ListView listView;
    private GridAdapter adapter;
    private ListAdapter listadapter;
    ProgressDialog loadingProgressDialog;
    Context context;
    public int lastClickedPosition;
    public AllFavouriteStoresBroadCastReciever receiver;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new AllFavouriteStoresBroadCastReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_FAVOURITE_STORES_DONE);
        filter.addAction(Actions.GET_ALL_STORE_ITEMS_DONE);
        filter.addAction(Actions.SHOW_POPULAR_STORES);
        getActivity().registerReceiver(receiver, filter);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.popular_stores_list, container, false);
            context = getActivity().getApplicationContext();
            if (AppData.getInstance(context).fvrStores != null) {
                Log.d("***Data****", String.valueOf(AppData.getInstance(context).fvrStores));
                redraw();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void redraw() {
        adapter = new GridAdapter(context, AppData.getInstance(context).fvrStores);
        gridView = (GridView) view.findViewById(R.id.gridview);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (AppData.getInstance(context).allStoresItems != null) {
                    showStoreItems(position);
                } else {
                    lastClickedPosition = position;
                    loadingProgressDialog = ProgressDialog.show(context, "", "Loading store items", true);
                }
            }


        });
    }

    public void showStoreItems(int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_screen_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Store choosenStore = AppData.getInstance(context).fvrStores[position];

        ImageView headerImg = (ImageView) dialog.findViewById(R.id.headerImg);

        if (choosenStore.getThumb() != null) {
            headerImg.setBackgroundResource(0);
            Picasso.with(context.getApplicationContext()).load(choosenStore.getThumb()).into(headerImg);
        }
        TextView storeName = (TextView) dialog.findViewById(R.id.name);
        storeName.setText(choosenStore.getName());

        Button cross = (Button) dialog.findViewById(R.id.cross);
        TabMenuActivity.halfSizingButton(getActivity(), cross, R.drawable.cross_home, 20, 20, "top");
        cross.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        listView = (ListView) dialog.findViewById(R.id.list);
        listadapter = new ListAdapter(dialog.getContext(), getStoreItems(choosenStore.getId()));
        listView.setAdapter(listadapter);
    }

    public class GridAdapter extends ArrayAdapter<String> {
        private final Context context;
        //        private final String[] values;
//        private int[] images;
        private Store[] stores;
        private TextView text;
        private ImageView img;

        public GridAdapter(Context context, Store[] stores) {
            super(context, R.layout.grid_row);
            this.context = context;
            this.stores = stores;
        }

        @Override
        public int getCount() {
            return stores.length;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.grid_row, parent, false);
            text = (TextView) rowView.findViewById(R.id.text);
            img = (ImageView) rowView.findViewById(R.id.image);

            text.setText(stores[position].getName());
            Picasso.with(getContext()).load(stores[position].getThumb()).into(img);
            img.setBackgroundColor(Color.rgb(216, 214, 215));
            img.setScaleType(ImageView.ScaleType.CENTER_CROP);

            return rowView;
        }

    }

    public class ListAdapter extends ArrayAdapter<String[]> {
        public View.OnClickListener listener;
        private final Context context;
        private StoreItem[] values;
        public StoreItem storeItem;


        public ListAdapter(Context context, StoreItem[] values) {
            super(context, R.layout.home_screen_popup_row);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.home_screen_popup_row, parent, false);

            storeItem = values[position];
            ImageView itemImage = (ImageView) rowView.findViewById(R.id.image);
            if (storeItem.getPhoto() != null) {
                itemImage.setBackgroundResource(0);
                Picasso.with(context).load(storeItem.getPhoto()).into(itemImage);
            }
            TextView itemTitle = (TextView) rowView.findViewById(R.id.title);
            itemTitle.setText(storeItem.getName());

            TextView itemDescp = (TextView) rowView.findViewById(R.id.item_description);
            itemDescp.setText(storeItem.getDescription());

            TextView itemPrice = (TextView) rowView.findViewById(R.id.price);
            itemPrice.setText(ArabicNumberFormatter.formatNumberInArabic(storeItem.getPrice()));

            final Button add = (Button) rowView.findViewById(R.id.addBtn);

            if (storeItem.inCart) {
                add.setText("Added");
            }

            add.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    storeItem = values[position];
                    if (!storeItem.inCart) {
                        CartItem cartItem = new CartItem();
                        cartItem.setStoreItem(storeItem.getId());
                        cartItem.setPrice(storeItem.getPrice());
                        cartItem.setQuantity(1);
                        AppData.getInstance(context).cartItems.add(cartItem);
                        add.setText("Added");

                        storeItem.inCart = true;

                        Intent i = new Intent(Actions.ADD_ITEM_TO_CART);
                        context.sendBroadcast(i);
                    }
                }
            });
            return rowView;
        }
    }

    public StoreItem[] getStoreItems(int storeId) {
        List<StoreItem> storeItems = new ArrayList<>();
        for (StoreItem storeItem : AppData.getInstance(context).allStoresItems) {
            if (storeItem.getStore() == storeId) {
                storeItems.add(storeItem);
            }
        }
        return storeItems.toArray(new StoreItem[storeItems.size()]);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_fragment, menu);
        return true;
    }*/

    public class AllFavouriteStoresBroadCastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_FAVOURITE_STORES_DONE)) {
                redraw();
            } else if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
                    loadingProgressDialog.cancel();
                    showStoreItems(lastClickedPosition);
                }
            } else if (action.equals(Actions.SHOW_POPULAR_STORES)) {
                AppData.getInstance(getActivity().getApplicationContext()).getAllFavouriteStores();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }
}

