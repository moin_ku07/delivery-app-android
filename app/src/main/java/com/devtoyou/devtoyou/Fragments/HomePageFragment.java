package com.devtoyou.devtoyou.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomePageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomePageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Context context;
    ArrayList prgmName;
    TextView title;
    ImageView logo, search;
    private View rootView;
    public ViewAnimator viewAnimator;
    public HomeFragmentBroadCastReceiver receiver;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PlaceOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomePageFragment newInstance(String param1, String param2) {
        HomePageFragment fragment = new HomePageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        receiver = new HomeFragmentBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.SHOW_ALL_STORES_FRAGMENT);
        filter.addAction(Actions.SHOW_FAVORITE_ITEMS_FRAGMENT);
        filter.addAction(Actions.SHOW_POPULAR_STORES);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
        }
        try {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
            viewAnimator = (ViewAnimator) rootView.findViewById(R.id.view_animator);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        TabMenuActivity.custom_actionbar("Home");
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Runnable() {
            @Override
            public void run() {
                try {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment allStoreFragment = fragmentManager.findFragmentById(R.id.all_stores_fragment);
                    Fragment popularItemsFragment = fragmentManager.findFragmentById(R.id.popularItemsFragment);
                    Fragment populatStoresFragment = fragmentManager.findFragmentById(R.id.popularStoresFragment);
                    android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.remove(allStoreFragment);
                    transaction.remove(popularItemsFragment);
                    transaction.commitAllowingStateLoss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        getActivity().unregisterReceiver(receiver);
    }

    public class HomeFragmentBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.SHOW_ALL_STORES_FRAGMENT)) {
                viewAnimator.setDisplayedChild(0);
            } else if (action.equals(Actions.SHOW_FAVORITE_ITEMS_FRAGMENT)) {
                viewAnimator.setDisplayedChild(1);
            } else if (action.equals(Actions.SHOW_POPULAR_STORES)) {
                viewAnimator.setDisplayedChild(2);
            }
        }
    }
}
