package com.devtoyou.devtoyou.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Adapters.DriverOrdersListAdapter;
import com.devtoyou.devtoyou.Adapters.MyOrdersListAdapter;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.Order;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyOrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyOrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListView lv;
    private View view;
    private MyOrdersListAdapter listadapter;
    public MyOrderFragmentBroadcastReceiver receiver;
    public SwipeRefreshLayout swipeRefreshLayout;
    public TextView emptyMsg;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyOrderFragment newInstance(String param1, String param2) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MyOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        receiver = new MyOrderFragmentBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_ORDERS_DONE);
        filter.addAction(Actions.ACCEPT_ORDER_BY_DRIVER_DONE);
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.my_order, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lv = (ListView) view.findViewById(R.id.list);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.myOrdersSwipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                AppData.getInstance(getActivity()).getAllOrders();
            }
        });
        emptyMsg = (TextView) view.findViewById(R.id.empty_msg);

        redraw();

        return view;
    }

    private void redraw() {
        boolean emptyFlag = false;
        lv.setVisibility(View.VISIBLE);
        emptyMsg.setVisibility(View.GONE);
        if (AppData.getInstance(getActivity()).user_type != null
                && !AppData.getInstance(getActivity()).user_type.equals(Constants.DRIVER)
                && AppData.getInstance(getActivity()).orders != null) {
            Order orders[] = AppData.getInstance(getActivity()).orders;
            if (orders.length > 0) {
                listadapter = new MyOrdersListAdapter(getActivity(), orders);
                lv.setAdapter(listadapter);
                listadapter.notifyDataSetChanged();
            } else {
                emptyFlag = true;
//                lv.setVisibility(View.GONE);
//                emptyMsg.setVisibility(View.VISIBLE);
            }
        } else if (AppData.getInstance(getActivity()).driverOrders != null) {
            DriverOrder[] driverOrders = AppData.getInstance(getActivity()).driverOrders;
            if (driverOrders.length > 0) {
                DriverOrdersListAdapter listAdapter = new DriverOrdersListAdapter(getActivity(), 0, driverOrders);
                lv.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
            } else {
                emptyFlag = true;
            }
        }
        if (emptyFlag) {
            lv.setVisibility(View.GONE);
            emptyMsg.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public class MyOrderFragmentBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_ORDERS_DONE)) {
                redraw();
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            } else if (action.equals(Actions.ACCEPT_ORDER_BY_DRIVER_DONE)) {
                AppData.getInstance(getActivity()).getAllOrders();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
    }
}
