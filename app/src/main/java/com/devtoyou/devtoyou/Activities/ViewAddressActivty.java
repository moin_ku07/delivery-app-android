package com.devtoyou.devtoyou.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.District;
import com.devtoyou.devtoyou.models.Location;
import com.google.gson.Gson;

import java.util.List;

public class ViewAddressActivty extends FragmentActivity {
    public static int PLACE_PICKER_REQUEST = 1;
    public ViewAnimator viewAnimator;
    public ViewAddressBroadCastReceiver receiver;
    public ProgressDialog progressDialog;
    public List<Location> locations;
    public double lastSelectedLat;
    public double lastSelectedLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.NAVIGATE_TO_LIST_OF_ADDRESS);
        filter.addAction(Actions.ADD_NEW_LOCATION_DONE);
        filter.addAction(Actions.GET_USER_INFO_DONE);
        filter.addAction(Actions.DELETE_USER_LOCATION_DONE);
        filter.addAction(Actions.GET_ALL_DISTRICTS_DONE);
        filter.addAction(Actions.EDIT_LOCATION_DONE);
        filter.addAction(Actions.GET_lOCATION_FROM_USER_DONE);
        receiver = new ViewAddressBroadCastReceiver();
        getApplicationContext().registerReceiver(receiver, filter);

        viewAnimator = (ViewAnimator) findViewById(R.id.view_address_view_animator);

        redraw();
        AppData.getInstance(this.getApplicationContext()).getAllDistricts();
    }


    private void redraw() {
        locations = AppData.getInstance(getApplicationContext())
                .user.getLocations();

        final SwipeMenuListView userLocations = (SwipeMenuListView) findViewById(R.id.user_locations);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xFD, 0x3B,
                        0x2F)));
                deleteItem.setWidth(dp2px(90));
                // set item title
                deleteItem.setTitle("حذف");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }

            private int dp2px(int i) {
                DisplayMetrics metrics = ViewAddressActivty.this.getResources().getDisplayMetrics();
                return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, metrics);
            }
        };

        userLocations.setMenuCreator(creator);
        userLocations.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);
        userLocations.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                Location location = locations.get(position);
                Intent i = new Intent(ViewAddressActivty.this, AppNetworkService.class);
                i.setAction(Actions.DELETE_USER_LOCATION);
                i.putExtra(Constants.AUTHENTICATION_TOKEN, AppData.getInstance(getApplicationContext()).auth_token);
                i.putExtra(Constants.LOCATION_ID, location.getId());
                getApplicationContext().startService(i);
                progressDialog = ProgressDialog.show(ViewAddressActivty.this, "", "Please wait", true);
                return false;
            }
        });


        Log.d("****Location", String.valueOf(locations));
        UserLocationsArrayAdapter adapter = new UserLocationsArrayAdapter(getApplicationContext()

                , R.layout.location_list_view_item
                , locations.toArray(new Location[locations.size()]));
        userLocations.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        userLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Actions.EDIT_LOCATION);
                Location location = locations.get(position);
                i.putExtra(Constants.SELECTED_LOCATION, new Gson().toJson(location));
                sendBroadcast(i);
                viewAnimator.showNext();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.popular_stores, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void plusBtnClick(View view) {
        Intent i = new Intent(ViewAddressActivty.this, AddLocationActivityOnMap.class);
        startActivityForResult(i, PLACE_PICKER_REQUEST);
    }

    public void backBtnFunction(View view) {
        ViewAddressActivty.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (data != null) {
                if (data.getDoubleExtra(Actions.LATITUDE, -1000.0) != -1000) {
                    lastSelectedLat = data.getDoubleExtra(Actions.LATITUDE, -1000);
                    Log.d("Latidude: ", lastSelectedLat + "");
                }
                if (data.getDoubleExtra(Actions.LONGITUDE, -1000.0) != -1000) {
                    lastSelectedLng = data.getDoubleExtra(Actions.LONGITUDE, -1000);
                    Log.d("Longitude: ", lastSelectedLng + "");
                }
                Intent i = new Intent(Actions.GET_lOCATION_FROM_USER);
                i.putExtra(Actions.LONGITUDE, lastSelectedLng);
                i.putExtra(Actions.LATITUDE, lastSelectedLat);
                sendBroadcast(i);
                // go to the form fragment.
                viewAnimator.setDisplayedChild(1);
            }
        }
    }

    public class ViewAddressBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.NAVIGATE_TO_LIST_OF_ADDRESS)) {
                viewAnimator.showPrevious();
            } else if (action.equals(Actions.ADD_NEW_LOCATION_DONE) || action.equals(Actions.EDIT_LOCATION_DONE)) {
                viewAnimator.showPrevious();
                progressDialog = ProgressDialog.show(ViewAddressActivty.this, "Getting all locations", "Please wait", true);
                AppData.getInstance(getApplicationContext()).init();
                if (AppData.getInstance(getApplicationContext()).from_place_order_flag) {
                    AppData.getInstance(getApplicationContext()).from_place_order_flag = false;
                    finish();
                }
            } else if (action.equals(Actions.GET_USER_INFO_DONE)) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                redraw();
            } else if (action.equals(Actions.DELETE_USER_LOCATION_DONE)) {
                AppData.getInstance(getApplicationContext()).init();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } else if (action.equals(Actions.GET_ALL_DISTRICTS_DONE)) {
                AppData.getInstance(getApplicationContext()).allDistricts = new Gson().fromJson(AppCache.cache.get(Constants.ALL_DISTRICTS), District[].class);
            }
        }
    }

    public class UserLocationsArrayAdapter extends ArrayAdapter<Location> {
        Location locations[];

        public UserLocationsArrayAdapter(Context context, int resource, Location[] objects) {
            super(context, resource, objects);
            this.locations = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View itemView = inflater.inflate(R.layout.location_list_view_item, parent, false);
            TextView locationName = (TextView) itemView.findViewById(R.id.name);
            locationName.setText(locations[position].getName());
            TextView locationPhn = (TextView) itemView.findViewById(R.id.phn);
            locationPhn.setText(locations[position].getPhone());
            TextView address = (TextView) itemView.findViewById(R.id.address);
            address.setText("رقم المنزل." + " " + locations[position].getHouseNo());
            TextView region = (TextView) itemView.findViewById(R.id.region);
            region.setText(locations[position].getRegion());

            return itemView;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        if (viewAnimator.getDisplayedChild() == 1) {
            Intent i = new Intent(this, AddLocationActivityOnMap.class);
            i.putExtra(Actions.LATITUDE, lastSelectedLat);
            i.putExtra(Actions.LONGITUDE, lastSelectedLng);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
            viewAnimator.setDisplayedChild(0);
        } else {
            finish();
        }
    }
}

