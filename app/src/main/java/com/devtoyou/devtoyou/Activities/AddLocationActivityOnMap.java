package com.devtoyou.devtoyou.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.Adapters.PlaceAutocompleteAdapter;
import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AddLocationActivityOnMap extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleMap.OnMarkerDragListener {
    public static final int RESULT_CODE = 1;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    AutoCompleteTextView autocompleteView;

    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;


    public Location currentLocation;

    public double chosenLatidude;
    public double chosenLongitude;

    public Place chosenPlace;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    private static final String LOG_TAG = "Google Places Autocomplete";
    private boolean editLocationFlag;
    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getStringExtra(Actions.LATITUDE) != null) {
            editLocationFlag = true;
            chosenLatidude = Double.parseDouble(getIntent().getStringExtra(Actions.LATITUDE));
            chosenLongitude = Double.parseDouble(getIntent().getStringExtra(Actions.LONGITUDE));
        }
        setContentView(com.devtoyou.devtoyou.R.layout.maps);

        autocompleteView = (AutoCompleteTextView) findViewById(R.id.searchText);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .build();

        // Register a listener that receives callbacks when a suggestion has been selected
        autocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY,
                null);
        autocompleteView.setAdapter(mAdapter);

        mGoogleApiClient.registerConnectionCallbacks(this);

        TextView doneBtn = (TextView) findViewById(R.id.doneBtn);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(Actions.LATITUDE, chosenLatidude);
                i.putExtra(Actions.LONGITUDE, chosenLongitude);
                setResult(RESULT_CODE, i);
                finish();
            }
        });

        ImageButton backBtn = (ImageButton) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            setUpMapIfNeeded();
        }
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(LOG_TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            View focusedView = AddLocationActivityOnMap.this.getCurrentFocus();
            if (focusedView != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(AddLocationActivityOnMap.this.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
            }
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            chosenPlace = place;
            chosenLatidude = place.getLatLng().latitude;
            chosenLongitude = place.getLatLng().longitude;
            setUpMapIfNeeded();
            places.release();
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            setUpMapIfNeeded();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            setUpMapIfNeeded();
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the mapFragment.
        if (mMap == null) {
            // Try to obtain the mapFragment from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mMap.clear();
                    Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
                    chosenLatidude = latLng.latitude;
                    chosenLongitude = latLng.longitude;
                }
            });
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    if (currentLocation != null)
                        mMap.animateCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 14));
                    return true;
                }
            });
        }
        // Check if we were successful in obtaining the mapFragment.
        if (mMap != null) {
            if (editLocationFlag) {
                addMarker(chosenLatidude, chosenLongitude, "");
            } else if (chosenPlace != null) {
                addMarker(chosenPlace.getLatLng().latitude, chosenPlace.getLatLng().longitude, chosenPlace.getName().toString());
            } else if (currentLocation != null) {
//                mMap.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).draggable(true));
//                mMap.animateCamera(
//                        CameraUpdateFactory.newLatLngZoom(
//                                new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 14));
                chosenLatidude = currentLocation.getLatitude();
                chosenLongitude = currentLocation.getLongitude();
                addMarker(currentLocation.getLatitude(), currentLocation.getLongitude(), "");
            }
        }
    }

    private void addMarker(double lat, double lng, String name) {
        mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                        new LatLng(lat, lng), 14));
        mMap.clear();
        Marker marker = mMap.addMarker(new MarkerOptions().position(
                new LatLng(lat, lng)).
                title(name).draggable(true));
        marker.setDraggable(true);
        mMap.setOnMarkerDragListener(this);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        setUpMapIfNeeded();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d("Marker", "Marker drag start");
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Log.d("Marker", "Marker drag");
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.d("Marker position", marker.getPosition().latitude + " " + marker.getPosition().longitude);
        chosenLatidude = marker.getPosition().latitude;
        chosenLongitude = marker.getPosition().longitude;
    }
}