package com.devtoyou.devtoyou.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.CustomViews.OrderStatusItemView;
import com.devtoyou.devtoyou.Dialogs.DriverLeaveStoreDialog;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.DriverOrderItem;
import com.devtoyou.devtoyou.models.LocationOrderDriver;
import com.devtoyou.devtoyou.models.OrderStatusItem;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by motaz on 10/01/16.
 */
public class DriverOrderDetailsActivity extends FragmentActivity {
    public DriverOrder order;
    public boolean acceptOrderButtonEnabled;
    public boolean deliverButtonEnabled;
    public DriverOrderDetailsActivityBroadCastReceiver receiver;
    public OrderStatusItemView[] orderStatusItemViews;
    public OrderStatusItem[] orderStatusItemsArray;
    public int lastStorePressedIndex;
    public double balanceAmount;
    public boolean driverHasDeliveredOrder;
    public TextView deliverOrderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_order_details_layout);

        receiver = new DriverOrderDetailsActivityBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_DRIVER_ORDER_DONE);
        filter.addAction(Actions.DRIVER_LEAVE_STORE_BALANCE);
        filter.addAction(Actions.UPDATE_STORE_AND_USER_BALANCE_DONE);
        filter.addAction(Actions.UPDATE_DRIVER_ORDER_STATUS_DONE);
        filter.addAction(Actions.DRIVER_DELIVER_ORDER_DONE);
        filter.addAction(Actions.ACCEPT_ORDER_BY_DRIVER_DONE);
        registerReceiver(receiver, filter);

        getDriverOrder();


        order = AppData.getInstance(this).current_driver_order;

        if (order != null) {
            initOrderStatusItemsArray();
        }
        redraw();
    }

    private void getDriverOrder() {
        int order_id = getIntent().getIntExtra(Constants.ORDER_ID, -1);
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_DRIVER_ORDER);
        i.putExtra(Constants.ORDER_ID, order_id);
        startService(i);
    }

    private void initOrderStatusItemsArray() {
        Collection<OrderStatusItem> orderStatusItems = getOrderStatusItems();
        orderStatusItemsArray = orderStatusItems.toArray(new OrderStatusItem[orderStatusItems.size()]);
    }

    private void redraw() {
        if (order != null) {
            ImageView backBtn = (ImageView) findViewById(R.id.backIcon);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            TextView orderListing = (TextView) findViewById(R.id.order_details);
            orderListing.setText(getOrderItemsText());

            final TextView orderAddress = (TextView) findViewById(R.id.order_address);
            orderAddress.setText(getOrderAddress());


            TextView showAddressButton = (TextView) findViewById(R.id.showOrderAddressButton);
            showAddressButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppData.getInstance(getApplicationContext()).current_driver_order = order;
                    Intent i = new Intent(DriverOrderDetailsActivity.this, ShowOrderAdressActivity.class);
                    startActivity(i);
                }
            });


            acceptOrderButtonEnabled = canWeAcceptOrder(order);
            deliverButtonEnabled = !acceptOrderButtonEnabled;

            drawAcceptOrderButton();

            drawDeliveredOrderButton();

            drawOrderStatusButtons();
        }
    }

    private void drawDeliveredOrderButton() {
        deliverOrderButton = (TextView) findViewById(R.id.deliver_order_done);

        if (deliverButtonEnabled && canIDeliverToUser()) {
            deliverOrderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("deliver order: ", "click");
                    driverHasDeliveredOrder = true;
                    DriverLeaveStoreDialog dialog = new DriverLeaveStoreDialog().newInstance("تنبيه", false);
                    dialog.show(getSupportFragmentManager(), "Driver delivered order");
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_not_active_button, getTheme()));
            else
                deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_active_button));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_not_active_button, getTheme()));
            else
                deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_not_active_button));
        }
    }

    private boolean canIDeliverToUser() {
        if (order.getStatus().equals("DELIV"))
            return false;

        for (int i = 0; i < orderStatusItemsArray.length; i++) {
            if (!orderStatusItemsArray[i].hasGoneToStore) {
                return false;
            }
        }
        return true;
    }

    private void drawAcceptOrderButton() {
        TextView acceptOrder = (TextView) findViewById(R.id.accept_order);
        if (acceptOrderButtonEnabled)
            acceptOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(DriverOrderDetailsActivity.this, AppNetworkService.class);
                    i.setAction(Actions.ACCEPT_ORDER_BY_DRIVER);
                    i.putExtra(Constants.DRIVER_ID, order.getDriver());
                    i.putExtra(Constants.ORDER_ID, order.getId());
                    startService(i);
                }
            });
        else {
            acceptOrder.setVisibility(View.GONE);
        }
    }

    private void drawOrderStatusButtons() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.order_status_buttons);
        linearLayout.removeAllViewsInLayout();

        orderStatusItemViews = new OrderStatusItemView[orderStatusItemsArray.length * 2];

        int inStoreIndex = checkIfDriverInAnyStore();
        int j = 0;
        drawStoreButtons(linearLayout, inStoreIndex, j);
    }

    private void drawStoreButtons(LinearLayout linearLayout, int inStoreIndex, int j) {
        for (int i = 0; i < orderStatusItemsArray.length; i++) {
            OrderStatusItemView inStore = new OrderStatusItemView(this);
            inStore.init("في " + orderStatusItemsArray[i].storeName);

            OrderStatusItemView leaveStore = new OrderStatusItemView(this);
            leaveStore.init("غادر " + orderStatusItemsArray[i].storeName);

            linearLayout.addView(inStore);
            orderStatusItemViews[j] = inStore;
            j++;
            linearLayout.addView(leaveStore);
            orderStatusItemViews[j] = leaveStore;
            j++;
            setStoreButtonsBackground(inStoreIndex, i, inStore, leaveStore);
            inStore.setOnClickListener(onClickListener);
            leaveStore.setOnClickListener(onClickListener);
        }
    }

    private void setStoreButtonsBackground(int inStoreIndex, int i, OrderStatusItemView inStore, OrderStatusItemView leaveStore) {
        int inStoreBackGroundColor = 0;
        int leaveStoreBackgroundColor = 0;
        boolean flag = false;

        if (orderStatusItemsArray[i].hasGoneToStore || (inStoreIndex != -1 && inStoreIndex != i) || order.getStatus().equals("AD")) {
            inStoreBackGroundColor = R.color.order_status_item_not_active_color;
            leaveStoreBackgroundColor = R.color.order_status_item_not_active_color;
            flag = true;
        } else if (orderStatusItemsArray[i].isInStore) {
            inStoreBackGroundColor = R.color.order_status_item_not_active_color;
            leaveStoreBackgroundColor = R.color.order_status_item_active_color;
            flag = true;
        }

        if (flag)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                inStore.setBackgroundColor(getResources().getColor(inStoreBackGroundColor, getTheme()));
                leaveStore.setBackgroundColor(getResources().getColor(leaveStoreBackgroundColor, getTheme()));
            } else {
                inStore.setBackgroundColor(getResources().getColor(inStoreBackGroundColor));
                leaveStore.setBackgroundColor(getResources().getColor(leaveStoreBackgroundColor));
            }
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView storeButton = (TextView) v;
            String storeName = storeButton.getText().toString();
            int storeIndex;
            for (int i = 0; i < orderStatusItemsArray.length; i++) {
                if (!orderStatusItemsArray[i].hasGoneToStore)
                    if (storeName.contains(orderStatusItemsArray[i].storeName)) {
                        if (storeName.contains("في")) {
                            lastStorePressedIndex = i;
                            orderStatusItemsArray[i].isInStore = true;
                            updateOrderStatusRequest("DIS");
                        } else {
                            lastStorePressedIndex = i;
                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                            DriverLeaveStoreDialog dialog = new DriverLeaveStoreDialog().newInstance("تنبيه", true);
                            dialog.show(fragmentManager, "add balance dialog");
                        }
                    }
            }
            redraw();
        }
    };

    private void driverLeaveStorePrcoedure() {
        orderStatusItemsArray[lastStorePressedIndex].isInStore = false;
        orderStatusItemsArray[lastStorePressedIndex].leaveStore = true;
        orderStatusItemsArray[lastStorePressedIndex].hasGoneToStore = true;
    }

    private int checkIfDriverInAnyStore() {
        for (int i = 0; i < orderStatusItemsArray.length; i++) {
            //TODO: check for order status to be "DIS"
            if (orderStatusItemsArray[i].isInStore) {
                return i;
            }
        }
        return -1;
    }

    private boolean canWeAcceptOrder(DriverOrder order) {
        String orderStatus = order.getStatus();
        if (orderStatus.equals("REQ")
                || orderStatus.equals("REV")
                || orderStatus.equals("APPR")
                || orderStatus.equals("AD"))
            return true;
        return false;
    }

    private Collection<OrderStatusItem> getOrderStatusItems() {
        HashMap<Integer, OrderStatusItem> allOrderStores = new HashMap<>();
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            if (!allOrderStores.containsKey(item.getStoreId())) {
                OrderStatusItem orderStatusItem = new OrderStatusItem();
                orderStatusItem.orderID = item.getOrder();
                orderStatusItem.storeName = item.getStoreName();
                orderStatusItem.storeID = item.getStoreId();
                List<LocationOrderDriver> dis_dls = order.getDis_dls();
                for (int j = 0; j < dis_dls.size(); j++) {
                    LocationOrderDriver locationOrderDriver = dis_dls.get(j);
                    if (locationOrderDriver.getStore() == orderStatusItem.storeID) {
                        if (locationOrderDriver.getStatus().equals("DLS")) {
                            orderStatusItem.hasGoneToStore = true;
                        } else if (locationOrderDriver.getStatus().equals("DIS")) {
                            orderStatusItem.isInStore = true;
                        }
                    }
                }
                allOrderStores.put(item.getStoreId(), orderStatusItem);
            }
        }
        return allOrderStores.values();
    }

    private String getOrderAddress() {
        String address = "";
        address += order.getUserLocation().getName() + "\n";
        address += "رقم المنزل: " + order.getUserLocation().getHouseNo() + "\n";
        address += "رقم الجوال: " + order.getUserLocation().getPhone() + "\n";
        address += "رقم الباب: " + order.getUserLocation().getDoorNo() + "\n";
        address += "لون الباب: " + order.getUserLocation().getDoorColor();
        return address;
    }

    private String getOrderItemsText() {
        String orderItemsText = "";
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            orderItemsText += item.getItemName() + " * " + item.getQuantity() + " من " + item.getStoreName();
            if (i != order.getItems().size() - 1) {
                orderItemsText += "\n";
            }
        }
        return orderItemsText;
    }

    private void updateStoreAndUserBalance(double balanceAmount) {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.UPDATE_STORE_AND_USER_BALANCE);
        i.putExtra(Constants.USER_ID, order.getUser());
        i.putExtra(Constants.DRIVER_ID, AppData.getInstance(this).user_id);
        i.putExtra(Constants.STORE_ID, orderStatusItemsArray[lastStorePressedIndex].storeID);
        i.putExtra(Constants.DRIVER_AMOUNT, balanceAmount);
        startService(i);
    }

    private void updateOrderStatusRequest(String status) {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.UPDATE_DRIVER_ORDER_STATUS);
        i.putExtra(Constants.ORDER_ID, order.getId());
        i.putExtra(Constants.STORE_ID, orderStatusItemsArray[lastStorePressedIndex].storeID);
        i.putExtra(Constants.DRIVER_ORDER_STATUS, status);
        startService(i);
    }

    private void deliverOrderToCustomer() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.DRIVER_DELIVER_ORDER);
        i.putExtra(Constants.DRIVER_AMOUNT, balanceAmount);
        i.putExtra(Constants.USER_ID, order.getUser());
        i.putExtra(Constants.DRIVER_ID, order.getDriver());
        i.putExtra(Constants.ORDER_ID, order.getId());
        startService(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppData.getInstance(this).current_driver_order = null;
        unregisterReceiver(receiver);
    }

    public class DriverOrderDetailsActivityBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_DRIVER_ORDER_DONE)) {
                order = AppData.getInstance(DriverOrderDetailsActivity.this).current_driver_order;
                initOrderStatusItemsArray();
                redraw();
            } else if (action.equals(Actions.DRIVER_LEAVE_STORE_BALANCE)) {
                balanceAmount = intent.getDoubleExtra(Constants.DRIVER_AMOUNT, 0);
                if (driverHasDeliveredOrder) {
                    deliverOrderToCustomer();
                } else {
                    updateStoreAndUserBalance(balanceAmount);
                }
            } else if (action.equals(Actions.UPDATE_STORE_AND_USER_BALANCE_DONE)) {
                driverLeaveStorePrcoedure();
                updateOrderStatusRequest("DLS");
            } else if (action.equals(Actions.UPDATE_DRIVER_ORDER_STATUS_DONE)) {
                getDriverOrder();
                Toast.makeText(getApplicationContext(), "تم بنجاح.", Toast.LENGTH_SHORT).show();
            } else if (action.equals(Actions.DRIVER_DELIVER_ORDER_DONE)) {
                deliverOrderButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_not_active_button, getTheme()));
                } else {
                    deliverOrderButton.setBackgroundColor(getResources().getColor(R.color.accept_order_not_active_button));
                }
                Toast.makeText(getApplicationContext(), "تم التوصيل بنجاح.", Toast.LENGTH_SHORT).show();
                AppData.getInstance(getApplicationContext()).getAllOrders();
                finish();
            } else if (action.equals(Actions.ACCEPT_ORDER_BY_DRIVER_DONE)) {
                getDriverOrder();
                Toast.makeText(getApplicationContext(), "تم بنجاح.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
