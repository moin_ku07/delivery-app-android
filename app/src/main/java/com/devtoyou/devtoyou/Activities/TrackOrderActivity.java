package com.devtoyou.devtoyou.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DriverLocation;
import com.devtoyou.devtoyou.models.DriverOrderItem;
import com.devtoyou.devtoyou.models.Location;
import com.devtoyou.devtoyou.models.Order;
import com.devtoyou.devtoyou.models.OrderItem;
import com.devtoyou.devtoyou.models.OrderStatus;
import com.devtoyou.devtoyou.models.Store;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class TrackOrderActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    ImageButton menu, back;
    TextView title;

    public Order order;
    public OrderStatus orderStatus;
    public DriverLocation driverLocation;
    public TrackOrderActivityBroadCastReceiver receiver;

    public TextView orderTime;
    public ImageView startCircle;

    public TextView start_review_separator;

    public TextView reviewText;
    public ImageView reviewCircle;

    public TextView review_approved_separator;

    public TextView approvedText;
    public ImageView approvedCircle;

    public TextView approved_assignedToDriver_separator;

    public TextView assignedToDriverText;
    public ImageView assignedToDriverCircle;

    public TextView assignedToDriver_driverGoingToStore_separator;

    public TextView driverGoingToStore;
    public ImageView driverGoingToStoreCircle;

    public TextView driverGoingToStoreCircle_inStoreCircle_separator;

    public TextView inStoreText;
    public ImageView inStoreCircle;

    public TextView inStoreCircle_LeaveStoreCircle_separator;

    public TextView leaveStoreText;
    public ImageView leaveStoreCircle;

    public TextView leaveStoreCircle_deliveredCircle_separator;

    public ImageView deliveredCircle;
    public TextView deliveredText;


    public double animateLat;
    public double animateLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_order);
        menu = (ImageButton) findViewById(R.id.menuBtn);
        back = (ImageButton) findViewById(R.id.backIcon);
        title = (TextView) findViewById(R.id.pageTitle);

        menu.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        title.setText("Track Order");

        orderTime = (TextView) findViewById(R.id.order_time);
        startCircle = (ImageView) findViewById(R.id.start_circle);

        start_review_separator = (TextView) findViewById(R.id.start_separator);

        reviewText = (TextView) findViewById(R.id.review_text);
        reviewCircle = (ImageView) findViewById(R.id.review_circle);

        review_approved_separator = (TextView) findViewById(R.id.review_separator);

        approvedText = (TextView) findViewById(R.id.approved_text);
        approvedCircle = (ImageView) findViewById(R.id.approved_circle);

        approved_assignedToDriver_separator = (TextView) findViewById(R.id.approved_separator);

        assignedToDriverText = (TextView) findViewById(R.id.assignToDriver_text);
        assignedToDriverCircle = (ImageView) findViewById(R.id.assignToDriver_circle);

        assignedToDriver_driverGoingToStore_separator = (TextView) findViewById(R.id.assignToDriver_separator);

        driverGoingToStore = (TextView) findViewById(R.id.driver_going_to_store);
        driverGoingToStoreCircle = (ImageView) findViewById(R.id.driverGoingToStore_circle);

        driverGoingToStoreCircle_inStoreCircle_separator = (TextView) findViewById(R.id.driverGoingToStore_separator);

        inStoreText = (TextView) findViewById(R.id.inStore_text);
        inStoreCircle = (ImageView) findViewById(R.id.inStore_circle);

        inStoreCircle_LeaveStoreCircle_separator = (TextView) findViewById(R.id.inStore_separator);

        leaveStoreText = (TextView) findViewById(R.id.leaveStore_text);
        leaveStoreCircle = (ImageView) findViewById(R.id.leaveStore_circle);

        leaveStoreCircle_deliveredCircle_separator = (TextView) findViewById(R.id.leaveStore_separator);

        deliveredText = (TextView) findViewById(R.id.delivered_text);
        deliveredCircle = (ImageView) findViewById(R.id.delivered_circle);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        receiver = new TrackOrderActivityBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ORDER_STATUS_DONE);
        filter.addAction(Actions.GET_DRIVER_LOCATION_DONE);
        registerReceiver(receiver, filter);

        Gson gson = new Gson();
        order = gson.fromJson(AppCache.cache.get(Actions.CURRENT_ORDER), Order.class);

        getOrderStatus();

        getDriverLocation(order.getDriver());

        setUpMapIfNeeded();
        redraw();
    }

    private void getDriverLocation(int driverID) {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_DRIVER_LOCATION);
        i.putExtra(Constants.DRIVER_ID, driverID);
        startService(i);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void redraw() {
        orderTime.setText(getOrderTime(order.getCreatedAt()));
        updateStepsBasedOnOrderStatus();
    }

    private void updateStepsBasedOnOrderStatus() {
        String status;
        if (orderStatus == null) {
            status = order.getStatus();

        } else {
            status = orderStatus.getStatus();
        }
        if (status.equals("REQ")) {
            applyRequestStatus();
        } else if (status.equals("REV")) {
            applyReviewStatus();
        } else if (status.equals("APPR")) {
            applyApprovedStatus();
        } else if (status.equals("AD")) {
            applyAssignedToDriverStatus();
        } else if (status.equals("DOTW")) {
            applyDriverOnTheWayStatus();
        } else if (status.equals("DIS")) {
            applyDriverInStore();
        } else if (status.equals("DLS")) {
            applyDriverLeaveStoreStatus();
        } else if (status.equals("DELIV")) {
            applyDeliveredStatus();
        }
    }

    private void applyDeliveredStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle_LeaveStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            inStoreCircle_LeaveStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle_deliveredCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            leaveStoreCircle_deliveredCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

    }

    private void applyDriverLeaveStoreStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle_LeaveStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            inStoreCircle_LeaveStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private void applyDriverInStore() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            driverGoingToStoreCircle_inStoreCircle_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private void applyDriverOnTheWayStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            assignedToDriver_driverGoingToStore_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private void applyAssignedToDriverStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        orderTime.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            approved_assignedToDriver_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

    }

    private void applyApprovedStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        orderTime.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            review_approved_separator.setBackground(getResources().getDrawable(R.color.red));
        }
        approvedText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private void applyReviewStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red, getTheme()));
        } else {
            start_review_separator.setBackground(getResources().getDrawable(R.color.red));
        }

        reviewText.setTextColor(getResources().getColor(R.color.black));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }

        approvedText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private void applyRequestStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle, getTheme()));
        } else {
            startCircle.setImageDrawable(getResources().getDrawable(R.drawable.red_circle));
        }
        orderTime.setTextColor(getResources().getColor(R.color.black));

        reviewText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            reviewCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        approvedText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            approvedCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        assignedToDriverText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            assignedToDriverCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        driverGoingToStore.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            driverGoingToStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        inStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            inStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        leaveStoreText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            leaveStoreCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }

        deliveredText.setTextColor(getResources().getColor(R.color.gray_text_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle, getTheme()));
        } else {
            deliveredCircle.setImageDrawable(getResources().getDrawable(R.drawable.grey_circle));
        }
    }

    private String getOrderTime(String createdAt) {
        java.text.DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = new Date();
        Calendar calendar;
        try {
            date = utcFormat.parse(createdAt);
            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

        return simpleDateFormat.format(date);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the mapFragment if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the mapFragment has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the mapFragment.
        if (mMap == null) {
            // Try to obtain the mapFragment from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the mapFragment.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.getUiSettings().setZoomControlsEnabled(true);
        if (driverLocation != null) {
            double driverLat = Double.parseDouble(driverLocation.getLatitude());
            double driverLong = Double.parseDouble(driverLocation.getLongitude());
            MarkerOptions driverMarker = new MarkerOptions().position(new LatLng(driverLat, driverLong));
            driverMarker.title("Driver");
            mMap.addMarker(driverMarker);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverLat, driverLong), 14));
        }

        drawStores();
    }

    private void drawStores() {
        HashSet<Integer> storeIds = new HashSet<>();
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            if (item.getStoreBranches() != null && item.getStoreBranches().size() > 0) {
                MarkerOptions storeMarker = new MarkerOptions().position(
                        new LatLng(Double.parseDouble(item.getStoreBranches().get(0).getLatitude()),
                                Double.parseDouble(item.getStoreBranches().get(0).getLongitude())));
                storeMarker.title(item.getStoreName());
                mMap.addMarker(storeMarker);
            }
        }

//        Store[] allStores = AppData.getInstance(getApplicationContext()).allStores;
//        for (int i = 0; i < allStores.length; i++) {
//            MarkerOptions storeMarker = new MarkerOptions(new LatLng(allStores[i].));
//            mMap.addMarker();

    }

    private HashSet<Integer> getAllStores() {
        HashSet<Integer> storeIds = new HashSet<>();
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            if (item != null && item.getStoreId() != null) {
                storeIds.add(item.getId());
            }
        }
        return storeIds;
    }

    public void getOrderStatus() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_ORDER_STATUS);
        i.putExtra(Constants.ORDER_ID, order.getId());
        startService(i);
    }

    public class TrackOrderActivityBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ORDER_STATUS_DONE)) {
                String orderStatusStr = intent.getStringExtra(Constants.ORDER_STATUS);
                AppData appData = AppData.getInstance(getApplicationContext());
                orderStatus = appData.orderStatus;
                updateStepsBasedOnOrderStatus();
//                if (orderStatus.getDriverLocation() != null && orderStatus.getDriverLocation().size() > 0) {
//                    driverLocation = (Location) orderStatus.getDriverLocation().get(0);
//                }
//                setUpMap();
            } else if (action.equals(Actions.GET_DRIVER_LOCATION_DONE)) {
                String driverLocationStr = intent.getStringExtra(Constants.DRIVER_LOCATION);
                Log.d("Driver location", driverLocationStr);
                Gson gson = new Gson();
                driverLocation = gson.fromJson(driverLocationStr, DriverLocation.class);
                setUpMap();
            }
        }
    }
}



