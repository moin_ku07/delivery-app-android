package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by motaz on 11/2/15.
 */
public class StoreUploadItemActivity extends FragmentActivity {
    public TextView editPhoto;
    public ImageView itemImage;
    public EditText itemName;
    public EditText itemPrice;
    public EditText itemDescp;
    public Button submit;
    public String itemImagePath;
    public BroadCastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_upload_item);

        initActionBar();

        receiver = new BroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.ADD_STORE_ITEM_DONE);
        registerReceiver(receiver, filter);

        editPhoto = (TextView) findViewById(R.id.edit_store_item_photo);
        itemImage = (ImageView) findViewById(R.id.item_image);
        itemName = (EditText) findViewById(R.id.item_name);
        itemPrice = (EditText) findViewById(R.id.item_price);
        itemDescp = (EditText) findViewById(R.id.item_description);
        submit = (Button) findViewById(R.id.submit);

        editPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageCaptureDialog();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitStoreItem();
            }
        });


    }

    private void initActionBar() {
        TextView pageTitle = (TextView) findViewById(R.id.pageTitle);
        pageTitle.setText("اضافه منتج");

        ImageButton menuButton = (ImageButton) findViewById(R.id.menuBtn);
        menuButton.setVisibility(View.GONE);

        ImageButton backButton = (ImageButton) findViewById(R.id.backIcon);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /* Show alert to select / take images*/

    private void showImageCaptureDialog() {

        String title = "Open Photo";
        CharSequence[] itemlist = {"Take Photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, 1);
//                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
//                        startActivityForResult(intent, 1);
                        getItemImage(Sources.CAMERA);
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
//                        Intent intent2 = new Intent(Intent.ACTION_PICK);
//                        intent2.setType("image/*");
//                        intent2.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(Intent.createChooser(intent2,
//                                "Select Picture"), 2);
                        getItemImage(Sources.GALLERY);
                        break;
                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }

    public void getItemImage(Sources sources) {
        RxImagePicker.with(this).requestImage(sources)
                .flatMap(new Func1<Uri, Observable<File>>() {
                    @Override
                    public Observable<File> call(Uri uri) {
                        long millis = System.currentTimeMillis() % 1000;
                        return RxImageConverters.uriToFile(StoreUploadItemActivity.this, uri,
                                new File(new ContextWrapper(getApplicationContext()).getDir("imageDir", Context.MODE_PRIVATE), "store_item_photo"+millis+".png"));
                    }
                })
                .subscribe(new Action1<File>() {
                    @Override
                    public void call(final File file) {
                        // Do something with your file copy
                        itemImagePath = file.getPath();
                        Picasso.with(StoreUploadItemActivity.this).load(file).fit().into(itemImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.d("load image ", "success"+file.getAbsolutePath());
                            }

                            @Override
                            public void onError() {
                                Log.d("load image ", "Fail");
                            }
                        });
                    }
                });
    }

//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Bitmap adjustedBitmap = null;
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d("#######resultCode:", String.valueOf(resultCode));
//        if (resultCode == -1) {
//            if (data != null) {
//                Bitmap selectedBitmap = null;
//                if (requestCode == 1) {
//                    Bundle extras = data.getExtras();
//                    selectedBitmap = extras.getParcelable("data");
//                } else if (requestCode == 2) {
//                    try {
//                        selectedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
//                        selectedBitmap = rotateBitmap(selectedBitmap, -90);
//                        Log.d("#######selectedBitmap:", String.valueOf(selectedBitmap));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (selectedBitmap != null) {
//                    adjustedBitmap = ThumbnailUtils.extractThumbnail(selectedBitmap,
//                            100, 100);
//
//
//                    ContextWrapper cw = new ContextWrapper(getApplicationContext());
//                    // path to /data/data/yourapp/app_data/imageDir
//                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
//                    // Create imageDir
//                    long millis = System.currentTimeMillis() % 1000;
//                    String imageName = "profileImg" + millis + ".png";
//                    File mypath = new File(directory, imageName);
//
//                    adjustedBitmap = rotateBitmap(adjustedBitmap, 90);
//                    FileOutputStream fos = null;
//                    try {
//                        fos = new FileOutputStream(mypath);
//
//                        // Use the compress method on the BitMap object to write image to
//                        // the OutputStream
//                        adjustedBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
//                        fos.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    Picasso.with(this).load(mypath).into(itemImage);
//                    itemImagePath = mypath + "";
//                }
//            }
//        }
//    }


    private void submitStoreItem() {
        boolean submitCheck = checkData();
        if (submitCheck) {
            String token = "Token " + AppData.getInstance(this).auth_token;

            Intent i = new Intent(this, AppNetworkService.class);
            i.setAction(Actions.ADD_STORE_ITEM);
            i.putExtra(Constants.PHOTO_PATH, itemImagePath);
            i.putExtra(Constants.ITEM_NAME, itemName.getText().toString());
            i.putExtra(Constants.ITEM_PRICE, Double.parseDouble(itemPrice.getText().toString()));
            i.putExtra(Constants.ITEM_DESCP, itemDescp.getText().toString());
            i.putExtra(Constants.STORE_ID, AppData.getInstance(this).store_id);
            i.putExtra(Constants.AUTHENTICATION_TOKEN, token);
            startService(i);
        }
    }

    private boolean checkData() {
        if (itemImagePath != null && itemImagePath.equals("")) {
            Toast.makeText(this, "Please choose photo at first.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (itemName.getText().toString() != null && itemName.getText().toString().equals("")) {
            Toast.makeText(this, "Please enter name at first.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (itemPrice.getText().toString() != null && itemPrice.getText().toString().equals("")) {
            Toast.makeText(this, "Please enter price at first.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public class BroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.ADD_STORE_ITEM_DONE)) {
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
