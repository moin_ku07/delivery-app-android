package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.AppSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class WebViewActivity extends Activity  implements
        GoogleApiClient.ConnectionCallbacks ,GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    JSONObject authObj;
    String userType = "";
    Integer id = 0;
    public BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        //AppSettings appSettings = AppData.getInstance(getApplicationContext()).appSettings[0];
        //Log.d("******data*******", String.valueOf(appSettings.getVal()));
        String auth = RegisterActivity.readData("auth", getApplicationContext());
        try {
            authObj = new JSONObject(auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle bundle = getIntent().getExtras();
        userType = bundle.getString("user_type");
        id = bundle.getInt("id");
        Log.d("****userTypr****", String.valueOf(userType));

        /*web view */


        if(userType.equals("customer")){
            WebView webView=(WebView)findViewById(R.id.webView);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });

            receiver = new AllStoresFragmentBroadCastReciever();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Actions.GET_APP_SETTINGS_DONE);
            getApplicationContext().registerReceiver(receiver, filter);

            webView.getSettings().setJavaScriptEnabled(true);
            //List<AppSettings> appSettings = new ArrayList<>();
            AppSettings[] appSettings = AppData.getInstance(getApplicationContext()).appSettings;
            Log.d("****appSettings****", String.valueOf(appSettings));
            //for (AppSettings appSetting : AppData.getInstance(getApplicationContext()).appSettings) {

           /* int i=0;
            while (i< appSettings.length) {
                if (appSettings[i].getConfig().equals("survey")) {
                    webView.loadUrl(appSettings[i].getVal());
                    break;
                }
                i++;
            }
*/
            Button doneBtn = (Button) findViewById(R.id.doneBtn);
            doneBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent iinent = new Intent(WebViewActivity.this, TabMenuActivity.class);
                    startActivity(iinent);
                }
            });
        }else{
            Intent iinent= new Intent(WebViewActivity.this,TabMenuActivity.class);
            startActivity(iinent);
        }


        /*web view done*/


        if(userType.equals("driver")) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

                new AlertDialog.Builder(WebViewActivity.this)
                        .setTitle("Alert")
                        .setMessage("Location Service is Not Enabled.Please Enable..")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // whatever...
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        }).create().show();
            }
        }

        /*fuse location*/



        /*Check for location Service*/

        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            Log.d("****oncrt Connect****","Connected");
        } else {
            Log.d("****not connected***", "Not connected");
        }
        /*fuse location done*/


    }


    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        //Toast.makeText(this, "Failed to connect...", Toast.LENGTH_SHORT).show();
        Log.d("***connection failed***", "connection faild");

    }


    /*private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }*/

    @Override
    public void onConnected(Bundle bundle) {
        String token = null;
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.d("***connected***", String.valueOf(mLastLocation));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        if (mLastLocation != null) {
            String result = dateFormat.format(mLastLocation.getTime());
            Log.d("****loc****","Latitude: "+ String.valueOf(mLastLocation.getLatitude())+"Longitude: "+
                    String.valueOf(mLastLocation.getLongitude()) + " Time:" +  result);


            if(userType.equals("driver")){
                Log.d("*******driver*****", "yes");
                String postURL = RegisterActivity.rootURL(getApplicationContext()) + "api/driver-gps/";
                try {
                    token = "Token " + authObj.getString("auth_token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Builders.Any.B ionBuilder = Ion.with(getApplicationContext())
                        .load(postURL);
                ionBuilder.setHeader("Authorization", token)
                        .setBodyParameter("driver", String.valueOf(id))
                        .setBodyParameter("latitude", String.valueOf(mLastLocation.getLatitude()))
                        .setBodyParameter("longitude", String.valueOf(mLastLocation.getLongitude()))
                        .setBodyParameter("updated_at", String.valueOf(result))
                        .asString()
                        .setCallback(
                                (FutureCallback<String>) new FutureCallback<String>() {
                                    @Override
                                    public void onCompleted(final Exception e,
                                                            final String data) {
                                        Log.d("****Post_data****",data);
                                    }
                                });
            }


        }

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        Toast.makeText(this, "Connection suspended...", Toast.LENGTH_SHORT).show();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public class AllStoresFragmentBroadCastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_APP_SETTINGS_DONE)) {
                AppSettings[] appSettings = AppData.getInstance(getApplicationContext()).appSettings;
            }
        }
    }

}
