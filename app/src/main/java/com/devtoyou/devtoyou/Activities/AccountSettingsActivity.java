package com.devtoyou.devtoyou.Activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.Adapters.SexSpinnerAdapter;
import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.Dialogs.ChangePasswordDialog;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.User;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class AccountSettingsActivity extends FragmentActivity {

    private ListView list;

    JSONObject accountSettingObj;
    TextView title, phoneEdit, emailEdit, phoneText, emailText;
    ImageButton menu;
    ImageView back;
    JSONObject authObj;
    EditText firstName, lastName;
    android.app.AlertDialog.Builder alert;
    boolean is_active = false;
    private NotificationsSettingActivityBroadcastReceiver receiver;
    public Spinner sexSpinner;
    public User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_settings);

        receiver = new NotificationsSettingActivityBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.UPDATE_DEVICE_GCM_DONE);
        filter.addAction(Actions.GET_USER_INFO_DONE);
        filter.addAction(Actions.UPDATE_USER_PASSWORD);
        registerReceiver(receiver, filter);

        title = (TextView) findViewById(R.id.pageTitle);
        back = (ImageView) findViewById(R.id.backIcon);
        menu = (ImageButton) findViewById(R.id.menuBtn);
        phoneEdit = (TextView) findViewById(R.id.phone_editBtn);
        emailEdit = (TextView) findViewById(R.id.email_editBtn);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        phoneText = (TextView) findViewById(R.id.phnNo);
        emailText = (TextView) findViewById(R.id.email);
        sexSpinner = (Spinner) findViewById(R.id.sex_spinner);

        phoneEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneEditClick(v);
            }
        });

        emailEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailBtnClick(v);
            }
        });
        String sex_array[] = {"ذكر", "انثى"};
        SexSpinnerAdapter adapter = new SexSpinnerAdapter(this, 0, sex_array);
        adapter.setForAccountSettings(true);
        sexSpinner.setAdapter(adapter);

        menu.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
        title.setText("اٍعدادات الحساب");
        alert = new android.app.AlertDialog.Builder(AccountSettingsActivity.this);
        accountSettingObj = new JSONObject();
//        User user = AppData.getInstance(getApplicationContext()).user;

        getUserData();
        if (user != null)
            redraw();


        CustomPlainTextView changePassword = (CustomPlainTextView) findViewById(R.id.changePass);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog dialog = new ChangePasswordDialog();
                dialog.show(getSupportFragmentManager(), "change_password_dialog");
            }
        });
    }

    private void getUserData() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_USER_INFO);
        startService(i);
    }

    private void redraw() {
        if (user.getFirstName() != null
                && !user.getFirstName().equals("")
                && !user.getFirstName().equals("N/A"))
            firstName.setText(user.getFirstName());
        if (user.getLastName() != null
                && !user.getLastName().equals("")
                && !user.getLastName().equals("N/A"))
            lastName.setText(user.getLastName());
        if (user.getUsername() != null
                && !user.getUsername().equals("")
                && !user.getUsername().equals("N/A"))
            phoneText.setText(user.getUsername());
        if (user.getEmail() != null
                && !user.getEmail().equals("")
                && !user.getEmail().equals("N/A"))
            emailText.setText(user.getEmail());
        if (user.getGender() != null && !user.getGender().equals("")) {
            if (user.getGender().equalsIgnoreCase("m") || user.getGender().equalsIgnoreCase("male")) {
                sexSpinner.setSelection(0);
            } else {
                sexSpinner.setSelection(1);
            }
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccountSettingsActivity.this.finish();
            }
        });


        Switch notificationSwitch = (Switch) findViewById(R.id.notifications_switch);
        final SharedPreferences sharedPreferences = getSharedPreferences(Constants.GCM_TOKEN, 0);
        notificationSwitch.setChecked(sharedPreferences.getBoolean(Constants.NOTIFICATIONS_ON, false));

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String gcmToken = sharedPreferences.getString(Constants.GCM_TOKEN, "");
                String uuid = sharedPreferences.getString(Constants.UUID, "");
                Intent i = new Intent(AccountSettingsActivity.this, AppNetworkService.class);
                i.setAction(Actions.UPDATE_DEVICE_GCM);
                i.putExtra(Constants.GCM_TOKEN, gcmToken);
                i.putExtra(Constants.UUID, uuid);
                i.putExtra(Constants.ACTIVE_FLAG, isChecked);
                i.putExtra(Constants.AUTHENTICATION_TOKEN, AppData.getInstance(AccountSettingsActivity.this).auth_token);
                startService(i);
                is_active = isChecked;
            }
        });
    }

    private void editData() {
        String auth = RegisterActivity.readData("auth", AccountSettingsActivity.this);
        try {
            authObj = new JSONObject(auth);
            String token = "Token " + authObj.getString("auth_token");

            String userType = "";
            if (authObj.getString("user_type").equals("customer")) {
                userType = "customers";
            } else if (authObj.getString("user_type").equals("store_owner")) {
                userType = "stores";
            } else {
                userType = "drivers";
            }
            String url = RegisterActivity.rootURL(AccountSettingsActivity.this) + "api/" + userType + "/" + authObj.getString("id") + "/";
            Builders.Any.B ionBuilder = Ion.with(getApplicationContext())
                    .load("PATCH", url);
            Iterator<String> iter = accountSettingObj.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                if (!accountSettingObj.isNull(key)) {
                    ionBuilder.setMultipartParameter(key, accountSettingObj.getString(key));
                }
            }
            final Dialog loading = RegisterActivity.loadingView(AccountSettingsActivity.this);
            loading.show();
            ionBuilder.setHeader("Authorization", token)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            accountSettingObj = new JSONObject();
                            loading.dismiss();
                            Log.e("profile data ", result + "");
                            Log.e("profile error", e + "");
                            if (result != null && !result.equals("") && result.contains("id")) {
                                Toast.makeText(AccountSettingsActivity.this, "Account has been updated.", Toast.LENGTH_SHORT);
                                getUserData();
                            }
                        }

                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void phoneEditClick(View view) {
        edit("phone");
    }

    public void emailBtnClick(View view) {
        edit("email");
    }

    public void submitBtnClick(View view) {
        String first = firstName.getText().toString();
        String last = lastName.getText().toString();
        try {
            if (first != null && !first.equals(""))
                accountSettingObj.put("first_name", first);
            else
                accountSettingObj.put("first_name", "");

            if (last != null && !last.equals(""))
                accountSettingObj.put("last_name", last);
            else
                accountSettingObj.put("last_name", "");
            
            String sex = "";
            if (sexSpinner.getSelectedItem().equals("ذكر")) {
                sex = "M";
            } else {
                sex = "F";
            }
            accountSettingObj.put("gender", sex);
            editData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void edit(final String val) {
        final Dialog Viewdialog = new Dialog(AccountSettingsActivity.this);
        Viewdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Viewdialog.setContentView(R.layout.acc_settings_popup);
        Viewdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView headerTitle = (TextView) Viewdialog.findViewById(R.id.headerTitle);
        TextView headerText = (TextView) Viewdialog.findViewById(R.id.headerText);
        final EditText editText = (EditText) Viewdialog.findViewById(R.id.editText);

        if (val.equals("email")) {
            headerTitle.setText("Enter Email");
            headerText.setText("Please enter a valid email address");
            if (user != null
                    && (user.getEmail()
                    == null || user.getEmail().equals("N/A")))
                editText.setHint("example@gmail.com");
            else
                editText.setText(user.getEmail());
        } else {
            headerTitle.setText("Enter Phone No");
            headerText.setText("Please enter a valid phone number");
            if (user != null
                    && (user.getUsername()
                    == null || user.getUsername().equals("N/A")))
                editText.setHint("966570000000");
            else
                editText.setText(user.getUsername());
        }
        Viewdialog.show();
        Button cancel = (Button) Viewdialog.findViewById(R.id.cancelBtn);
        Button submit = (Button) Viewdialog.findViewById(R.id.submitBtn);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Viewdialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (val.equals("email")) {
                        accountSettingObj.put("email", editText.getText().toString());
                        editData();
                    } else {
                        accountSettingObj.put("username", editText.getText().toString());
                        editData();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Viewdialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.first, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.s
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class NotificationsSettingActivityBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.UPDATE_DEVICE_GCM_DONE)) {
                SharedPreferences.Editor editor = getSharedPreferences(Constants.GCM_TOKEN, 0).edit();
                editor.putBoolean(Constants.NOTIFICATIONS_ON, is_active);
                editor.commit();
                Toast.makeText(getApplicationContext(), "GCM Updated!", Toast.LENGTH_SHORT).show();
            } else if (action.equals(Actions.GET_USER_INFO_DONE)) {
                user = AppData.getInstance(getApplicationContext()).user;
                redraw();
            } else if (action.equals(Actions.UPDATE_USER_PASSWORD)) {
                try {
                    accountSettingObj.put("password", intent.getStringExtra(Constants.USER_NEW_PASSWORD));
                    editData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
