package com.devtoyou.devtoyou.Activities;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.AppData;

public class NavigatorActivity extends Activity {
    public String FIRST_TIME_FLAG = "FIRST_TIME_FLAG";
    public int back_press_counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        back_press_counter = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.first, menu);
        return true;
    }

    public void loginBtnClick(View view) {
        startActivity(new Intent(NavigatorActivity.this, LoginActivity.class));
    }

    public void registerBtnClick(View view) {
        //startActivity(new Intent(NavigatorActivity.this, UserHistoryFeedbackActivity.class));
        startActivity(new Intent(NavigatorActivity.this, RegisterActivity.class));
        // finish();
    }

    public void skipBtnClick(View view) {
        AppData.getInstance(NavigatorActivity.this).guestMode = true;
        AppData.getInstance(this).skipFlag = true;
        startActivity(new Intent(NavigatorActivity.this, TabMenuActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.s
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        back_press_counter++;
        if (back_press_counter == 2) {
            moveTaskToBack(true);
        }
    }
}
