package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Adapters.CustomAdapter;
import com.devtoyou.devtoyou.R;

import java.util.ArrayList;

public class HomePageActivity extends Activity {
    GridView gv;
    Context context;
    ArrayList prgmName;
    TextView title;
    ImageView logo, search;
    public static String [] prgmNameList={"Bonafide Restaurant","Srivas Restaurant","Grub Restaurants","Food Restaurant Drink","FoodCircles","Best Italian"};
    public static int [] prgmImages={R.drawable.bonafide,R.drawable.srivas,R.drawable.grub,R.drawable.food,R.drawable.circle,R.drawable.best};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_fragment);

        gv=(GridView) findViewById(R.id.gridview);
        gv.setAdapter(new CustomAdapter(this, prgmNameList, prgmImages));

        title=(TextView) findViewById(R.id.pageTitle);
        title.setVisibility(View.GONE);
        logo=(ImageView) findViewById(R.id.logo);
        logo.setVisibility(View.VISIBLE);
        search=(ImageView) findViewById(R.id.searchIcon);
        search.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.popular_stores, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void menuBtnClick(View view){

        final Dialog Viewdialog = new Dialog(this);
        Viewdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Viewdialog.setContentView(R.layout.home_page_menu);
        Viewdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Viewdialog.show();

        final Button delete = (Button) Viewdialog.findViewById(R.id.crossBtn);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Viewdialog.dismiss();
            }
        });

    }
}
