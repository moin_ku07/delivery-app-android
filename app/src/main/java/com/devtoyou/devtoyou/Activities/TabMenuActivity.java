package com.devtoyou.devtoyou.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Adapters.DriverPagerAdapter;
import com.devtoyou.devtoyou.Adapters.SectionsPagerAdapter;
import com.devtoyou.devtoyou.Adapters.SkipPagerAdapter;
import com.devtoyou.devtoyou.NetworkServices.DriverLocationService;
import com.devtoyou.devtoyou.NetworkServices.RegistrationIntentService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Constants;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

/**
 * Created by sharmin on 8/19/15.
 */

/**
 * This demonstrates how you can implement switching between the tabs of a
 * TabHost through fragments, using FragmentTabHost.
 */
public class TabMenuActivity extends FragmentActivity {
    public static final int REQUEST_READ_PHONE_STATE = 1;
    private FragmentTabHost mTabHost;
    public RegistrationIntentServiceBroadcastReceiver receiver;

    private ViewPager viewPager;
    //private MyPagerAdapter adapter;
    TextView winTitle, title;
    ScrollView sc;
    RelativeLayout ac;
    ImageView logo, search, back;
    public static ImageButton menu;
    RelativeLayout actionBar;


    //create object of FragmentPagerAdapter
    SectionsPagerAdapter mSectionsPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //check user is signed in or not
        if (!AppData.getInstance(this).guestMode) {
            if (AppData.getInstance(this).user_type == Constants.CUSTOMER && AppData.getInstance(this).user == null)
                startActivity(new Intent(this, SplashActivity.class));
        }

        setContentView(R.layout.tab);
        receiver = new RegistrationIntentServiceBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.UPDATE_DEVICE_GCM_DONE);
        registerReceiver(receiver, filter);

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.pager);
        if (viewPager != null)
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    Log.d("Tab position: ", position + "");
                    if (AppData.getInstance(TabMenuActivity.this).user_type != null &&
                            !AppData.getInstance(getApplicationContext()).user_type.equals("driver")) {
                        switch (position) {
                            case 0:
                                AppData.getInstance(getApplicationContext()).getAllStores();
                                AppData.getInstance(getApplicationContext()).getAllFavouriteStores();
                                break;
                            case 1:
                                AppData.getInstance(getApplicationContext()).getAllNotification();
                                break;
                            case 3:
                                AppData.getInstance(getApplicationContext()).getAllOrders();
                                break;
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.GCM_TOKEN, 0);
        if (!sharedPreferences.getBoolean(Constants.GCM_TOKEN_UPDATED_ON_SERVER, false)) {
            if (sharedPreferences.getString(Constants.GCM_TOKEN, "").equals("")) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            android.Manifest.permission.READ_PHONE_STATE)) {
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                REQUEST_READ_PHONE_STATE);
                    }
                }
            }
        }

        String user_type = AppData.getInstance(getApplicationContext()).user_type;
        if (user_type != null && user_type.equals("driver")) {
            viewPager.setAdapter(new DriverPagerAdapter(getSupportFragmentManager()));
            Intent i = new Intent(this, DriverLocationService.class);
            i.setAction(Actions.UPDATE_DRIVER_LOCATION);
            i.putExtra(Constants.USER_TYPE, user_type);
            i.putExtra(Constants.DRIVER_ID, AppData.getInstance(getApplicationContext()).user_id);
            startService(i);
        } else if (AppData.getInstance(this).skipFlag) {
            viewPager.setAdapter(new SkipPagerAdapter(getSupportFragmentManager()));
            findViewById(R.id.tabs).setVisibility(View.GONE);
        } else {
            mSectionsPagerAdapter = new SectionsPagerAdapter(
                    getSupportFragmentManager());
            viewPager.setAdapter(mSectionsPagerAdapter);
        }

        if (AppData.getInstance(this).skipFlag) {
            AppData.getInstance(getApplicationContext()).getAllStores();
            AppData.getInstance(getApplicationContext()).getAllFavouriteStores();
            AppData.getInstance(getApplicationContext()).getAllStoreItems();
        }

        // Give the PagerSlidingTabStrip the ViewPager
        SmartTabLayout tabsLayout = (SmartTabLayout) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        if (tabsLayout != null && viewPager != null)
            tabsLayout.setViewPager(viewPager);

        actionBar = (RelativeLayout)

                findViewById(R.id.actionBar);

        sc = (ScrollView)

                findViewById(R.id.scrollView);

        title = (TextView)

                findViewById(R.id.pageTitle);

        logo = (ImageView)

                findViewById(R.id.logo);

        search = (ImageView)

                findViewById(R.id.searchIcon);

        back = (ImageView)

                findViewById(R.id.backIcon);

        menu = (ImageButton)

                findViewById(R.id.menuBtn);

        search.setVisibility(View.VISIBLE);
        title.setVisibility(View.GONE);
        logo.setVisibility(View.VISIBLE);

        search.setOnClickListener(new View.OnClickListener()

                                  {
                                      @Override
                                      public void onClick(View view) {
                                          Intent intent = new Intent(TabMenuActivity.this, SearchActivity.class);
                                          startActivity(intent);
                                      }
                                  }

        );

        if (getIntent().getBooleanExtra(Constants.FROM_NOTIFICATIONS, false)) {
            // go to notifications tab
            viewPager.setCurrentItem(1);
            PendingIntent.getBroadcast(this, 0, getIntent(),
                    PendingIntent.FLAG_UPDATE_CURRENT).cancel();
        }
    }

    private void getGCMToken() {
        Intent getGCMToken = new Intent(this, RegistrationIntentService.class);
        startService(getGCMToken);
        Log.d("GCM: ", "get GCM token");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getGCMToken();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void menuBtnClick(final View view) {
        final Dialog viewdialog = new Dialog(this);
        viewdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        viewdialog.setContentView(R.layout.home_page_menu);
        viewdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        viewdialog.show();
        final Button delete = (Button) viewdialog.findViewById(R.id.crossBtn);
        halfSizingButton(this, delete, R.drawable.cross_home, 20, 20, "top");
        delete.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                viewdialog.dismiss();
            }


        });

        final Button allStores = (Button) viewdialog.findViewById(R.id.allSroteBtn);
        final Button allFavStores = (Button) viewdialog.findViewById(R.id.popStoreBtn);
        allStores.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Actions.SHOW_ALL_STORES_FRAGMENT);
                sendBroadcast(i);
                viewdialog.hide();
            }
        });

        allFavStores.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Actions.SHOW_POPULAR_STORES);
                sendBroadcast(i);
                viewdialog.hide();
            }
        });

        Button aboutBtn = (Button) viewdialog.findViewById(R.id.aboutBtn);
        aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iinent = new Intent(TabMenuActivity.this, AboutActivity.class);
                startActivity(iinent);
            }
        });

        Button popItemBtn = (Button) viewdialog.findViewById(R.id.popItemBtn);
        popItemBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Actions.SHOW_FAVORITE_ITEMS_FRAGMENT);
                sendBroadcast(intent);
                viewdialog.hide();
            }
        });
        if (AppData.getInstance(this).skipFlag) {
            Button registerButton = (Button) viewdialog.findViewById(R.id.registerBtn);
            registerButton.setVisibility(View.VISIBLE);
            registerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(TabMenuActivity.this, RegisterActivity.class));
                }
            });

            Button loginActivity = (Button) viewdialog.findViewById(R.id.siginBtn);
            loginActivity.setVisibility(View.VISIBLE);
            loginActivity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(TabMenuActivity.this, LoginActivity.class));
                }
            });
        }
    }

    public static void halfSizingButton(Activity act, Button btn, int image, int width, int height, String side) {
        Drawable drawable = act.getResources().getDrawable(image);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5), (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, width, height);
        btn.setCompoundDrawables(side.equals("left") ? sd.getDrawable() : null, side.equals("top") ? sd.getDrawable() : null, side.equals("right") ? sd.getDrawable() : null, side.equals("bottom") ? sd.getDrawable() : null);
    }


    @Override
    protected void onPause() {
        super.onPause();
        List<CartItem> cartItems = AppData.getInstance(getApplicationContext()).cartItems;
        if (cartItems != null && cartItems.size() > 0) {
            Gson gson = new Gson();
            String cartItemsStr = gson.toJson(cartItems);
            Log.d("cart items string", cartItemsStr);
            AppData.getInstance(getApplicationContext()).storeCartItems(cartItemsStr);
        } else {
            AppData.getInstance(getApplicationContext()).storeCartItems("");
        }
    }

    @Override
    protected void onDestroy() {
        AppData.getInstance(this).guestMode = false;
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public class RegistrationIntentServiceBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.UPDATE_DEVICE_GCM_DONE)) {
                SharedPreferences.Editor editor = getSharedPreferences(Constants.GCM_TOKEN, 0).edit();
                editor.putBoolean(Constants.GCM_TOKEN_UPDATED_ON_SERVER, true);
                editor.putBoolean(Constants.NOTIFICATIONS_ON, true);
                editor.commit();
                Log.d("GCM: ", "Server updated");
            }
        }
    }
}