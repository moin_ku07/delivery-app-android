package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.util.Log;

import com.devtoyou.devtoyou.Adapters.SearchListAdapter;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.SearchResultItem;
import com.google.gson.Gson;

/**
 * Created by Durlov-L3 on 12/14/2015.
 */
public class SearchActivity extends Activity {
    public SearchView searchView;
    public ListView listView;
    public SearchListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d("Search: ", query);
                Intent i = new Intent(SearchActivity.this, AppNetworkService.class);
                i.setAction(Actions.SEARCH_BY_QUERY);
                i.putExtra(Constants.SEARCH_QUERY, query);
                startService(i);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        TextView cancelBtn = (TextView) findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        adapter = new SearchListAdapter(this, 0, new SearchResultItem[0]);
        listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, getIntentFilter());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    //    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        }
//
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            //This sets a textview to the current length
////            Button btn = (Button) findViewById(R.id.clear_txt);
////            if(s.length() > 0){
////                btn.setVisibility(View.VISIBLE);
////                //String text = search.getText().toString().toLowerCase(Locale.getDefault());
////                //((InteractiveArrayAdapter) adapter).filter(text);
////            }else{
////                btn.setVisibility(View.GONE);
////            }
//
//            Log.d("Search: ", s.toString());
//        }
//
//        public void afterTextChanged(Editable s) {
//        }
//    };

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.SEARCH_BY_QUERY_DONE)) {
                String searchResultStr = AppCache.cache.get(Constants.SEARCH_QUERY_RESULT);
                SearchResultItem[] searchResultItems = new Gson().fromJson(searchResultStr, SearchResultItem[].class);
                if (searchResultItems.length > 0) {
                    adapter = new SearchListAdapter(SearchActivity.this, 0 , searchResultItems);
                    listView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        }
    };

    public IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Actions.SEARCH_BY_QUERY_DONE);
        return intentFilter;
    }
}
