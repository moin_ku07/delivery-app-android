package com.devtoyou.devtoyou.Activities;

import android.app.*;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.devtoyou.devtoyou.R;

public class GotoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        /*if (AppCache.cache.get("first_time_flag") == null) {
            startActivity(new Intent(GotoActivity.this, ScreenSlideActivity.class));
            AppCache.cache.put("first_time_flag", "true");
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.first, menu);
        return true;
    }
    public void loginBtnClick(View view){
        startActivity(new Intent(GotoActivity.this, LoginActivity.class));
//        startActivity(new Intent(NavigatorActivity.this, LoginActivity.class));
//        finish();
    }
    public void registerBtnClick(View view){
        startActivity(new Intent(GotoActivity.this,RegisterActivity.class));
        // startActivity(new Intent(NavigatorActivity.this,RegisterActivity.class));
        // finish();
    }

    public void skipBtnClick(View view){
        startActivity(new Intent(GotoActivity.this,TabMenuActivity.class));
//
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.s
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
