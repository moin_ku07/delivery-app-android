package com.devtoyou.devtoyou.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.OrderDriverLocation;
import com.devtoyou.devtoyou.models.OrderStatus;
import com.devtoyou.devtoyou.models.StoreBranch;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.HashMap;

/**
 * Created by motaz on 17/01/16.
 */
public class ShowOrderAdressActivity extends FragmentActivity {
    public DriverOrder order;
    public OrderStatus orderStatus;
    public SupportMapFragment mapFragment;
    public ShowOrderAddressActivityBroadCastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_order_address_activity);

        receiver = new ShowOrderAddressActivityBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ORDER_STATUS_DONE);
        registerReceiver(receiver, filter);

        order = AppData.getInstance(getApplicationContext()).current_driver_order;

        ImageView backIcon = (ImageView) findViewById(R.id.backIcon);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getDriverLocation();
        setupMapIfNeeded();
    }

    private void setupMapIfNeeded() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                loadMap(googleMap);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            }
        });
    }

    private void loadMap(GoogleMap map) {
        if (map != null) {
            LatLng allStoresLocations[] = getAllStores();
            for (int i = 0; i < allStoresLocations.length; i++) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(allStoresLocations[i]);
                markerOptions.title("Store");
                Marker marker = map.addMarker(markerOptions);
            }
            Marker driverMarker;
            if (orderStatus != null)
                if (orderStatus.getDriverLocation() != null & orderStatus.getDriverLocation().size() > 0) {
                    OrderDriverLocation location = orderStatus.getDriverLocation().get(0);
                    LatLng driverLocation = new LatLng(Double.parseDouble(location.getLat())
                            , Double.parseDouble(location.getLng()));

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(driverLocation);
                    markerOptions.title("Driver");
                    driverMarker = map.addMarker(markerOptions);
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLocation, 14));
                } else {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(allStoresLocations[0], 14));
                }
        }

    }


    private LatLng[] getAllStores() {
        HashMap<String, LatLng> allstores = new HashMap<>();
        for (int i = 0; i < order.getItems().size(); i++) {
            if (!allstores.containsKey(order.getItems().get(i))) {
                if (order.getItems().get(i).getStoreBranches() != null && order.getItems().get(i).getStoreBranches().get(0) != null) {
                    StoreBranch storeBranch = order.getItems().get(i).getStoreBranches().get(0);
                    LatLng latLng = new LatLng(Double.parseDouble(storeBranch.getLatitude())
                            , Double.parseDouble(storeBranch.getLongitude()));
                    allstores.put(order.getItems().get(i).getStoreName(), latLng);
                }
            }
        }
        return allstores.values().toArray(new LatLng[allstores.values().size()]);
    }

    public void getDriverLocation() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_ORDER_STATUS);
        i.putExtra(Constants.ORDER_ID, order.getId());
        startService(i);
    }

    public class ShowOrderAddressActivityBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ORDER_STATUS_DONE)) {
                orderStatus = AppData.getInstance(getApplicationContext()).orderStatus;
                setupMapIfNeeded();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
