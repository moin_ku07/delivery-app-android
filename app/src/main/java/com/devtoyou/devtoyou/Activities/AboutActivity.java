package com.devtoyou.devtoyou.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.devtoyou.devtoyou.R;

public class AboutActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        ImageButton backbtn = (ImageButton) findViewById(R.id.backIcon);
        Button mailBtn = (Button) findViewById(R.id.about_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        mailBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                String emailAddress = "contactus@devtoyou.com";
                String subject = "Contact Us";
                String text = "";
                i.setType("message/rfc822");
                i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailAddress});
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
                i.putExtra(android.content.Intent.EXTRA_TEXT, text);
                startActivity(Intent.createChooser(i, "Send email"));
            }
        });


    }

}
