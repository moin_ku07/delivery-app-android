package com.devtoyou.devtoyou.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Dialogs.UserFeedbackDialog;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Order;
import com.devtoyou.devtoyou.models.OrderItem;
import com.devtoyou.devtoyou.models.OrdersHistory;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by motaz on 11/2/15.
 */
public class UserHistoryActivty extends Activity {
    public ListView listView;
    public View rootView;
    public View view;
    JSONObject authObj;
    Dialog loading;
    AlertDialog.Builder alert;
    Integer selectedStoreID = null;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_history_activity);
        listView = (ListView) findViewById(R.id.user_history_list);
        UserHistoryListAdapter listadapter = new UserHistoryListAdapter(getApplicationContext(), AppData.getInstance(getApplicationContext()).allOrdersHistory);
        listView.setAdapter(listadapter);

        String auth = RegisterActivity.readData("auth", getApplicationContext());
        try {
            authObj = new JSONObject(auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        alert = new AlertDialog.Builder(UserHistoryActivty.this);
    }

    public void userHistoryBackBtn(View view) {
        UserHistoryActivty.this.finish();
    }

    public class UserHistoryListAdapter extends ArrayAdapter {
        private final Context context;
        private OrdersHistory[] ordersHistories;
        private Order[] values;

        public UserHistoryListAdapter(Context context, OrdersHistory[] ordersHistories) {
            super(context, R.layout.user_history_list_item);
            this.context = context;
            this.ordersHistories = ordersHistories;
        }

        @Override
        public int getCount() {
            return ordersHistories.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.user_history_list_item, parent, false);
            if (AppData.getInstance(getApplicationContext()).allOrdersHistory != null) {
                Button status = (Button) rowView.findViewById(R.id.delivered_button);
                TextView orderDetailsTitle = (TextView) rowView.findViewById(R.id.order_details);
                Button feedback_btn = (Button) rowView.findViewById(R.id.history_feedback_btn);
                Button upload_btn = (Button) rowView.findViewById(R.id.history_upload_btn);

                //Object[] mStringArray = arr.toArray();
                //Log.d("this is my array", "arr: " + Arrays.toString(mStringArray));

                /*................Upload image to server............*/

                upload_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<OrderItem> items = ordersHistories[position].getItems();
                        List<String> storeNames = new ArrayList<String>();
                        final List<Integer> storeIDs = new ArrayList<Integer>();

                        for (int i = 0; i < items.size(); i++) {
                            OrderItem item = items.get(i);
                            if (!storeNames.contains(item.getStoreName())) {
                                storeNames.add(item.getStoreName());
                                storeIDs.add(item.getStoreId());
                            }
                        }

                        String title = "Select Store";
                        CharSequence[] itemlist = storeNames.toArray(new CharSequence[storeNames.size()]);
                        AlertDialog.Builder builder = new AlertDialog.Builder(UserHistoryActivty.this);
                        builder.setTitle(title);
                        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedStoreID = storeIDs.get(which);
                                showImageCaptureDialog();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.setCancelable(true);
                        alert.show();
                    }
                });

                /*............End............*/

                TextView itemDetails = (TextView) rowView.findViewById(R.id.item_details);
                status.setText(getStatusTextFromCode(ordersHistories[position].getStatus()));
                orderDetailsTitle.setText(ordersHistories[position].getDisplayId() + "#");
                itemDetails.setText(getOrderItemsText(ordersHistories[position]));

                if (ordersHistories[position].getStatus().equals("DELIV")) {
                    feedback_btn.setVisibility(View.VISIBLE);
                    upload_btn.setVisibility(View.VISIBLE);
                }

                try {
                    String dateTime = ordersHistories[position].getCreatedAt();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date oldDate = dateFormat.parse(dateTime);
                    TextView deliveryTime = (TextView) rowView.findViewById(R.id.delivery_time);
                    TextView deliveryDate = (TextView) rowView.findViewById(R.id.delivery_date);
                    String deliveryTimeFormat = (String) android.text.format.DateFormat.format("hh:mm aa", oldDate);
                    String deliveryDateFormat = (String) android.text.format.DateFormat.format("dd MMM, yyyy", oldDate);
                    deliveryTime.setText(deliveryTimeFormat);
                    deliveryDate.setText(deliveryDateFormat);
                } catch (ParseException e) {

                    e.printStackTrace();
                }


                status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserFeedbackDialog dialog = UserFeedbackDialog.newInstance("");
                        FragmentManager fragmentManager = getFragmentManager();
                        dialog.show(fragmentManager, "User feedback form");
                    }
                });

                feedback_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent iinent = new Intent(UserHistoryActivty.this, UserHistoryFeedbackActivity.class);
                        iinent.putExtra("display_id", ordersHistories[position].getDisplayId());
                        //Log.d("####orderDetailhistory", String.valueOf(ordersHistories[position].getItems()));
                        //iinent.putExtra("orderDetail", (Parcelable) ordersHistories[position].getItems());
                        iinent.putExtra("orderID", ordersHistories[position].getId());
                        startActivity(iinent);
                    }
                });
            }

            return rowView;
        }

        /* Show alert to select / take images*/

        private void showImageCaptureDialog() {

            String title = "Open Photo";
            CharSequence[] itemlist = {"Take Photo", "Choose from Gallery"};


            AlertDialog.Builder builder = new AlertDialog.Builder(UserHistoryActivty.this);
            builder.setTitle(title);
            builder.setItems(itemlist, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0:// Take Photo
                            // Do Take Photo task here
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, 1);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                            startActivityForResult(intent, 1);
                            break;
                        case 1:// Choose Existing Photo
                            // Do Pick Photo task here
                            Intent intent2 = new Intent(Intent.ACTION_PICK);
                            intent2.setType("image/*");
                            intent2.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent2,
                                    "Select Picture"), 1);
                            break;
                        default:
                            break;
                    }
                }
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(true);
            alert.show();
        }

        private String getOrderItemsText(OrdersHistory order) {
            String orderItemsText = "";
            for (int i = 0; i < order.getItems().size(); i++) {
                OrderItem item = order.getItems().get(i);
                orderItemsText += item.getItemName() + " * " + item.getQuantity() + " من " + item.getStoreName();
                if (i != order.getItems().size() - 1) {
                    orderItemsText += "\n";
                }
            }
            return orderItemsText;
        }

        /*private ArrayList<String> myNumbers(OrdersHistory order)    {
            ArrayList<String> numbers = new ArrayList<String>();
            for (int i = 0; i < order.getItems().size(); i++) {
                OrderItem item = order.getItems().get(i);
                numbers.add("Store-" + item.getStoreId());
            }
            return(numbers);
        }*/

    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedImage = data.getData();

                String filePath = getPath(selectedImage);
                String file_extn = filePath.substring(filePath.lastIndexOf(".") + 1);
                Log.d("===File Path===", filePath);
                File file = scaleImage(filePath);
                if (file != null){
                    try {
                        //loading.show();
                        final ProgressDialog progress = new ProgressDialog(this);
                        progress.setTitle("Loading");
                        progress.setMessage("Image is posting to server...");
                        progress.show();
                        String token = null;
                        if (file_extn.equals("img") || file_extn.equals("jpg") || file_extn.equals("jpeg") || file_extn.equals("gif") || file_extn.equals("png")) {
                            Log.e("=======photo image ", filePath + "");

                            String postURL = RegisterActivity.rootURL(getApplicationContext()) + "api/store-photos/";
                            Log.d("postURL: ", postURL);
                            token = "Token " + authObj.getString("auth_token");
                            //String uploadImageResult = "";
                            Builders.Any.B ionBuilder = Ion.with(getApplicationContext())
                                    .load(postURL);
                            ionBuilder.setHeader("Authorization", token)
                                    .setMultipartFile("photo", file)
                                    .setMultipartParameter("store", String.valueOf(selectedStoreID))
                                    .asString()
                                    .setCallback(
                                            (FutureCallback<String>) new FutureCallback<String>() {
                                                @Override
                                                public void onCompleted(final Exception e,
                                                                        final String uploadImageResult) {
                                                    //  loading.dismiss();
                                                    progress.dismiss();
                                                    try {
                                                        JSONObject result = new JSONObject(uploadImageResult);
                                                        if (result.getInt("id") > 0) {
                                                            alert.setTitle("تنبيه");
                                                            alert.setMessage("تم بنجاح");
                                                            alert.setCancelable(false)
                                                            .setNegativeButton("حسنا", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                }
                                                            });
                                                            AlertDialog dialog = alert.create();
                                                            dialog.show();
                                                        }
                                                    } catch (JSONException e1) {
                                                        Log.d("uploadJSONParseErr: ", "");
                                                        e1.printStackTrace();
                                                        alert.setTitle("خطأ");
                                                        alert.setMessage("حاول مرة اخرى");
                                                        alert.setCancelable(false)
                                                        .setNegativeButton("حسنا", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                        AlertDialog dialog = alert.create();
                                                        dialog.show();
                                                    }
                                                }

                                            });
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        Log.d("########Error##########", "");
                        e.printStackTrace();
                    }
                }else{
                    alert.setTitle( "خطأ");
                    alert.setMessage("حاول مرة اخرى");
                    alert.show();
                }
            }
        }
    }

    public File scaleImage(String filePath) {
        Bitmap adjustedBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

       adjustedBitmap = ThumbnailUtils.extractThumbnail(bitmap,
                100, 100);


        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "storephoto.png");
        //Picasso.with(getActivity()).load(new File(mypath.toURI())).fit().transform(new CircleTransform()).into(profileImage);
        adjustedBitmap = RotateBitmap(adjustedBitmap, 90);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to
            // the OutputStream
            adjustedBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return  mypath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String imagePath = cursor.getString(column_index);

        return cursor.getString(column_index);
    }


    public String getStatusTextFromCode(String code) {
        String fullText = "";
        switch (code) {
            case "REQ":
                fullText = "طلب";
                break;
            case "REV":
                fullText = "مراجعة";
                break;
            case "APPR":
                fullText = "معتمد";
                break;
            case "AD":
                fullText = "تم اسناده للمندوب";
                break;
            case "DOTW":
                fullText = "المندوب في الطريق";
                break;
            case "DIS":
                fullText = "المندوب في المتجر";
                break;
            case "DLS":
                fullText = "المندوب غادر المتجر";
                break;
            case "DELIV":
                fullText = "تم التوصيل";
                break;
            case "MCL":
                fullText = "مغلق";
                break;
            case "UCAN":
                fullText = "ملغي";
                break;
            case "MREJ":
                fullText = "مرفوض";
                break;
        }
        return fullText;
    }

    public class AllNotificationsFragmentBroadCastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_ORDERS_HISTORY_DONE)) {
                ListView lv = (ListView) view.findViewById(R.id.user_history_list);
                UserHistoryListAdapter listadapter = new UserHistoryListAdapter(getApplicationContext(), AppData.getInstance(getApplicationContext()).allOrdersHistory);
                lv.setAdapter(listadapter);
            } /*else if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
                    loadingProgressDialog.cancel();
                    showStoreItems(lastClickedPosition);
                }
            }*/
        }
    }

    public class ArrayAdapterWithIcon extends ArrayAdapter<String> {

        private List<Integer> images;

        public ArrayAdapterWithIcon(Context context, List<String> items, List<Integer> images) {
            super(context, android.R.layout.select_dialog_item, items);
            this.images = images;
        }

        public ArrayAdapterWithIcon(Context context, String[] items, Integer[] images) {
            super(context, android.R.layout.select_dialog_item, items);
            this.images = Arrays.asList(images);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setCompoundDrawablesWithIntrinsicBounds(images.get(position), 0, 0, 0);
            textView.setCompoundDrawablePadding(
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getContext().getResources().getDisplayMetrics()));
            return view;
        }

    }


}

