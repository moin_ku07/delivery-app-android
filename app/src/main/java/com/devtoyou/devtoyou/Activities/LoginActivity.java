package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devtoyou.devtoyou.CustomViews.CustomButtonView;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;


public class LoginActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {

    LinearLayout twitterBtn, googlePlusBtn;
    public CustomButtonView loginButton;
    TwitterAuthClient mTwitterAuthClient;
    public LoginActivityBroadCastReceiver receiver;

    public static int RC_SIGN_IN = 2;
    public GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        receiver = new LoginActivityBroadCastReceiver();
        IntentFilter filter = new IntentFilter();

        filter.addAction(Actions.LOGIN_USING_SOCIAL_ACCOUNT_DONE);
        registerReceiver(receiver, filter);

        twitterBtn = (LinearLayout) findViewById(R.id.twitterBtn);
        googlePlusBtn = (LinearLayout) findViewById(R.id.google_plus);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        googlePlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        mTwitterAuthClient = new TwitterAuthClient();
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTwitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

                    @Override
                    public void onResponse(Response<TwitterSession> response, Retrofit retrofit) {

                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }

                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        // Success
                        Log.d("Twitter", "susccess: " + twitterSessionResult.data.getAuthToken() + " user ID:" + twitterSessionResult.data.getUserId() + twitterSessionResult.data.getUserName());
                        loginUsingSocialAccount(twitterSessionResult.data.getAuthToken().toString()
                                , twitterSessionResult.data.getUserId(), "twitter");
                    }

                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                    }
                });

            }
        });


        EditText passwordText = (EditText) findViewById(R.id.passText);
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                }
                return false;
            }
        });

        loginButton = (CustomButtonView) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void loginUsingSocialAccount(String auth_token, long user_id, String service) {
        Intent i = new Intent(LoginActivity.this, AppNetworkService.class);
        i.setAction(Actions.LOGIN_USING_SOCIAL_ACCOUNT);
        i.putExtra(Constants.ACCESS_TOKEN, auth_token);
        i.putExtra(Constants.USER_ID, user_id);
        i.putExtra(Constants.SERVICE_NAME, service);
        i.putExtra(Constants.USER_TYPE, "customer");
        startService(i);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Google sign in:", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("google user info: ", acct.getId() + "-" + acct.getServerAuthCode() + "-" + acct.getEmail());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void registerBtnClick(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void skipBtnClick(View view) {
        Log.d("Skip Button", "Pressed");

        AppData.getInstance(this).guestMode = true;
        startActivity(new Intent(this, TabMenuActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login() {
        EditText phonenum = (EditText) findViewById(R.id.emailText);
        EditText password = (EditText) findViewById(R.id.passText);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final String phnStr = phonenum.getText().toString();
        final String passStr = password.getText().toString();

        if (phnStr.matches("") || passStr.matches("")) {
            alert.setTitle("Alert");
            alert.setMessage("Please, fill up all the fields.");
            alert.setPositiveButton("OK", null);
            alert.show();
            return;
        }
        final Dialog loading = RegisterActivity.loadingView(LoginActivity.this);

        loading.show();
        if (!!RegisterActivity.netWorkChecking(LoginActivity.this)) {
            String logUrl = RegisterActivity.rootURL(LoginActivity.this) + "auth/multi/login";

            try {
                Ion.with(getApplicationContext()).
                        load(logUrl)
                        .setBodyParameter("username", phnStr)
                        .setBodyParameter("password", passStr)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                Log.e("login result ", result + "");
                                loading.dismiss();
                                if (result.get("status").getAsBoolean()) {
                                    RegisterActivity.StoreData("username", phnStr, LoginActivity.this);
                                    RegisterActivity.StoreData("password", passStr, LoginActivity.this);
                                    loginDoneCallback(result);
                                } else {
                                    RegisterActivity.alertDialog(alert, "Alert", "Please enter valid username or password");
                                }

                            }

                        });
            } catch (Exception e) {
                RegisterActivity.alertDialog(alert, "Alert", "Could not connect to the  Server. Please try again later");
            }
        } else {
            RegisterActivity.alertDialog(alert, "Alert", "No Network Connection Available. Please Connect to Internet to use DEVTOU App!");
        }

    }

    private void loginDoneCallback(JsonObject result) {
        RegisterActivity.StoreData("auth", result.toString(), LoginActivity.this);

        AppData.getInstance(getApplicationContext()).user_type = result.get("user_type").getAsString();
        Log.d("User type: ", AppData.getInstance(getApplicationContext()).user_type);
        AppData.getInstance(getApplicationContext()).user_id = result.get("id").getAsInt();
        AppData.getInstance(getApplicationContext()).auth_token = result.get("auth_token").getAsString();

        if (AppData.getInstance(getApplicationContext()).user_type.equals("store_owner")) {
            AppData.getInstance(getApplicationContext()).store_id = result.get("store_id").getAsInt();
            Log.d("User store id: ", AppData.getInstance(getApplicationContext()).store_id + "");
        }

        AppData.getInstance(getApplicationContext()).skipFlag = false;
        AppData.getInstance(getApplicationContext()).init();
        finish();

        //Intent i = new Intent(LoginActivity.this, TabMenuActivity.class);
        Intent i = new Intent(LoginActivity.this, WebViewActivity.class);
        i.putExtra("user_type", result.get("user_type").getAsString());
        i.putExtra("id", result.get("id").getAsInt());
        startActivity(i);

    }

    public void loginDoneCallback(String user) {
        Gson gson = new Gson();
        com.devtoyou.devtoyou.models.User result = gson.fromJson(user, com.devtoyou.devtoyou.models.User.class);

        RegisterActivity.StoreData("auth", user, LoginActivity.this);

        AppData.getInstance(getApplicationContext()).user_type = result.getUserType();
        Log.d("User type: ", AppData.getInstance(getApplicationContext()).user_type);
        AppData.getInstance(getApplicationContext()).user_id = result.getId();
        AppData.getInstance(getApplicationContext()).auth_token = result.auth_token;

        if (AppData.getInstance(getApplicationContext()).user_type.equals("store_owner")) {
            AppData.getInstance(getApplicationContext()).store_id = result.getStore_id();
            Log.d("User store id: ", AppData.getInstance(getApplicationContext()).store_id + "");
        }

        AppData.getInstance(getApplicationContext()).skipFlag = false;
        AppData.getInstance(getApplicationContext()).init();
        finish();

        //Intent i = new Intent(LoginActivity.this, TabMenuActivity.class);
        Intent i = new Intent(LoginActivity.this, WebViewActivity.class);
        i.putExtra("user_type", result.getUserType());
        i.putExtra("id", result.getId());
        startActivity(i);

    }

    //for using 2x image with 1x size in drawable button
    public static void halfSizingButton(Activity act, Button btn, int image, int width, int height, String side) {
        Drawable drawable = act.getResources().getDrawable(image);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5), (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, width, height);
        btn.setCompoundDrawables(side.equals("left") ? sd.getDrawable() : null, side.equals("top") ? sd.getDrawable() : null, side.equals("right") ? sd.getDrawable() : null, side.equals("bottom") ? sd.getDrawable() : null);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class LoginActivityBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.LOGIN_USING_SOCIAL_ACCOUNT_DONE)) {
                String user = AppCache.cache.get(Constants.USER);
                loginDoneCallback(user);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
