package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.PlaceOrderResponse;
import com.google.gson.Gson;

/**
 * Created by Durlov-L3 on 12/23/2015.
 */
public class PlaceOrderDoneActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_done_dialog);
        ImageView close = (ImageView) findViewById(R.id.closeBtn);
        TextView orderId = (TextView) findViewById(R.id.order_id);
        Gson gson = new Gson();
        PlaceOrderResponse response = gson.fromJson(AppCache.cache.get(Actions.PLACE_ORDER_DONE), PlaceOrderResponse.class);
        orderId.setText("#" + response.getDisplayId());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
