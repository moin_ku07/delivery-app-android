package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.devtoyou.devtoyou.Adapters.SexSpinnerAdapter;
import com.devtoyou.devtoyou.CustomViews.CircleTransform;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.AppData;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class RegisterActivity extends Activity {
    private JSONObject loc;
    Dialog loading;
    AlertDialog.Builder alert;
    JSONObject authObj;
    Boolean takenProfileImage = false;
    ImageView profilePic;
    public Spinner sexSpinner;
    public String gender = "f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        EditText phnText = (EditText) findViewById(R.id.phnText);
        EditText passText = (EditText) findViewById(R.id.passText);
        EditText emailText = (EditText) findViewById(R.id.emailText);
        Button imgBtn = (Button) findViewById(R.id.imgBtn);
        profilePic = (ImageView) findViewById(R.id.image);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk == android.os.Build.VERSION_CODES.JELLY_BEAN) {
            imgBtn.setGravity(Gravity.RIGHT);
            passText.setGravity(Gravity.RIGHT);
            phnText.setGravity(Gravity.RIGHT);
            emailText.setGravity(Gravity.RIGHT);
            phnText.setPadding(15, 15, 15, 15);
            passText.setPadding(15, 15, 15, 15);
            emailText.setPadding(15, 15, 15, 15);
        }

        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageCaptureDialog();
            }
        });
        sexSpinner = (Spinner) findViewById(R.id.sex_spinner);
        final String sex_array[] = {"ذكر", "انثى"};
        SexSpinnerAdapter adapter = new SexSpinnerAdapter(this, 0, sex_array);
        sexSpinner.setAdapter(adapter);
        sexSpinner.setSelection(1);
        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getSelectedItem().equals("انثى")) {
                    gender = "f";
                } else {
                    gender = "m";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    public void loginBtnClick(View view) {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        // startActivity(new Intent(NavigatorActivity.this,RegisterActivity.class));
        // finish();
    }

    public void skipBtnClick(View view) {
        AppData.getInstance(this).guestMode = true;
        startActivity(new Intent(RegisterActivity.this, TabMenuActivity.class));
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private boolean isValidMobile(String phone) {

        String expression = "^(009665|9665|\\+9665|05|\\d)(5|0|3|6|4|9|1|8|7)([0-9]{7})$";
        CharSequence inputStr = phone;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return (matcher.matches()) ? true : false;


//        String regexStr = "^[0-9]$";
//        String number=phone.toString();
//        if(number.length()<10 || number.length()>13 || number.matches(regexStr)==false ) {
////            Toast.makeText(MyDialog.this, "Please enter " + "\n" + " valid phone number", Toast.LENGTH_SHORT).show(); // am_checked=0; }`
//            return true;
//        }else{
//            return false;
//        }

//        String number = phone.toString();
//
//        if(number != null && number.matches("966[0-9]{9}"))
//            return true;
//        else
//            return false;

    }

    public static String rootURL(Context act) {
        Resources resources = act.getResources();
        AssetManager assetManager = resources.getAssets();
        String url = null;
        try {
            InputStream inputStream = assetManager.open("config.properties");
            Properties config = new Properties();
            config.load(inputStream);
            url = config.getProperty("root");// + "users/login";
        } catch (IOException e) {
            System.err.println("Failed to open microlog property file");
            e.printStackTrace();
        }
        return url;
    }


    public void registerClick(View view) {
        EditText email = (EditText) findViewById(R.id.emailText);
        EditText password = (EditText) findViewById(R.id.passText);
        EditText phoneNo = (EditText) findViewById(R.id.phnText);
        //EditText location = (EditText) findViewById(R.id.locationText);


        final AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
        String emailStr = email.getText().toString();
        final String passStr = password.getText().toString();
        final String phnStr = phoneNo.getText().toString();
        // String locStr = location.getText().toString();

        if (passStr.matches("") || phnStr.matches("")) {
            alertDialog(alert, "Alert", "Please fill up all field.");
            return;
        }
        if (!emailStr.matches("") && !isEmailValid(email.getText().toString())) {
            alertDialog(alert, "Alert", "Incorrect email address, please try again.");
            return;
        }
        if (!isValidMobile(phoneNo.getText().toString())) {
            alertDialog(alert, "Alert", "Incorrect phone no, please try again.");
            return;
        }
        final Dialog loading = loadingView(RegisterActivity.this);
        loading.show();
        if (!!netWorkChecking(RegisterActivity.this)) {
            final String regUrl = rootURL(RegisterActivity.this) + "auth/multi/register";
            Log.d("****Url***", regUrl);
            try {
                JsonObject json = new JsonObject();
                if (emailStr.length() > 0) {
                    json.addProperty("email", emailStr);
                }
                json.addProperty("user_type", "customer");
                json.addProperty("username", phnStr);
                json.addProperty("password", passStr);
                json.addProperty("gender", gender);

                Ion.with(getApplicationContext()).
                        load(regUrl)
                        .setJsonObjectBody(json)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                Log.d("****result****", String.valueOf(result));
                                if (result.has("id") && result.get("id").getAsInt() > 0) {
                                    Log.d("****registered*****", String.valueOf(result));
                                    StoreData("auth", result.toString(), RegisterActivity.this);
                                    StoreData("username", phnStr, RegisterActivity.this);
                                    StoreData("password", passStr, RegisterActivity.this);

                                    AppData.getInstance(getApplicationContext()).user_type = result.get("user_type").getAsString();
                                    AppData.getInstance(getApplicationContext()).user_id = result.get("id").getAsInt();
                                    AppData.getInstance(getApplicationContext()).auth_token = result.get("auth_token").getAsString();
                                    AppData.getInstance(getApplicationContext()).init();

                                    if (takenProfileImage) {
                                        image_upload();
                                        Log.d("****upload****", "Uploaded");
                                    } else {
                                        Intent i = new Intent(RegisterActivity.this, TabMenuActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                } else if (result.has("status") && !result.get("status").getAsBoolean()) {
                                    alertDialog(alert, "Alert", result.get("errors").getAsString());
                                }
                                loading.dismiss();

                            }

                        });
            } catch (Exception e) {
                alertDialog(alert, "Alert", "Could not connect to the  Server. Please try again later");
            }
        } else {
            alertDialog(alert, "Alert", "No Network Connection Available. Please Connect to Internet to use DEVTOU App!");
        }
    }

    public static void alertDialog(AlertDialog.Builder alert, String title, String msg) {
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    public static void removeData(Activity act, String removeData) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(act.getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(removeData);
        editor.clear();
        editor.commit();
    }

    public static boolean netWorkChecking(Activity activity) {
        ConnectivityManager conMgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        } else
            return false;
    }

    public static void StoreData(String storeName, String storeData, Activity act) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(act);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(storeName, storeData);
        editor.commit();
    }

    //read data
    public static String readData(String storeName, Context cx) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(cx);
        return preferences.getString(storeName, "");
    }

    public static Dialog loadingView(Activity act) {
        Dialog dialogView = new Dialog(act, android.R.style.Theme_Translucent);
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.setContentView(R.layout.progressbar);
        dialogView.setCancelable(true);
        return dialogView;
    }

    /* Show alert to select / take images*/

    private void showImageCaptureDialog() {

        String title = "Open Photo";
        CharSequence[] itemlist = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, 1);
//                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
//                        startActivityForResult(intent, 1);
                        getItemImage(Sources.CAMERA);
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
//                        Intent intent2 = new Intent(Intent.ACTION_PICK);
//                        intent2.setType("image/*");
//                        intent2.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(Intent.createChooser(intent2,
//                                "Select Picture"), 2);
                        getItemImage(Sources.GALLERY);
                        break;
                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }

    public void getItemImage(Sources sources) {
        RxImagePicker.with(this).requestImage(sources)
                .flatMap(new Func1<Uri, Observable<File>>() {
                    @Override
                    public Observable<File> call(Uri uri) {
                        Picasso.with(getApplicationContext()).load(uri).transform(new CircleTransform()).fit().into(profilePic);

                        return RxImageConverters.uriToFile(RegisterActivity.this, uri,
                                new File(new ContextWrapper(getApplicationContext()).getDir("imageDir", Context.MODE_PRIVATE), "profileImg.png"));
                    }
                })
                .subscribe(new Action1<File>() {
                    @Override
                    public void call(final File file) {
                        Log.d("image path", file.getAbsolutePath());
                        if (file != null) {
                            takenProfileImage = true;
                        } else {
                            takenProfileImage = false;
                        }
                    }
                });
    }


////    public void onActivityResult(int requestCode, int resultCode, Intent data) {
////        Bitmap adjustedBitmap = null;
////        super.onActivityResult(requestCode, resultCode, data);
////        if (resultCode == RESULT_OK) {
////            if (data != null) {
////                Log.d("#######data:", String.valueOf(data));
////
////                Bitmap selectedBitmap = null;
////                if (requestCode == 1) {
////                    Bundle extras = data.getExtras();
////                    selectedBitmap = extras.getParcelable("data");
////                } else if (requestCode == 2) {
////                    try {
////                        selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
////                        selectedBitmap = RotateBitmap(selectedBitmap, -90);
////                        Log.d("#######selectedBitmap:", String.valueOf(selectedBitmap));
////                    } catch (IOException e) {
////                        e.printStackTrace();
////                    }
////                }
////
////                if (selectedBitmap != null) {
////                    adjustedBitmap = ThumbnailUtils.extractThumbnail(selectedBitmap,
////                            100, 100);
////
////
////                    ContextWrapper cw = new ContextWrapper(getApplicationContext());
////                    // path to /data/data/yourapp/app_data/imageDir
////                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
////                    // Create imageDir
////                    File mypath = new File(directory, "profileImg.png");
////                    //Picasso.with(getActivity()).load(new File(mypath.toURI())).fit().transform(new CircleTransform()).into(profileImage);
////                    adjustedBitmap = RotateBitmap(adjustedBitmap, 90);
////                    FileOutputStream fos = null;
////                    try {
////                        fos = new FileOutputStream(mypath);
////
////                        // Use the compress method on the BitMap object to write image to
////                        // the OutputStream
////                        adjustedBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
////                        fos.close();
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                    Picasso.with(getApplicationContext()).invalidate(mypath);
////                    Picasso.with(getApplicationContext()).load(mypath).transform(new CircleTransform()).into(profilePic);
////                    takenProfileImage = true;
////                }
////
////            } else {
////                takenProfileImage = false;
////            }
////        } else {
////            takenProfileImage = false;
////        }
////    }
//
//    public static Bitmap RotateBitmap(Bitmap source, float angle) {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle);
//        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
//    }

    private void image_upload() {
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            final File mypath = new File(directory, "profileImg.png");
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setTitle("Loading");
            progress.setMessage("Image is posting to server...");
            progress.show();
            String token = null;

            String auth = RegisterActivity.readData("auth", getApplicationContext());
            try {
                authObj = new JSONObject(auth);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String postURL = RegisterActivity.rootURL(getApplicationContext()) + "api/customers/" + authObj.getString("id") + "/";
            Log.d("postURL: ", postURL);
            token = "Token " + authObj.getString("auth_token");
            //String uploadImageResult = "";
            Builders.Any.B ionBuilder = Ion.with(getApplicationContext())
                    .load("PATCH", postURL);
            ionBuilder.setHeader("Authorization", token)
                    //.setHeader("Method", "PATCH")
                    .setMultipartFile("photo", mypath)
                    .asString()
                    .setCallback(
                            (FutureCallback<String>) new FutureCallback<String>() {
                                @Override
                                public void onCompleted(final Exception e,
                                                        final String uploadImageResult) {
                                    //  loading.dismiss();
                                    progress.dismiss();
                                    try {
                                        JSONObject result = new JSONObject(uploadImageResult);
                                        Log.d("ImageResult: ", result.toString());
                                        if (result.has("status") && !result.getBoolean("status")) {
                                            Log.d("ImageUploadError:", "Error");
                                        } else {
                                            Intent i = new Intent(RegisterActivity.this, TabMenuActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    } catch (JSONException e1) {
                                        Log.d("uploadJSONParseErr: ", "");
                                        e1.printStackTrace();
                                    }
                                }

                            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.d("########Error##########", "");
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
