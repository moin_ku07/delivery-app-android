package com.devtoyou.devtoyou.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;

/**
 * Created by motaz on 21/01/16.
 */
public class NotificationsSettingActivity extends FragmentActivity {
    public boolean is_active;
    public NotificationsSettingActivityBroadcastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifications_settings_activity);

        receiver = new NotificationsSettingActivityBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.UPDATE_DEVICE_GCM_DONE);
        registerReceiver(receiver, filter);

        Switch notificationSwitch = (Switch) findViewById(R.id.notifications_switch);
        final SharedPreferences sharedPreferences = getSharedPreferences(Constants.GCM_TOKEN, 0);
        notificationSwitch.setChecked(sharedPreferences.getBoolean(Constants.NOTIFICATIONS_ON, false));

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String gcmToken = sharedPreferences.getString(Constants.GCM_TOKEN, "");
                String uuid = sharedPreferences.getString(Constants.UUID, "");
                Intent i = new Intent(NotificationsSettingActivity.this, AppNetworkService.class);
                i.setAction(Actions.UPDATE_DEVICE_GCM);
                i.putExtra(Constants.GCM_TOKEN, gcmToken);
                i.putExtra(Constants.UUID, uuid);
                i.putExtra(Constants.ACTIVE_FLAG, isChecked);
                i.putExtra(Constants.AUTHENTICATION_TOKEN, AppData.getInstance(NotificationsSettingActivity.this).auth_token);
                startService(i);
                is_active = isChecked;
            }
        });
    }

    public class NotificationsSettingActivityBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.UPDATE_DEVICE_GCM_DONE)) {
                SharedPreferences.Editor editor = getSharedPreferences(Constants.GCM_TOKEN, 0).edit();
                editor.putBoolean(Constants.NOTIFICATIONS_ON, is_active);
                editor.commit();
                Toast.makeText(getApplicationContext(), "GCM Updated!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
