package com.devtoyou.devtoyou.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class SplashActivity extends FragmentActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
/* Create an Intent that will start the Menu-Activity. */
// SharedPreferences info = PreferenceManager
// .getDefaultSharedPreferences(SplashScreen.this);
                String username = prefs.getString("username", "");
                String password = prefs.getString("password", "");
                Log.e("member", username + " / " + password);
                Intent mainIntent;
                if (username != "" && password != "") {
                    String logUrl = RegisterActivity.rootURL(SplashActivity.this) + "auth/multi/login";
                    try {
                        final Dialog loading = RegisterActivity.loadingView(SplashActivity.this);
                        loading.show();

                        Ion.with(getApplicationContext()).
                                load(logUrl)
                                .setBodyParameter("username", username)
                                .setBodyParameter("password", password)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        Log.e("login result ", result + "");
                                        loading.dismiss();
                                        if (result != null) {
                                            if (result.get("status").getAsBoolean()) {
                                                RegisterActivity.StoreData("auth", result.toString(), SplashActivity.this);
                                                AppData.getInstance(getApplicationContext()).user_type = result.get("user_type").getAsString();
                                                AppData.getInstance(getApplicationContext()).user_id = result.get("id").getAsInt();
                                                AppData.getInstance(getApplicationContext()).auth_token = result.get("auth_token").getAsString();

                                                if (AppData.getInstance(getApplicationContext()).user_type.equals(Constants.STORE)) {
                                                    AppData.getInstance(getApplicationContext()).store_id = result.get("store_id").getAsInt();
                                                    Log.d("User store id splash: ", AppData.getInstance(getApplicationContext()).store_id + "");
                                                }

                                                AppData.getInstance(getApplicationContext()).init();
                                                Log.d("User Type: ", AppData.getInstance(getApplicationContext()).user_type);
//                                                finish();

                                                Intent i = new Intent(SplashActivity.this, TabMenuActivity.class);
                                                //Intent i = new Intent(SplashActivity.this, WebViewActivity.class);
                                                i.putExtra("user_type", result.get("user_type").getAsString());
                                                i.putExtra("id", result.get("id").getAsInt());
                                                startActivity(i);
                                            } else {
                                                startActivity(new Intent(SplashActivity.this, ScreenSlideActivity.class));
                                            }
                                        }
                                    }

                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                        AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                        alert.setTitle("Alert");
                        alert.setMessage("Could not connect to the  Server. Please try again later");
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                } else {
                    mainIntent = new Intent(SplashActivity.this, ScreenSlideActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
//                    SplashActivity.this.finish();
                }


            }
        }, SPLASH_TIME_OUT);

//        new Handler().postDelayed(new Runnable() {
//
//			/*
//			 * Showing splash screen with a timer. This will be useful when you
//			 * want to show case your app logo / company
//			 */
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                // Start your app main activity
//                Intent i = new Intent(SplashActivity.this, NavigatorActivity.class);
//                startActivity(i);
//
//                // close this activity
//                finish();
//            }
//        }, SPLASH_TIME_OUT);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
