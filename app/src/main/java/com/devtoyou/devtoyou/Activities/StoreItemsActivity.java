package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.Adapters.StoreItemsListViewAdapter;
import com.devtoyou.devtoyou.Adapters.StorePhotosAdapter;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.StoreItem;
import com.devtoyou.devtoyou.models.StorePhoto;
import com.google.gson.Gson;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by motaz on 26/01/16.
 */
public class StoreItemsActivity extends Activity {
    public ProgressDialog progressDialog;
    public StoreItem[] storeItems;
    public StorePhoto[] storePhotos;
    public StoreItemsActivityBroadCastReceiver receiver;
    public TextView emptyMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_items_activity);


        receiver = new StoreItemsActivityBroadCastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_STORE_ITEMS_DONE);
        filter.addAction(Actions.GET_STORE_PHOTOS_DONE);
        filter.addAction(Actions.ADD_STORE_ITEM_DONE);
        registerReceiver(receiver, filter);

        getStorePhotos();

        ImageView addBtn = (ImageView) findViewById(R.id.addButton);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StoreItemsActivity.this, StoreUploadItemActivity.class));
            }
        });

        ImageView backButton = (ImageView) findViewById(R.id.backIcon);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getAllStoreItems();

        emptyMsg = (TextView) findViewById(R.id.store_items_empty_msg);

        if (storeItems != null && storeItems.length > 0)
            redraw();
    }

    private void getAllStoreItems() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_STORE_ITEMS);
        i.putExtra(Constants.STORE_ID, AppData.getInstance(getApplicationContext()).store_id);
        startService(i);
    }

    private void getStorePhotos() {
        Intent i = new Intent(this, AppNetworkService.class);
        i.setAction(Actions.GET_STORE_PHOTOS);
        i.putExtra(Constants.STORE_ID, AppData.getInstance(this).store_id);
        startService(i);
    }

    private void redraw() {
        ListView storeItemsListview = (ListView) findViewById(R.id.store_items);
        StoreItemsListViewAdapter adapter = new StoreItemsListViewAdapter(getApplicationContext(), storeItems);
        storeItemsListview.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        storeItemsListview.setVisibility(View.VISIBLE);
        emptyMsg.setVisibility(View.GONE);

        if (storePhotos != null && storePhotos.length > 0)
            redrawStorePhotos();
    }

    private void redrawStorePhotos() {
        TwoWayView twoWayView = (TwoWayView) findViewById(R.id.store_photos_list_view);
        StorePhotosAdapter adapter = new StorePhotosAdapter(this, 0, storePhotos);
        twoWayView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public StoreItem[] getStoreItems(int storeId) {
        HashMap<Integer, CartItem> cartItemsMap = new HashMap<>();
        ArrayList<CartItem> cartItemsList = AppData.getInstance(this).cartItems;
        for (int i = 0; i < cartItemsList.size(); i++) {
            cartItemsMap.put(cartItemsList.get(i).getStoreItem(), cartItemsList.get(i));
        }
        List<StoreItem> storeItems = new ArrayList<>();
        for (StoreItem storeItem : AppData.getInstance(this).allStoresItems) {
            if (storeItem.getStore() == storeId) {
                if (cartItemsMap.containsKey(storeItem.getId())) {
                    storeItem.inCart = true;
                }
                storeItems.add(storeItem);
            }
        }
        return storeItems.toArray(new StoreItem[storeItems.size()]);
    }

    public class StoreItemsActivityBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                storeItems = AppData.getInstance(getApplicationContext()).specificStoreItems;
                redraw();
            } else if (action.equals(Actions.GET_STORE_PHOTOS_DONE)) {
                Gson gson = new Gson();
                String storePhotosStr = AppCache.cache.get(Actions.GET_STORE_PHOTOS);
                if (storePhotosStr != null && !storePhotosStr.equals("")) {
                    storePhotos = gson.fromJson(storePhotosStr, StorePhoto[].class);
                    redrawStorePhotos();
                }
            } else if (action.equals(Actions.ADD_STORE_ITEM_DONE)) {
                getAllStoreItems();
                getStorePhotos();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
