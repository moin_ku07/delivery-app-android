package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;


import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Dialogs.UserFeedbackDialog;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durlov-L3 on 11/18/2015.
 */
public class UserHistoryFeedbackActivity extends Activity {
    ListView listView;
    String[] sectionHeader = {"رأي العميل في المنتجات و المتجر:", "التقييم ممن:", "التقييم ممن:"};
    List<JsonObject> sectionData1 = new ArrayList<JsonObject>();
    List<JsonObject> sectionData2 = new ArrayList<JsonObject>();
    List<JsonObject> sectionData3 = new ArrayList<JsonObject>();
    //    String[] values = {"blog 1", "Blog 2", "Blog 3"};
    ArrayList<SectionStructure> sectionList = new ArrayList<UserHistoryFeedbackActivity.SectionStructure>();
    Integer orderID = null;
    JsonObject orderDetail = null;
    SectionStructure str;
    LayoutInflater inf;
    String display_id;
    AdapterClass adapter;
    RelativeLayout rating;
    RatingBar ratingBar;
    JsonObject items_feedback;
    public UserHistoryFeedbackViewBroadCastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_history_feedback_view);
        ImageButton backBtn = (ImageButton) findViewById(R.id.backIcon);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserHistoryFeedbackActivity.this.finish();
            }
        });
        TextView displayId = (TextView) findViewById(R.id.orderId);
        Bundle bundle = getIntent().getExtras();
        display_id = bundle.getString("display_id");
        orderID = bundle.getInt("orderID");
        displayId.setText("#" + display_id);

        inf = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        listView = (ListView) findViewById(R.id.list1);

        loadListData();

        adapter = new AdapterClass(this, 0, sectionList.toArray(new SectionStructure[sectionList.size()]));
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        receiver = new UserHistoryFeedbackViewBroadCastReceiver();
        IntentFilter filter = new IntentFilter(Actions.CLOSE_USER_FEEDBACK_DIALOG);
        registerReceiver(receiver, filter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void loadListData() {

        final Dialog loading = RegisterActivity.loadingView(this);
        loading.show();

        String url = RegisterActivity.rootURL(this) + "api/orders/" + orderID.toString() + "/?include_feedback=true&";

        if (AppData.getInstance(getApplicationContext()).user_type.equals("store_owner")) {
            url = url + "store=" + AppData.getInstance(getApplicationContext()).store_id;
        } else if (AppData.getInstance(getApplicationContext()).user_type.equals("driver")) {
            url = url + "driver=" + AppData.getInstance(getApplicationContext()).user_id;
        } else {
            url = url + "user=" + AppData.getInstance(getApplicationContext()).user_id;
        }
        Log.d("###orderURL: ", url);
        Ion.with(getApplicationContext()).load(url).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (result != null) {
                    Log.d("Orderresult:", result.toString());
                    orderDetail = result;
                    displayListData();
                } else if (e != null) {
                    e.printStackTrace();
                }
                loading.hide();
            }
        });
    }

    private void displayListData() {
        sectionList = new ArrayList<>();
        if (orderDetail != null && orderDetail.has("items")) {
            String storeName = "";

            JsonArray items = orderDetail.getAsJsonArray("items");
            for (int i = 0; i < items.size(); i++) {
                JsonObject item = items.get(i).getAsJsonObject();

                if (!storeName.equals(item.get("store_name").getAsString())) {
                    storeName = item.get("store_name").getAsString();

                    JsonObject store = new JsonObject();

                    store.addProperty("type", "store");
                    store.addProperty("store_name", storeName);
                    //item.remove("store_name");
                    store.addProperty("store_id", item.get("store_id").getAsInt());
                    //item.remove("store_id");
                    if (item.has("stores_feedback") && !item.get("stores_feedback").isJsonNull()) {
                        store.add("store_feedback", item.get("stores_feedback").getAsJsonObject());
                    } else {
                        store.add("store_feedback", null);
                    }
                    //item.remove("store_feedback");

                    sectionData1.add(store);
                    item.addProperty("type", "item");
                    sectionData1.add(item);
                } else {
                    item.addProperty("type", "item");
                    sectionData1.add(item);
                }
            }

            if (AppData.getInstance(getApplicationContext()).user_type.equals("store_owner")) {
                // section 2 object
                JsonObject driver = new JsonObject();
                driver.addProperty("type", "driver");
                driver.addProperty("name", "المندوب");
                driver.addProperty("id", orderDetail.get("driver").getAsInt());
                if (orderDetail.get("driver_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("driver_feedback").getAsJsonArray();
                    Log.d("#driver_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("store_owner")) {
                            driver.add("driver_feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData2.add(driver);

                // section 2 object
                JsonObject customer = new JsonObject();
                customer.addProperty("type", "customer");
                customer.addProperty("name", "العميل");
                customer.addProperty("id", orderDetail.get("user").getAsInt());
                if (orderDetail.get("customers_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("customers_feedback").getAsJsonArray();
                    Log.d("#customers_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("store_owner")) {
                            customer.add("customers_feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData2.add(customer);

                // section 3 object
                // No need now since driver can not provide store feedback atm
                /*JsonObject byDriver = new JsonObject();
                byDriver.addProperty("type", "bdriver");
                byDriver.addProperty("name", "المندوب");
                byDriver.addProperty("id", orderDetail.get("driver").getAsInt());
                sectionData3.add(byDriver);*/

                /*// section 3 object
                // No need as store rating already shown in section 1
                JsonObject byCustomer = new JsonObject();
                byCustomer.addProperty("type", "bcustomer");
                byCustomer.addProperty("name", "العميل");
                byCustomer.addProperty("id", orderDetail.get("user").getAsInt());
                sectionData3.add(byCustomer);*/
            } else if (AppData.getInstance(getApplicationContext()).user_type.equals("driver")) {
                // section 2 object
                JsonObject customer = new JsonObject();
                customer.addProperty("type", "customer");
                customer.addProperty("name", "العميل");
                customer.addProperty("id", orderDetail.get("user").getAsInt());
                if (orderDetail.get("customers_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("customers_feedback").getAsJsonArray();
                    Log.d("#customers_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("driver")) {
                            customer.add("customers_feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData2.add(customer);

                // section 3 object
                JsonObject byCustomer = new JsonObject();
                byCustomer.addProperty("type", "bcustomer");
                byCustomer.addProperty("name", "العميل");
                byCustomer.addProperty("id", orderDetail.get("user").getAsInt());
                if (orderDetail.get("driver_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("driver_feedback").getAsJsonArray();
                    Log.d("#bdriver_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("customer")) {
                            byCustomer.add("feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData3.add(byCustomer);

                // section 3 object
                JsonObject byStore = new JsonObject();
                byStore.addProperty("type", "bstore");
                byStore.addProperty("name", "متجر");
                byStore.addProperty("id", orderDetail.get("user").getAsInt());
                if (orderDetail.get("driver_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("driver_feedback").getAsJsonArray();
                    Log.d("#bdriver_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("store_owner")) {
                            byStore.add("feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData3.add(byStore);
            } else {
                // section 2 object
                JsonObject driver = new JsonObject();
                driver.addProperty("type", "driver");
                driver.addProperty("name", "المندوب");
                driver.addProperty("id", orderDetail.get("driver").getAsInt());
                if (orderDetail.get("driver_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("driver_feedback").getAsJsonArray();
                    Log.d("#driver_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("customer")) {
                            driver.add("driver_feedback", obj.getAsJsonObject());
                        }
                    }
                }
                Log.d("###driverData", String.valueOf(driver));
                sectionData2.add(driver);

                // section 3 object
                JsonObject byStore = new JsonObject();
                byStore.addProperty("type", "bstore");
                byStore.addProperty("name", "متجر");
                byStore.addProperty("id", orderDetail.get("user").getAsInt());
                if (orderDetail.get("customers_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("customers_feedback").getAsJsonArray();
                    Log.d("#customers_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("store_owner")) {
                            byStore.add("feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData3.add(byStore);

                // section 3 object
                JsonObject byDriver = new JsonObject();
                byDriver.addProperty("type", "bdriver");
                byDriver.addProperty("name", "المندوب");
                byDriver.addProperty("id", orderDetail.get("driver").getAsInt());
                if (orderDetail.get("customers_feedback").getAsJsonArray().size() > 0) {
                    JsonArray feedback = orderDetail.get("customers_feedback").getAsJsonArray();
                    Log.d("#customers_feedback", String.valueOf(feedback));
                    for (int i = 0; i < feedback.size(); i++) {
                        JsonObject obj = feedback.get(i).getAsJsonObject();
                        //Log.d("###driverData", String.valueOf(obj));
                        if (obj.get("given_by").getAsString().equals("driver")) {
                            byDriver.add("feedback", obj.getAsJsonObject());
                        }
                    }
                }
                sectionData3.add(byDriver);
            }
        }

        for (int i = 0; i < sectionHeader.length; i++) {
            // Remove section 1 header & rows for driver
            if (i == 0 && AppData.getInstance(getApplicationContext()).user_type.equals("driver")) {
                continue;
            } else if (i == 2 && AppData.getInstance(getApplicationContext()).user_type.equals("store_owner")) {
                continue;
            }

            str = new SectionStructure();
            str.setSectionName(sectionHeader[i]);
            Log.d("##Section: ", sectionHeader[i]);
            str.setSectionValue("");
            sectionList.add(str);

            if (i == 0) {
                for (int j = 0; j < sectionData1.size(); j++) {
                    str = new SectionStructure();
                    str.setSectionName("");

                    JsonObject item = sectionData1.get(j);
                    str.setJsonObject(item);
                    if (item.get("type").getAsString().equals("store")) {
                        str.setSectionValue(item.get("store_name").getAsString());
                        Log.d("##Store_name:", item.get("store_name").getAsString());
                    } else if (item.get("type").getAsString().equals("item")) {
                        str.setSectionValue(item.get("item_name").getAsString() + "  \u21B5");
                    }

                    sectionList.add(str);
                }
            } else if (i == 1) {
                for (int j = 0; j < sectionData2.size(); j++) {
                    str = new SectionStructure();
                    str.setSectionName("");

                    JsonObject item = sectionData2.get(j);
                    str.setJsonObject(item);
                    str.setSectionValue(item.get("name").getAsString());

                    sectionList.add(str);
                }
            } else if (i == 2) {
                for (int j = 0; j < sectionData3.size(); j++) {
                    str = new SectionStructure();
                    str.setSectionName("");

                    JsonObject item = sectionData3.get(j);
                    str.setJsonObject(item);
                    str.setSectionValue(item.get("name").getAsString());
                    sectionList.add(str);
                }
            }
        }

        adapter = new AdapterClass(this, 0, sectionList.toArray(new SectionStructure[sectionList.size()]));
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//        listView.invalidateViews();
//        listView.refreshDrawableState();
    }

    public class AdapterClass extends ArrayAdapter<SectionStructure> {
        SectionStructure[] values;

        public AdapterClass(Context context, int resource, SectionStructure[] objects) {
            super(context, resource, objects);
            values = objects;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi;
            //RelativeLayout relativeLayout = (RelativeLayout)vi.findViewById(R.id.relativeLayout);
            if (values[position].getSectionValue() != null && values[position].getSectionValue().equalsIgnoreCase("")) {
                vi = inf.inflate(R.layout.user_history_feedback_view_header, parent, false);
                TextView textView = (TextView) vi.findViewById(R.id.feedback_view_text);
                textView.setText(values[position].getSectionName());
                RelativeLayout relativeLayout = (RelativeLayout) vi.findViewById(R.id.relativeLayout_header);
                relativeLayout.setBackgroundColor(Color.rgb(10, 172, 241));

            } else {
                final JsonObject obj = values[position].getJsonObject();
                Log.d("##SectionObj:", String.valueOf(obj));
                vi = inf.inflate(R.layout.user_history_feedback_view_row, null);
                TextView textView = (TextView) vi.findViewById(R.id.feedback_view_text);
                ratingBar = (RatingBar) vi.findViewById(R.id.user_feedback_ratingbar_view);

                if (obj.get("type").getAsString().equals("customer")) {
                    if (obj.has("customers_feedback") && !obj.get("customers_feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("customers_feedback").getAsJsonObject();
                        Log.d("##customers_feedback:", String.valueOf(feedbackItem));
                        if (!feedbackItem.isJsonNull()) {
                            if (feedbackItem.get("rating").getAsFloat() > 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                        }
                    }
                } else if (obj.get("type").getAsString().equals("bcustomer")) {
                    if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("feedback").getAsJsonObject();
                        Log.d("##bcustomer_feedback:", String.valueOf(feedbackItem));
                        if (feedbackItem.get("rating").getAsFloat() >= 0) {
                            ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            ratingBar.setIsIndicator(true);
                        }
                    }
                } else if (obj.get("type").getAsString().equals("driver")) {
                    if (obj.has("driver_feedback") && !obj.get("driver_feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("driver_feedback").getAsJsonObject();
                        Log.d("##driver_feedback:", String.valueOf(feedbackItem));
                        if (!feedbackItem.isJsonNull()) {
                            if (feedbackItem.get("rating").getAsFloat() > 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                        }
                    }
                } else if (obj.get("type").getAsString().equals("bdriver")) {
                    if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("feedback").getAsJsonObject();
                        Log.d("##bdriver_feedback:", String.valueOf(feedbackItem));
                        if (feedbackItem.get("rating").getAsFloat() >= 0) {
                            ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            ratingBar.setIsIndicator(true);
                        }
                    }
                } else if (obj.get("type").getAsString().equals("store")) {
                    //textView.setTextSize(10);
                    //textView.setBackgroundColor(Color.parseColor("#F00000"));
                    if (obj.has("store_feedback") && !obj.get("store_feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("store_feedback").getAsJsonObject();
                        //Log.d("##items_feedback:", String.valueOf(feedbackItem));
                        if (!feedbackItem.isJsonNull()) {
                            if (feedbackItem.get("rating").getAsFloat() > 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                        }
                    }
                } else if (obj.get("type").getAsString().equals("bstore")) {
                    if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("feedback").getAsJsonObject();
                        Log.d("##bstore_feedback:", String.valueOf(feedbackItem));
                        if (feedbackItem.get("rating").getAsFloat() >= 0) {
                            ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            ratingBar.setIsIndicator(true);
                        }
                    }
                } else if (obj.get("type").getAsString().equals("item")) {
                    if (obj.has("items_feedback") && !obj.get("items_feedback").isJsonNull()) {
                        JsonObject feedbackItem = obj.get("items_feedback").getAsJsonObject();
                        //Log.d("##items_feedback:", String.valueOf(feedbackItem));
                        if (!feedbackItem.isJsonNull()) {
                            if (feedbackItem.get("rating").getAsFloat() > 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                        }
                    }
                }

                rating = (RelativeLayout) vi.findViewById(R.id.rating);
                rating.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserFeedbackDialog dialog = UserFeedbackDialog.newInstance("");
                        FragmentManager fragmentManager = getFragmentManager();
                        Bundle bundle = new Bundle();
                        bundle.putString("jsonObj", obj.toString());
                        bundle.putString("displayId", display_id);
                        bundle.putInt("orderID", orderID);
                        dialog.setArguments(bundle);
                        dialog.show(fragmentManager, "User feedback form");
                    }
                });
                textView.setText(values[position].getSectionValue());
                RelativeLayout relativeLayout = (RelativeLayout) vi.findViewById(R.id.relativeLayout);
                relativeLayout.setBackgroundColor(Color.rgb(239, 236, 217));
            }


            return vi;
        }

    }

    private class SectionStructure {
        public String sectionName;
        public String sectionValue;
        public JsonObject jsonObject;

        public String getSectionName() {
            return sectionName;
        }

        public void setSectionName(String sectionName) {
            this.sectionName = sectionName;
        }

        public void setJsonObject(JsonObject obj) {
            this.jsonObject = obj;
        }

        public JsonObject getJsonObject() {
            return this.jsonObject;
        }

        public String getSectionValue() {
            return sectionValue;
        }

        public void setSectionValue(String sectionValue) {
            this.sectionValue = sectionValue;
        }
    }

    public class UserHistoryFeedbackViewBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.CLOSE_USER_FEEDBACK_DIALOG)) {
                sectionList = new ArrayList<>();
                sectionData1 = new ArrayList<>();
                  sectionData2 = new ArrayList<>();
                sectionData3 = new ArrayList<>();
                loadListData();
            }
        }
    }
}