package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.Store;
import com.devtoyou.devtoyou.models.StoreItem;
import com.squareup.picasso.Picasso;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class PopularStoresActivity extends Activity {

    public View view;
    private GridAdapter adapter;
    private ListAdapter listadapter;
    GridView gridView;
    ListView listView;
    ProgressDialog loadingProgressDialog;
    //GridView gv;
    Context context;
    public BroadcastReceiver receiver;
    public int lastClickedPosition;
    //ArrayList prgmName;
    //public static String[] prgmNameList = {"Bonafide Restaurant", "Srivas Restaurant", "Grub Restaurants", "Food Restaurant Drink", "FoodCircles", "Best Italian"};
    //public static int[] prgmImages = {R.drawable.bonafide, R.drawable.srivas, R.drawable.grub, R.drawable.food, R.drawable.circle, R.drawable.best};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popular_stores_list);
        if (AppData.getInstance(getApplicationContext()).fvrStores != null){
            Log.d("***Data****", String.valueOf(AppData.getInstance(getApplicationContext()).fvrStores));
            redraw();
        }

        ImageButton imgBtn = (ImageButton) findViewById(R.id.menuBtn);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageView popItemBtn = (ImageView) findViewById(R.id.searchIcon);
        popItemBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iinent = new Intent(PopularStoresActivity.this, SearchActivity.class);
                startActivity(iinent);

            }
        });

        receiver = new AllFavouriteStoresBroadCastReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Actions.GET_ALL_FAVOURITE_STORES_DONE);
        filter.addAction(Actions.GET_ALL_STORE_ITEMS_DONE);
        getApplicationContext().registerReceiver(receiver, filter);
    }

    private void redraw() {
        gridView = (GridView) findViewById(R.id.gridview);
        adapter = new GridAdapter(getApplicationContext(), AppData.getInstance(getApplicationContext()).fvrStores);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (AppData.getInstance(getApplicationContext()).allStoresItems != null) {
                    showStoreItems(position);
                } else {
                    lastClickedPosition = position;
                    loadingProgressDialog = ProgressDialog.show(getApplicationContext(), "", "Loading store items", true);
                }
            }


        });
    }

    public void showStoreItems(int position) {
        Context context = getApplicationContext();
        final Dialog dialog = new Dialog(PopularStoresActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_screen_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Store choosenStore = AppData.getInstance(getApplicationContext()).fvrStores[position];

        ImageView headerImg = (ImageView) dialog.findViewById(R.id.headerImg);

        if(choosenStore.getThumb() != null ){
            headerImg.setBackgroundResource(0);
            Picasso.with(getApplicationContext().getApplicationContext()).load(choosenStore.getThumb()).into(headerImg);
        }
        TextView storeName = (TextView) dialog.findViewById(R.id.name);
        storeName.setText(choosenStore.getName());

        Button cross = (Button) dialog.findViewById(R.id.cross);
        TabMenuActivity.halfSizingButton(this, cross, R.drawable.cross_home, 20, 20, "top");
        cross.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        listView = (ListView) dialog.findViewById(R.id.list);
        listadapter = new ListAdapter(dialog.getContext(), getStoreItems(choosenStore.getId()));
        listView.setAdapter(listadapter);
    }

    public class GridAdapter extends ArrayAdapter<String> {
        private final Context context;
        //        private final String[] values;
//        private int[] images;
        private Store[] stores;
        private TextView text;
        private ImageView img;

        public GridAdapter(Context context, Store[] stores) {
            super(context, R.layout.grid_row);
            this.context = context;
            this.stores = stores;
        }

        @Override
        public int getCount() {
            return stores.length;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.grid_row, parent, false);
            text = (TextView) rowView.findViewById(R.id.text);
            img = (ImageView) rowView.findViewById(R.id.image);

            text.setText(stores[position].getName());
            Picasso.with(getContext()).load(stores[position].getThumb()).into(img);
            img.setBackgroundColor(Color.rgb(216, 214, 215));
            img.setScaleType(ImageView.ScaleType.CENTER_CROP);

            return rowView;
        }

    }

    public class ListAdapter extends ArrayAdapter<String[]> {
        public View.OnClickListener listener;
        private final Context context;
        private StoreItem[] values;
        public StoreItem storeItem;


        public ListAdapter(Context context, StoreItem[] values) {
            super(context, R.layout.home_screen_popup_row);
            this.context = context;
            this.values = values;
        }

        @Override
        public int getCount() {
            return values.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.home_screen_popup_row, parent, false);

            storeItem = values[position];
            ImageView itemImage = (ImageView) rowView.findViewById(R.id.image);
            if(storeItem.getPhoto() != null){
                itemImage.setBackgroundResource(0);
                Picasso.with(getApplicationContext()).load(storeItem.getPhoto()).into(itemImage);
            }
            TextView itemTitle = (TextView) rowView.findViewById(R.id.title);
            itemTitle.setText(storeItem.getName());

            TextView itemDescp = (TextView) rowView.findViewById(R.id.item_description);
            itemDescp.setText(storeItem.getDescription());

            TextView itemPrice = (TextView) rowView.findViewById(R.id.price);
            itemPrice.setText("رس " + storeItem.getPrice());

            final Button add = (Button) rowView.findViewById(R.id.addBtn);

            if (storeItem.inCart) {
                add.setText("Added");
            }

            add.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    storeItem = values[position];
                    if (!storeItem.inCart) {
                        CartItem cartItem = new CartItem();
                        cartItem.setStoreItem(storeItem.getId());
                        cartItem.setPrice(storeItem.getPrice());
                        cartItem.setQuantity(1);
                        AppData.getInstance(getApplicationContext()).cartItems.add(cartItem);
                        add.setText("Added");

                        storeItem.inCart = true;

                        Intent i = new Intent(Actions.ADD_ITEM_TO_CART);
                        getApplicationContext().sendBroadcast(i);
                    }
                }
            });
            return rowView;
        }
    }

    public StoreItem[] getStoreItems(int storeId) {
        List<StoreItem> storeItems = new ArrayList<>();
        for (StoreItem storeItem : AppData.getInstance(getApplicationContext()).allStoresItems) {
            if (storeItem.getStore() == storeId) {
                storeItems.add(storeItem);
            }
        }
        return storeItems.toArray(new StoreItem[storeItems.size()]);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_fragment, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*public void menuBtnClick(View view) {

        final Dialog Viewdialog = new Dialog(this);
        Viewdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Viewdialog.setContentView(R.layout.home_page_menu);
        Viewdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Viewdialog.show();

        final Button delete = (Button) Viewdialog.findViewById(R.id.crossBtn);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Viewdialog.dismiss();
            }
        });

    }*/

    public class AllFavouriteStoresBroadCastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Actions.GET_ALL_FAVOURITE_STORES_DONE)) {
                redraw();
            } else if (action.equals(Actions.GET_ALL_STORE_ITEMS_DONE)) {
                if (loadingProgressDialog != null && loadingProgressDialog.isShowing()) {
                    loadingProgressDialog.cancel();
                    showStoreItems(lastClickedPosition);
                }
            }
        }
    }
}

