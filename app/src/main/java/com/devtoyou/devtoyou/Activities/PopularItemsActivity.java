package com.devtoyou.devtoyou.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.devtoyou.devtoyou.Fragments.PopularItemsFragment;
import com.devtoyou.devtoyou.R;

/**
 * Created by Durlov-L3 on 12/13/2015.
 */
public class PopularItemsActivity extends Activity {
    // private static final int CONTENT_VIEW_ID = 10101010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popular_items);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        PopularItemsFragment popularitems = new PopularItemsFragment();

        fragmentTransaction.add(R.id.fragment_container, popularitems, "");
        fragmentTransaction.commit();

        ImageButton imgBtn = (ImageButton) findViewById(R.id.menuBtn);
        imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
