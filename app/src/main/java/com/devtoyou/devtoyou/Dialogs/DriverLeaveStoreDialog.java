package com.devtoyou.devtoyou.Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.Constants;

import org.w3c.dom.Text;

/**
 * Created by motaz on 14/01/16.
 */
public class DriverLeaveStoreDialog extends android.support.v4.app.DialogFragment {
    public String title;
    public boolean isForStore;

    public DriverLeaveStoreDialog newInstance(String title, boolean isForStore) {
        this.title = title;
        this.isForStore = isForStore;

        Bundle args = new Bundle();
        DriverLeaveStoreDialog fragment = new DriverLeaveStoreDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.driver_update_balance, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final EditText balance = (EditText) view.findViewById(R.id.balance);
        balance.setText("0");

        TextView message = (TextView) view.findViewById(R.id.message);
        if (isForStore) {
            message.setText("Enter store amount");
        } else {
            message.setText("Enter customer amount");
        }

        TextView submit = (TextView) view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double amount = Double.parseDouble(balance.getText().toString());
                Intent i;
                i = new Intent(Actions.DRIVER_LEAVE_STORE_BALANCE);
                i.putExtra(Constants.DRIVER_AMOUNT, amount);
                getActivity().sendBroadcast(i);
                getDialog().dismiss();
            }
        });

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        balance.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

    }
}
