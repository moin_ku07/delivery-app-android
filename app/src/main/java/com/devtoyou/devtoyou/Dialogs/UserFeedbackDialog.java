package com.devtoyou.devtoyou.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.devtoyou.devtoyou.Activities.RegisterActivity;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by motaz on 11/2/15.
 */
public class UserFeedbackDialog extends DialogFragment {
    Button btn;
    EditText editText;
    RatingBar ratingBar;
    View view;
    TextView page_title;
    int orderID = 0;
    JsonObject obj;
    String url;
    JSONObject authObj;
    JsonObject feedbackItem;
    Dialog loading;

    public static UserFeedbackDialog newInstance(String title) {
        UserFeedbackDialog dialog = new UserFeedbackDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        dialog.setArguments(args);
        dialog.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.user_feedback_form, container, false);
            btn = (Button) view.findViewById(R.id.user_feedback_btn);
            editText = (EditText) view.findViewById(R.id.driver_feedback_edttxt);
            ratingBar = (RatingBar) view.findViewById(R.id.user_feedback_ratingbar);
            page_title = (TextView) view.findViewById(R.id.page_title);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loading = RegisterActivity.loadingView(getActivity());
                    loading.show();
                    postFeedback();
                }
            });
            getDialog().setCanceledOnTouchOutside(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        url = RegisterActivity.rootURL(getActivity());

        Bundle bundle = this.getArguments();
//        Log.d("####displayId",bundle.get("displayId").toString());
        if (bundle != null) {
            if (bundle.get("jsonObj") != null) {
                JsonParser jsonParser = new JsonParser();
                obj = (JsonObject) jsonParser.parse(bundle.getString("jsonObj"));
                Log.d("##JSONOBJ:", String.valueOf(obj));

                orderID = bundle.getInt("orderID");

                if (!obj.isJsonNull()) {
                    if (obj.get("type").getAsString().equals("customer")) {
                        page_title.setText("تقييم العميل");
                        if (obj.has("customers_feedback") && !obj.get("customers_feedback").isJsonNull()) {
                            feedbackItem = obj.get("customers_feedback").getAsJsonObject();
                            Log.d("##customers_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                                editText.setKeyListener(null);
                                btn.setVisibility(View.GONE);
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                        }
                        url += "api/customer-evaluations/";
                    } else if (obj.get("type").getAsString().equals("bcustomer")) {
                        if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                            feedbackItem = obj.get("feedback").getAsJsonObject();
                            Log.d("##bstore_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                        }
                        ratingBar.setIsIndicator(true);
                        editText.setKeyListener(null);
                        btn.setVisibility(View.GONE);
                    } else if (obj.get("type").getAsString().equals("store")) {
                        page_title.setText("تقييم متجر");
                        if (obj.has("store_feedback") && !obj.get("store_feedback").isJsonNull()) {
                            feedbackItem = obj.get("store_feedback").getAsJsonObject();
                            Log.d("##store_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                            editText.setKeyListener(null);
                            btn.setVisibility(View.GONE);
                        } else if (AppData.getInstance(getActivity()).user_type.equals("store_owner")) {
                            ratingBar.setIsIndicator(true);
                            editText.setKeyListener(null);
                            btn.setVisibility(View.GONE);
                        }
                        url += "api/store-evaluations/";
                    } else if (obj.get("type").getAsString().equals("bstore")) {
                        page_title.setText("تقييم متجر");
                        if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                            feedbackItem = obj.get("feedback").getAsJsonObject();
                            Log.d("##bstore_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                        }
                        ratingBar.setIsIndicator(true);
                        editText.setKeyListener(null);
                        btn.setVisibility(View.GONE);
                    } else if (obj.get("type").getAsString().equals("driver")) {
                        page_title.setText("تقييم المندوب");
                        if (obj.has("driver_feedback") && !obj.get("driver_feedback").isJsonNull()) {
                            feedbackItem = obj.get("driver_feedback").getAsJsonObject();
                            Log.d("##driver_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                ratingBar.setIsIndicator(true);
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                            editText.setKeyListener(null);
                            btn.setVisibility(View.GONE);
                        }
                        url += "api/driver-evaluations/";
                    } else if (obj.get("type").getAsString().equals("bdriver")) {
                        page_title.setText("تقييم المندوب");
                        if (obj.has("feedback") && !obj.get("feedback").isJsonNull()) {
                            feedbackItem = obj.get("feedback").getAsJsonObject();
                            Log.d("##bdriver_feedback:", String.valueOf(feedbackItem));
                            if (feedbackItem.get("rating").getAsFloat() >= 0) {
                                ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                            }
                            editText.setText(feedbackItem.get("text").getAsString());
                        }
                        ratingBar.setIsIndicator(true);
                        editText.setKeyListener(null);
                        btn.setVisibility(View.GONE);
                    } else if (obj.get("type").getAsString().equals("item")) {
                        page_title.setText("تقييم منتج");
                        //Log.d("####Items_feedback", String.valueOf(obj.get("items_feedback")));
                        if (obj.has("items_feedback") && !obj.get("items_feedback").isJsonNull()) {
                            feedbackItem = obj.get("items_feedback").getAsJsonObject();
                            //Log.d("##items_feedback:", String.valueOf(feedbackItem));
                            if (!feedbackItem.isJsonNull()) {
                                if (feedbackItem.get("rating").getAsFloat() > 0) {
                                    ratingBar.setRating(feedbackItem.get("rating").getAsFloat());
                                    ratingBar.setIsIndicator(true);
                                    editText.setText(feedbackItem.get("text").getAsString());
                                    editText.setKeyListener(null);
                                    btn.setVisibility(View.GONE);
                                }
                            }
                        } else if (AppData.getInstance(getActivity()).user_type.equals("store_owner")) {
                            ratingBar.setIsIndicator(true);
                            editText.setKeyListener(null);
                            btn.setVisibility(View.GONE);
                        }

                        url += "api/item-evaluations/";
                    }
                }
            } else {
                Log.d("##JSONOBJ:", "No Obj");
            }
        }
    }

    private void postFeedback() {
        Log.d("##postUrl:", url);
        String auth = RegisterActivity.readData("auth", getActivity());
        try {
            authObj = new JSONObject(auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String token = null;
        if (orderID > 0) {
            JsonObject postJson = new JsonObject();

            postJson.addProperty("text", editText.getText().toString());
            postJson.addProperty("rating", Math.round((ratingBar.getRating() * 100) / 100));
            postJson.addProperty("user", AppData.getInstance(getActivity()).user_id);
            postJson.addProperty("order", orderID);
            if (obj.get("type").getAsString().equals("store")) {
                postJson.addProperty("store", obj.get("store_id").getAsInt());
            } else if (obj.get("type").getAsString().equals("item")) {
                postJson.addProperty("item", obj.get("store_item").getAsInt());
            } else if (obj.get("type").getAsString().equals("driver")) {
                postJson.addProperty("driver", obj.get("id").getAsInt());
            } else if (obj.get("type").getAsString().equals("customer")) {
                postJson.addProperty("customer", obj.get("id").getAsInt());
            }

            Log.d("####POST_DATA", String.valueOf(postJson));
            try {
                token = "Token " + authObj.getString("auth_token");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Builders.Any.B ionBuilder = Ion.with(getActivity())
                    .load(url);
            ionBuilder.setHeader("Authorization", token)
                    .setJsonObjectBody(postJson)
                    .asJsonObject()
                    .setCallback(
                            (FutureCallback<JsonObject>) new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(final Exception e, final JsonObject result) {
                                    loading.dismiss();
                                    Log.d("****Post Json data", String.valueOf(result));
                                    if (result.has("id")) {
                                        // close dialog and reload loadListData
                                        Intent i = new Intent(Actions.CLOSE_USER_FEEDBACK_DIALOG);
                                        getActivity().sendBroadcast(i);
                                        dismiss();
                                    } else {
                                        // show error alert
                                        Toast.makeText(getActivity(), "Error in submitting feedback.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            });
        }
    }
}
