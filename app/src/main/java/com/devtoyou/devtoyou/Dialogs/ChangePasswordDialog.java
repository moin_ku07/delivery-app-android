package com.devtoyou.devtoyou.Dialogs;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devtoyou.devtoyou.CustomViews.CustomEditText;
import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.Constants;

/**
 * Created by motaz on 07/03/16.
 */
public class ChangePasswordDialog extends DialogFragment {
    public View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_password_dialog_layout, container, false);
        final CustomEditText newPassword = (CustomEditText) view.findViewById(R.id.new_password);
        final CustomEditText newPasswordConfirm = (CustomEditText) view.findViewById(R.id.new_password_confirm);
        CustomPlainTextView submit = (CustomPlainTextView) view.findViewById(R.id.submit);
        newPasswordConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                newPasswordConfirm.setError(null);
                return false;
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPassword.getText().toString().equals(newPasswordConfirm.getText().toString())) {
                    Intent i = new Intent(Actions.UPDATE_USER_PASSWORD);
                    i.putExtra(Constants.USER_NEW_PASSWORD, newPassword.getText().toString());
                    getActivity().sendBroadcast(i);
                    dismiss();
                } else {
                    newPasswordConfirm.setError("كلمة المرور غير متطابقة.");
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.popup_width);
        int height = getResources().getDimensionPixelSize(R.dimen.change_passoword_dialog_height);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

    }

}
