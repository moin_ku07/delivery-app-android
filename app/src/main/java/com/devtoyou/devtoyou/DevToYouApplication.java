package com.devtoyou.devtoyou;

import android.app.Application;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by motaz on 20/02/16.
 */
public class DevToYouApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "m2c4UFO9Zk0cTI8CLicc3QPe0";
    private static final String TWITTER_SECRET = "gX5Tffg9cKTxyYQ2AzmgCeUrbZcmHeYzJeeJY2UbqJVOQc6r7e";

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }
}
