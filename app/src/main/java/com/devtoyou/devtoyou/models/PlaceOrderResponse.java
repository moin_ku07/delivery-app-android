package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 11/7/15.
 */
public class PlaceOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("display_id")
    @Expose
    private String displayId;

    /**
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return The orderId
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * @param orderId The order_id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * @return The displayId
     */
    public String getDisplayId() {
        return displayId;
    }

    /**
     * @param displayId The display_id
     */
    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }
}
