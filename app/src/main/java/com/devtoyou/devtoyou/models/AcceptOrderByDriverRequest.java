package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 10/01/16.
 */
public class AcceptOrderByDriverRequest {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("driver")
    @Expose
    private Integer driver;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The driver
     */
    public Integer getDriver() {
        return driver;
    }

    /**
     * @param driver The driver
     */
    public void setDriver(Integer driver) {
        this.driver = driver;
    }

}


