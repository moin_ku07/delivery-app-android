package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 23/03/16.
 */
public class SocialLoginRequest {

    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("profile_id")
    @Expose
    private Long profileId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("user_type")
    @Expose
    private String userType;

    /**
     *
     * @return
     * The accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     *
     * @param accessToken
     * The access_token
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     *
     * @return
     * The profileId
     */
    public Long getProfileId() {
        return profileId;
    }

    /**
     *
     * @param profileId
     * The profile_id
     */
    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    /**
     *
     * @return
     * The serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     *
     * @param serviceName
     * The service_name
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     *
     * @return
     * The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     *
     * @param userType
     * The user_type
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

}