package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 09/01/16.
 */
public class UserLocation {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("door_no")
    @Expose
    private String doorNo;
    @SerializedName("door_color")
    @Expose
    private String doorColor;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("own_location")
    @Expose
    private Boolean ownLocation;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("district")
    @Expose
    private Object district;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The houseNo
     */
    public String getHouseNo() {
        return houseNo;
    }

    /**
     * @param houseNo The house_no
     */
    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    /**
     * @return The doorNo
     */
    public String getDoorNo() {
        return doorNo;
    }

    /**
     * @param doorNo The door_no
     */
    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    /**
     * @return The doorColor
     */
    public String getDoorColor() {
        return doorColor;
    }

    /**
     * @param doorColor The door_color
     */
    public void setDoorColor(String doorColor) {
        this.doorColor = doorColor;
    }

    /**
     * @return The note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note The note
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The ownLocation
     */
    public Boolean getOwnLocation() {
        return ownLocation;
    }

    /**
     * @param ownLocation The own_location
     */
    public void setOwnLocation(Boolean ownLocation) {
        this.ownLocation = ownLocation;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * @return The district
     */
    public Object getDistrict() {
        return district;
    }

    /**
     * @param district The district
     */
    public void setDistrict(Object district) {
        this.district = district;
    }

}