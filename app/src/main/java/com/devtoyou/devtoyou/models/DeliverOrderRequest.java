package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 16/01/16.
 */
public class DeliverOrderRequest {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("pickedup")
    @Expose
    private String pickedup;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The pickedup
     */
    public String getPickedup() {
        return pickedup;
    }

    /**
     * @param pickedup The pickedup
     */
    public void setPickedup(String pickedup) {
        this.pickedup = pickedup;
    }

}
