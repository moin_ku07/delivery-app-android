package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 11/5/15.
 */
public class CartItem {
    @SerializedName("store_item")
    @Expose
    private Integer storeItem;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private Double price;

    /**
     * @return The storeItem
     */
    public Integer getStoreItem() {
        return storeItem;
    }

    /**
     * @param storeItem The store_item
     */
    public void setStoreItem(Integer storeItem) {
        this.storeItem = storeItem;
    }

    /**
     * @return The quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity The quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }
}
