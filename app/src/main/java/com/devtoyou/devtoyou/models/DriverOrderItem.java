package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 09/01/16.
 */
public class DriverOrderItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("item_name")
    @Expose
    private String itemName;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_owner")
    @Expose
    private Integer storeOwner;
    @SerializedName("store_id")
    @Expose
    private Integer storeId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("purchased")
    @Expose
    private Boolean purchased;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("store_item")
    @Expose
    private Integer storeItem;
    @SerializedName("store_branches")
    @Expose
    private List<StoreBranch> storeBranches = new ArrayList<StoreBranch>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName The item_name
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return The storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName The store_name
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return The storeOwner
     */
    public Integer getStoreOwner() {
        return storeOwner;
    }

    /**
     * @param storeOwner The store_owner
     */
    public void setStoreOwner(Integer storeOwner) {
        this.storeOwner = storeOwner;
    }

    /**
     * @return The storeId
     */
    public Integer getStoreId() {
        return storeId;
    }

    /**
     * @param storeId The store_id
     */
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    /**
     * @return The quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity The quantity
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return The purchased
     */
    public Boolean getPurchased() {
        return purchased;
    }

    /**
     * @param purchased The purchased
     */
    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }

    /**
     * @return The order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order The order
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * @return The storeItem
     */
    public Integer getStoreItem() {
        return storeItem;
    }

    /**
     * @param storeItem The store_item
     */
    public void setStoreItem(Integer storeItem) {
        this.storeItem = storeItem;
    }

    /**
     * @return The storeBranches
     */
    public List<StoreBranch> getStoreBranches() {
        return storeBranches;
    }

    /**
     * @param storeBranches The store_branches
     */
    public void setStoreBranches(List<StoreBranch> storeBranches) {
        this.storeBranches = storeBranches;
    }

}