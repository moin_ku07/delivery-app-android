package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Durlov-L3 on 12/1/2015.
 */
public class AppSettings {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("config")
    @Expose
    private String config;
    @SerializedName("val")
    @Expose
    private String val;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The config
     */
    public String getConfig() {
        return config;
    }

    /**
     * @param config The config
     */
    public void setConfig(String config) {
        this.config = config;
    }

    /**
     * @return The val
     */
    public String getVal() {
        return val;
    }

    /**
     * @param val The val
     */
    public void setVal(String val) {
        this.val = val;
    }


}
