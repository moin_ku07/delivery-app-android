package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motazelshaer on 11/14/15.
 */
public class Order {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("avg_rating")
    @Expose
    private Object avgRating;
    @SerializedName("display_id")
    @Expose
    private String displayId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("additional_notes")
    @Expose
    private String additionalNotes;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("driver")
    @Expose
    private int driver;
    @SerializedName("payment_method")
    @Expose
    private Integer paymentMethod;
    @SerializedName("user_location")
    @Expose
    private Integer userLocation;
    @SerializedName("items")
    @Expose
    private List<DriverOrderItem> items = new ArrayList<DriverOrderItem>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The avgRating
     */
    public Object getAvgRating() {
        return avgRating;
    }

    /**
     * @param avgRating The avg_rating
     */
    public void setAvgRating(Object avgRating) {
        this.avgRating = avgRating;
    }

    /**
     * @return The displayId
     */
    public String getDisplayId() {
        return displayId;
    }

    /**
     * @param displayId The display_id
     */
    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The pickup
     */
    public String getPickup() {
        return pickup;
    }

    /**
     * @param pickup The pickup
     */
    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    /**
     * @return The additionalNotes
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * @param additionalNotes The additional_notes
     */
    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    /**
     * @return The total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType The user_type
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * @return The driver
     */
    public int getDriver() {
        return driver;
    }

    /**
     * @param driver The driver
     */
    public void setDriver(int driver) {
        this.driver = driver;
    }

    /**
     * @return The paymentMethod
     */
    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return The userLocation
     */
    public Integer getUserLocation() {
        return userLocation;
    }

    /**
     * @param userLocation The user_location
     */
    public void setUserLocation(Integer userLocation) {
        this.userLocation = userLocation;
    }

    /**
     * @return The items
     */
    public List<DriverOrderItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<DriverOrderItem> items) {
        this.items = items;
    }

}