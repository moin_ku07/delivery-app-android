package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 30/01/16.
 */
public class StorePhoto {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("approved")
    @Expose
    private Boolean approved;
    @SerializedName("store")
    @Expose
    private Integer store;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return The approved
     */
    public Boolean getApproved() {
        return approved;
    }

    /**
     * @param approved The approved
     */
    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    /**
     * @return The store
     */
    public Integer getStore() {
        return store;
    }

    /**
     * @param store The store
     */
    public void setStore(Integer store) {
        this.store = store;
    }
}