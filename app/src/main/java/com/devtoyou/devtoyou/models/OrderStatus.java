package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 09/01/16.
 */
public class OrderStatus {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("driver_location")
    @Expose
    private List<OrderDriverLocation> driverLocation = new ArrayList<OrderDriverLocation>();
    @SerializedName("changes")
    @Expose
    private List<Change> changes = new ArrayList<Change>();
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The driverLocation
     */
    public List<OrderDriverLocation> getDriverLocation() {
        return driverLocation;
    }

    /**
     * @param driverLocation The driver_location
     */
    public void setDriverLocation(List<OrderDriverLocation> driverLocation) {
        this.driverLocation = driverLocation;
    }

    /**
     * @return The changes
     */
    public List<Change> getChanges() {
        return changes;
    }

    /**
     * @param changes The changes
     */
    public void setChanges(List<Change> changes) {
        this.changes = changes;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
