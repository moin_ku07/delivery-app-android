package com.devtoyou.devtoyou.models;

/**
 * Created by motaz on 30/12/15.
 */
public class Constants {
    public static String FAVORITE_ITEM_POSITION = "FAVORITE_ITEM_POSITION";
    public static String CUSTOMER = "customer";
    public static String STORE = "store_owner";
    public static String DRIVER = "driver";

    public static String ORDER_ID = "ORDER_ID";
    public static String ORDER_STATUS = "ORDER_STATUS";
    public static String DRIVER_ID = "DRIVER_ID";
    public static String DRIVER_LOCATION = "DRIVER_LOCATION";
    public static String POSITION = "POSITION";
    public static String DRIVER_AMOUNT = "DRIVER_AMOUNT";
    public static String USER_ID = "USER_ID";
    public static String STORE_ID = "STORE_ID";
    public static String DRIVER_ORDER_STATUS = "DRIVER_ORDER_STATUS";
    public static String CART_ITEMS = "CART_ITEMS";
    public static String GCM_TOKEN = "GCM_TOKEN";
    public static String REGISTRATION_ID = "REGISTRATION_ID";
    public static String DEVICE_ID = "DEVICE_ID";
    public static String ACTIVE_FLAG = "ACTIVE_FLAG";
    public static String UUID = "UUID";
    public static String NOTIFICATIONS_ON = "NOTIFICATIONS_ON";
    public static String GCM_TOKEN_UPDATED_ON_SERVER = "GCM_TOKEN_UPDATED_ON_SERVER";
    public static String AUTHENTICATION_TOKEN = "AUTHENTICATION_TOKEN";
    public static String PHOTO_PATH = "PHOTO_PATH";
    public static String ITEM_NAME = "ITEM_NAME";
    public static String ITEM_PRICE = "ITEM_PRICE";
    public static String ITEM_DESCP = "ITEM_DESCP";
    public static String LOCATION_ID = "LOCATION_ID";
    public static String USER_NEW_PASSWORD = "USER_NEW_PASSWORD";
    public static String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static String PROFILE_ID = "PROFILE_ID";
    public static String SERVICE_NAME = "SERVICE_NAME";
    public static String USER_TYPE = "USER_TYPE";
    public static String USER = "USER";
    public static String FROM_NOTIFICATIONS = "FROM_NOTIFICATIONS";
    public static String SELECTED_LOCATION = "SELECTED_LOCATION";
    public static String ALL_DISTRICTS = "ALL_DISTRICTS";
    public static String SEARCH_QUERY = "SEARCH_QUERY";
    public static String SEARCH_QUERY_RESULT = "SEARCH_QUERY_RESULT";
    public static String SUCCESS = "SUCCESS";
}
