package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durlov-L3 on 11/21/2015.
 */
public class OrdersHistory {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("avg_rating")
    @Expose
    private Integer avgRating;
    @SerializedName("display_id")
    @Expose
    private String displayId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("additional_notes")
    @Expose
    private String additionalNotes;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("items")
    @Expose
    private List<OrderItem> items = new ArrayList<OrderItem>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The avgRating
     */
    public Integer getAvgRating() {
        return avgRating;
    }

    /**
     * @param avgRating The avgRating
     */
    public void setAvgRating(Integer avgRating) {
        this.avgRating = avgRating;
    }

    /**
     * @return The displayId
     */
    public String getDisplayId() {
        return displayId;
    }

    /**
     * @param displayId The displayId
     */
    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The pickup
     */
    public String getPickup() {
        return pickup;
    }

    /**
     * @param pickup The pickup
     */
    public void setUser(String pickup) {
        this.pickup = pickup;
    }

    /**
     * @return The additionalNotes
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * @param additionalNotes The additionalNotes
     */
    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    /**
     * @return The driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver The driver
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }

    /**
     * @return The total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * @return The user_type
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType The userType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return The created_at
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The items
     */
    public List<OrderItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

}
