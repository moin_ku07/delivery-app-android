package com.devtoyou.devtoyou.models;

/**
 * Created by motaz on 11/01/16.
 */
public class OrderStatusItem {
    public int orderID = -1;
    public int storeID = -1;
    public String storeName;
    public boolean isInStore;
    public boolean leaveStore;
    public boolean hasGoneToStore;
}
