package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 14/01/16.
 */
public class UpdateOrderStatusRequest {

    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("store")
    @Expose
    private Integer store;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * @return The order
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order The order
     */
    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * @return The store
     */
    public Integer getStore() {
        return store;
    }

    /**
     * @param store The store
     */
    public void setStore(Integer store) {
        this.store = store;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
