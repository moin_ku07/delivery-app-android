package com.devtoyou.devtoyou.models;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 11/4/15.
 */
public class AppData {
    public int user_id;
    public Store[] allStores;
    public Store[] fvrStores;
    public AppSettings[] appSettings;
    public UserNotification[] allNotifications;
    public OrdersHistory[] allOrdersHistory;
    private static AppData singelton;
    private Context context;
    public StoreItem[] allStoresItems;
    public ArrayList<CartItem> cartItems;
    public User user;
    public Order[] orders;
    public boolean from_place_order_flag = false;
    public String user_type;
    public int store_id;
    public FavoriteItem[] favoriteItems;
    public DriverOrder[] driverOrders;
    public DriverOrder current_driver_order;
    public boolean cartItemsHasLoadedBefore = false;
    public String auth_token;
    public OrderStatus orderStatus;
    public StoreItem[] specificStoreItems;
    public boolean skipFlag;
    public District[] allDistricts;
    public boolean guestMode;

    private AppData(Context context) {
        this.context = context;
        cartItems = new ArrayList<>();
    }

    public static AppData getInstance(Context context) {
        if (singelton != null) {
            return singelton;
        } else {
            singelton = new AppData(context);
            return singelton;
        }
    }

    public void init() {
        if (!skipFlag) {
            loadSavedCartItems();
            getUser();
            getAllStores();
            getAllFavouriteStores();
            getAllStoreItems();
            getAllNotification();
            getOrdersHistory();
            getAllOrders();
            getAppSetting();
            getFavouriteItems();
        }
    }

    public void getAllDistricts() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_DISTRICTS);
        context.startService(i);
    }

    private void loadSavedCartItems() {
        if (!cartItemsHasLoadedBefore) {
            cartItemsHasLoadedBefore = true;
            SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CART_ITEMS, 0);
            String cartItemsStr = sharedPreferences.getString(Constants.CART_ITEMS, "");
            if (!cartItemsStr.equals("")) {
                Type type = new TypeToken<ArrayList<CartItem>>() {
                }.getType();

                Gson gson = new Gson();
                ArrayList<CartItem> cartItemsList = gson.fromJson(cartItemsStr, type);
                if (cartItemsList != null && cartItemsList.size() > 0) {
                    cartItems = cartItemsList;
                }
            }
        }
    }

    public void getAllOrders() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_ORDERS);
        i.putExtra(Constants.USER_TYPE, user_type);
        context.startService(i);
    }

    private void getUser() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_USER_INFO);

        context.startService(i);
    }

    public void getAllStoreItems() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_STORE_ITEMS);
        context.startService(i);
    }

    public void getAllStores() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_STORES);
        context.startService(i);
    }

    public void getAllFavouriteStores() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_FAVOURITE_STORES);
        context.startService(i);
    }

    public void getFavouriteItems() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_FAVORITE_ITEMS);
        context.startService(i);
    }

    private void getAppSetting() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_APP_SETTINGS);
        context.startService(i);
    }

    public void getAllNotification() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_NOTIFICATIONS);
        context.startService(i);
    }

    public void getOrdersHistory() {
        Intent i = new Intent(context, AppNetworkService.class);
        i.setAction(Actions.GET_ALL_ORDERS_HISTORY);
        context.startService(i);
    }

    public void storeCartItems(String cartItemsStr) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CART_ITEMS, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.CART_ITEMS, cartItemsStr);
        editor.commit();
    }
}
