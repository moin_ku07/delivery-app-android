package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 11/6/15.
 */
public class User {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("locations")
    @Expose
    private List<Location> locations = new ArrayList<Location>();
    @SerializedName("avg_rating")
    @Expose
    private Object avgRating;
    @SerializedName("last_login")
    @Expose
    private Object lastLogin;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("date_joined")
    @Expose
    private String dateJoined;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("loyalty_amount")
    @Expose
    private Double loyaltyAmount;
    @SerializedName("deposit_amount")
    @Expose
    private Double depositAmount;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("store_id")
    @Expose
    private int store_id;

    @SerializedName("auth_token")
    @Expose
    public String auth_token;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The locations
     */
    public List<Location> getLocations() {
        return locations;
    }

    /**
     * @param locations The locations
     */
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    /**
     * @return The avgRating
     */
    public Object getAvgRating() {
        return avgRating;
    }

    /**
     * @param avgRating The avg_rating
     */
    public void setAvgRating(Object avgRating) {
        this.avgRating = avgRating;
    }

    /**
     * @return The lastLogin
     */
    public Object getLastLogin() {
        return lastLogin;
    }

    /**
     * @param lastLogin The last_login
     */
    public void setLastLogin(Object lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The dateJoined
     */
    public String getDateJoined() {
        return dateJoined;
    }

    /**
     * @param dateJoined The date_joined
     */
    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    /**
     * @return The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType The user_type
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The loyaltyAmount
     */
    public Double getLoyaltyAmount() {
        return loyaltyAmount;
    }

    /**
     * @param loyaltyAmount The loyalty_amount
     */
    public void setLoyaltyAmount(Double loyaltyAmount) {
        this.loyaltyAmount = loyaltyAmount;
    }

    /**
     * @return The depositAmount
     */
    public Double getDepositAmount() {
        return depositAmount;
    }

    /**
     * @param depositAmount The deposit_amount
     */
    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    /**
     * @return The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
}
