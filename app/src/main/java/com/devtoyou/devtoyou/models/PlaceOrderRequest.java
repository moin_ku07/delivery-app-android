package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by motaz on 11/7/15.
 */
public class PlaceOrderRequest {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("additional_notes")
    @Expose
    private String additionalNotes;
    @SerializedName("driver")
    @Expose
    private Object driver;
    @SerializedName("user_location")
    @Expose
    private Integer userLocation;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("payment_method")
    @Expose
    private int paymentMethod;
    @SerializedName("items")
    @Expose
    private List<CartItem> items = new ArrayList<CartItem>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * @return The additionalNotes
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * @param additionalNotes The additional_notes
     */
    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    /**
     * @return The driver
     */
    public Object getDriver() {
        return driver;
    }

    /**
     * @param driver The driver
     */
    public void setDriver(Object driver) {
        this.driver = driver;
    }

    /**
     * @return The userLocation
     */
    public Integer getUserLocation() {
        return userLocation;
    }

    /**
     * @param userLocation The user_location
     */
    public void setUserLocation(Integer userLocation) {
        this.userLocation = userLocation;
    }

    /**
     * @return The pickup
     */
    public String getPickup() {
        return pickup;
    }

    /**
     * @param pickup The pickup
     */
    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    /**
     * @return The total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return The paymentMethod
     */
    public int getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return The items
     */
    public List<CartItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<CartItem> items) {
        this.items = items;
    }
}
