package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 15/01/16.
 */
public class UpdateUserBalanceUpdateRequest {

    @SerializedName("amount")
    @Expose
    private Object amount;
    @SerializedName("user")
    @Expose
    private Object user;
    @SerializedName("store")
    @Expose
    private Object store;
    @SerializedName("given_by")
    @Expose
    private Object givenBy;

    /**
     * @return The amount
     */
    public Object getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Object amount) {
        this.amount = amount;
    }

    /**
     * @return The user
     */
    public Object getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(Object user) {
        this.user = user;
    }

    /**
     * @return The store
     */
    public Object getStore() {
        return store;
    }

    /**
     * @param store The store
     */
    public void setStore(Object store) {
        this.store = store;
    }

    /**
     * @return The givenBy
     */
    public Object getGivenBy() {
        return givenBy;
    }

    /**
     * @param givenBy The given_by
     */
    public void setGivenBy(Object givenBy) {
        this.givenBy = givenBy;
    }

}
