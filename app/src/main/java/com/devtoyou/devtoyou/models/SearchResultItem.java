package com.devtoyou.devtoyou.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by motaz on 10/04/16.
 */
public class SearchResultItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("avg_rating")
    @Expose
    private Object avgRating;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("avg_time_spent")
    @Expose
    private Integer avgTimeSpent;
    @SerializedName("favorite")
    @Expose
    private Boolean favorite;
    @SerializedName("delivery_charge")
    @Expose
    private Double deliveryCharge;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("store_type")
    @Expose
    private Integer storeType;
    @SerializedName("type")
    @Expose
    private String type;
    @Expose
    private String price;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The avgRating
     */
    public Object getAvgRating() {
        return avgRating;
    }

    /**
     * @param avgRating The avg_rating
     */
    public void setAvgRating(Object avgRating) {
        this.avgRating = avgRating;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * @param thumb The thumb
     */
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The avgTimeSpent
     */
    public Integer getAvgTimeSpent() {
        return avgTimeSpent;
    }

    /**
     * @param avgTimeSpent The avg_time_spent
     */
    public void setAvgTimeSpent(Integer avgTimeSpent) {
        this.avgTimeSpent = avgTimeSpent;
    }

    /**
     * @return The favorite
     */
    public Boolean getFavorite() {
        return favorite;
    }

    /**
     * @param favorite The favorite
     */
    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    /**
     * @return The deliveryCharge
     */
    public Double getDeliveryCharge() {
        return deliveryCharge;
    }

    /**
     * @param deliveryCharge The delivery_charge
     */
    public void setDeliveryCharge(Double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The storeType
     */
    public Integer getStoreType() {
        return storeType;
    }

    /**
     * @param storeType The store_type
     */
    public void setStoreType(Integer storeType) {
        this.storeType = storeType;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
