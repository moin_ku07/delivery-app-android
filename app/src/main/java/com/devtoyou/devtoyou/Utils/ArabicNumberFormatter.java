package com.devtoyou.devtoyou.Utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by motaz on 09/05/16.
 */
public class ArabicNumberFormatter {
    public static String formatNumberInArabic(Number number) {
        Locale arabic = new Locale("ar");
        NumberFormat format = NumberFormat.getInstance(arabic);
        format.setMaximumFractionDigits(2);
        return format.format(number);
    }

}
