package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Utils.ArabicNumberFormatter;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.CartItem;
import com.devtoyou.devtoyou.models.StoreItem;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by motaz on 27/01/16.
 */
public class StoreItemsListViewAdapter extends ArrayAdapter<String[]> {
    public View.OnClickListener listener;
    private final Context context;
    private StoreItem[] values;
    public StoreItem storeItem;


    public StoreItemsListViewAdapter(Context context, StoreItem[] values) {
        super(context, R.layout.home_screen_popup_row);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.home_screen_popup_row, parent, false);

        storeItem = values[position];
        ImageView itemImage = (ImageView) rowView.findViewById(R.id.image);
        if (storeItem.getPhoto() != null) {
            itemImage.setBackgroundResource(0);
            Picasso.with(getContext()).load(storeItem.getPhoto()).into(itemImage);
        }
        TextView itemTitle = (TextView) rowView.findViewById(R.id.title);
        itemTitle.setText(storeItem.getName());

        TextView itemDescp = (TextView) rowView.findViewById(R.id.item_description);
        itemDescp.setText(storeItem.getDescription());

        TextView itemPrice = (TextView) rowView.findViewById(R.id.price);
        itemPrice.setText(ArabicNumberFormatter.formatNumberInArabic(storeItem.getPrice()));

        final Button add = (Button) rowView.findViewById(R.id.addBtn);

        if (storeItem.inCart) {
            add.setText("مضاف للسلة");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                add.setBackground(getContext().getResources().getDrawable(R.drawable.added_button_style, getContext().getTheme()));
            } else {
                add.setBackground(getContext().getResources().getDrawable(R.drawable.added_button_style));
            }
        }

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                storeItem = values[position];
                if (!storeItem.inCart) {
                    CartItem cartItem = new CartItem();
                    cartItem.setStoreItem(storeItem.getId());
                    cartItem.setPrice(storeItem.getPrice());
                    cartItem.setQuantity(1);
                    AppData.getInstance(getContext()).cartItems.add(cartItem);
                    add.setText("مضاف للسلة");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        add.setBackground(getContext().getResources().getDrawable(R.drawable.added_button_style, getContext().getTheme()));
                    } else {
                        add.setBackground(getContext().getResources().getDrawable(R.drawable.added_button_style));
                    }
                    storeItem.inCart = true;

                    Intent i = new Intent(Actions.ADD_ITEM_TO_CART);
                    getContext().getApplicationContext().sendBroadcast(i);
                }
            }
        });
        return rowView;
    }
}

