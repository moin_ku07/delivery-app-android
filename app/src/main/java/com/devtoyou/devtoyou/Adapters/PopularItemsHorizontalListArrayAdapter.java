package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Utils.ArabicNumberFormatter;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.FavoriteItem;
import com.squareup.picasso.Picasso;

/**
 * Created by motaz on 25/12/15.
 */
public class PopularItemsHorizontalListArrayAdapter extends ArrayAdapter<FavoriteItem> {
    public Object[] values;

    public PopularItemsHorizontalListArrayAdapter(Context context, int resource, FavoriteItem[] objects) {
        super(context, resource, objects);
        this.values = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View itemView = null;
        if (position % 2 == 0)
            itemView = layoutInflater.inflate(R.layout.popular_item_view_in_slider, parent, false);
        else
            itemView = layoutInflater.inflate(R.layout.popular_item_view_in_slider_with_popwindow_down, parent, false);

        FavoriteItem favoriteItem = (FavoriteItem) values[position];

        TextView itemName = (TextView) itemView.findViewById(R.id.item_name);
        itemName.setText(favoriteItem.getName());

        TextView itemPrice = (TextView) itemView.findViewById(R.id.item_price);
        itemPrice.setText(ArabicNumberFormatter.formatNumberInArabic(favoriteItem.getPrice()));

        RatingBar ratingBar = (RatingBar) itemView.findViewById(R.id.item_ratingBar);
        ratingBar.setRating(favoriteItem.getAvgRating());

        ImageView popularItemImage = (ImageView) itemView.findViewById(R.id.popular_item_image);
        if (favoriteItem.getPhoto() != null)
            Picasso.with(getContext()).load(favoriteItem.getPhoto()).into(popularItemImage);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Actions.SHOW_FAVORITE_ITEM_DETAILS);
                i.putExtra(Constants.FAVORITE_ITEM_POSITION, position);
                getContext().sendBroadcast(i);
            }
        });

        return itemView;
    }

    @Override
    public int getCount() {
        return values.length;
    }
}
