package com.devtoyou.devtoyou.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.devtoyou.devtoyou.Fragments.HomePageFragment;

/**
 * Created by motaz on 29/02/16.
 */
public class SkipPagerAdapter extends FragmentPagerAdapter {
    public SkipPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new HomePageFragment();
    }

    @Override
    public int getCount() {
        return 1;
    }

}
