package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.District;

/**
 * Created by motaz on 19/04/16.
 */
public class DistrictSpinnerAdapter extends ArrayAdapter<District>{
    public DistrictSpinnerAdapter(Context context, int resource, District[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(getContext()).inflate(R.layout.sex_spinner_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text1);
        textView.setText(getItem(position).getName());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.my_order_fragment_backgroundColor, getContext().getTheme()));
            } else {
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.my_order_fragment_backgroundColor));
            }
        return rowView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(getContext()).inflate(R.layout.sex_spinner_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text1);
        textView.setText(getItem(position).getName());
        return rowView;
    }
}
