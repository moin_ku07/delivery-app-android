package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.TrackOrderActivity;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.DriverOrderItem;
import com.devtoyou.devtoyou.models.Order;
import com.google.gson.Gson;

/**
 * Created by motaz on 04/01/16.
 */
public class MyOrdersListAdapter extends ArrayAdapter<Order[]> {
    private final Context context;
    private Order[] values;

    public MyOrdersListAdapter(Context context, Order[] values) {
        super(context, R.layout.my_order_row);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.my_order_row, parent, false);

        TextView order_items = (TextView) rowView.findViewById(R.id.order_items);
        order_items.setText(getOrderItemsText(values[position]));

        Button trackOrder = (Button) rowView.findViewById(R.id.trackBtn);
        trackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                AppCache.cache.put(Actions.CURRENT_ORDER, gson.toJson(values[position]));
                getContext().startActivity(new Intent(getContext(), TrackOrderActivity.class));
            }
        });

        final Button cancelButton = (Button) rowView.findViewById(R.id.cancelBtn);
        Order order = values[position];
        if (order.getStatus().equals("REQ") || order.getStatus().equals("REV") || order.getStatus().equals("APPR")) {
            cancelButton.setVisibility(View.VISIBLE);
        }

        return rowView;
    }

    private String getOrderItemsText(Order order) {
        String orderItemsText = "";
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            orderItemsText += item.getItemName() + " * " + item.getQuantity() + " من " + item.getStoreName();
            if (i != order.getItems().size() - 1) {
                orderItemsText += "\n";
            }
        }
        return orderItemsText;
    }

}
