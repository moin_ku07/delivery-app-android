package com.devtoyou.devtoyou.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.devtoyou.devtoyou.Fragments.MyOrderFragment;
import com.devtoyou.devtoyou.Fragments.MyProfileFragment;
import com.devtoyou.devtoyou.Fragments.NotificationsFragment;

/**
 * Created by motaz on 04/01/16.
 */
public class DriverPagerAdapter extends FragmentPagerAdapter {
    public static final int TABS_COUNT = 3;

    public DriverPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NotificationsFragment();
            case 1:
                return new MyOrderFragment();
            case 2:
                return new MyProfileFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Notification";
            case 1:
                return "My Order";
            case 2:
                return "My Profile";
        }
        return "";
    }
}
