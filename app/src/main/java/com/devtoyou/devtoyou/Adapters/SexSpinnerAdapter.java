package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;

/**
 * Created by motaz on 06/02/16.
 */
public class SexSpinnerAdapter extends ArrayAdapter<String> {

    public boolean isForAccountSettings = false;

    public SexSpinnerAdapter(Context context, int resource, String[] values) {
        super(context, resource, values);
    }

    public void setForAccountSettings(boolean forAccountSettings) {
        isForAccountSettings = forAccountSettings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(getContext()).inflate(R.layout.sex_spinner_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text1);
        textView.setText(getItem(position));
        if (!isForAccountSettings)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.sex_spinner_background_color, getContext().getTheme()));
            } else {
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.sex_spinner_background_color));
            }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.sex_spinner_account_settings_background_color, getContext().getTheme()));
            else
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.sex_spinner_account_settings_background_color));
        }
        return rowView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View rowView = LayoutInflater.from(getContext()).inflate(R.layout.sex_spinner_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text1);
        textView.setText(getItem(position));
        return rowView;
    }
}
