package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.StorePhoto;
import com.squareup.picasso.Picasso;

/**
 * Created by motaz on 30/01/16.
 */
public class StorePhotosAdapter extends ArrayAdapter<StorePhoto> {
    public StorePhotosAdapter(Context context, int resource, StorePhoto[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.store_photos_element_view, parent, false);
        StorePhoto storePhoto = getItem(position);
        ImageView storePhotoImage = (ImageView) view.findViewById(R.id.store_image);
        if (storePhoto.getPhoto() != null) {
            Picasso.with(getContext()).load(storePhoto.getPhoto()).into(storePhotoImage);
        }
        return view;
    }
}
