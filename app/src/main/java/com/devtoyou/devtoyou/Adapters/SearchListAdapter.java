package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.devtoyou.devtoyou.CustomViews.CustomPlainTextView;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.SearchResultItem;
import com.squareup.picasso.Picasso;

import java.util.Collection;

/**
 * Created by motaz on 10/04/16.
 */
public class SearchListAdapter extends ArrayAdapter<SearchResultItem> {
    public SearchListAdapter(Context context, int resource, SearchResultItem[] objects) {
        super(context, resource, objects);
    }

    @Override
    public void addAll(Collection<? extends SearchResultItem> collection) {
        for (SearchResultItem searchResultItem : collection) {
            add(searchResultItem);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.search_list_view_item, parent, false);
        SearchResultItem searchResultItem = getItem(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        CustomPlainTextView name = (CustomPlainTextView) view.findViewById(R.id.name);
        CustomPlainTextView descp = (CustomPlainTextView) view.findViewById(R.id.descp);
        CustomPlainTextView price = (CustomPlainTextView) view.findViewById(R.id.price);

        Picasso.with(getContext()).load(searchResultItem.getThumb());
        name.setText(searchResultItem.getName());
        descp.setText(searchResultItem.getDescription());
        if (searchResultItem.getType().equals("store_item")) {
            price.setVisibility(View.VISIBLE);
            price.setText(searchResultItem.getPrice());
        } else {
            price.setVisibility(View.GONE);
        }
        return view;
    }
}
