package com.devtoyou.devtoyou.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.devtoyou.devtoyou.Activities.DriverOrderDetailsActivity;
import com.devtoyou.devtoyou.NetworkServices.AppNetworkService;
import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.DriverOrderItem;

/**
 * Created by motaz on 09/01/16.
 */
public class DriverOrdersListAdapter extends ArrayAdapter<DriverOrder> {
    public DriverOrder[] orders;

    public DriverOrdersListAdapter(Context context, int resource, DriverOrder[] objects) {
        super(context, resource, objects);
        this.orders = objects;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item_view = inflater.inflate(R.layout.driver_orders_list_item_view, parent, false);
        final DriverOrder order = orders[position];
        TextView orderDetails = (TextView) item_view.findViewById(R.id.order_items);
        orderDetails.setText(getOrderItemsText(order));

        TextView orderAddress = (TextView) item_view.findViewById(R.id.order_address);
        orderAddress.setText(order.getUserLocation().getName());

        Button orderDetailsBtn = (Button) item_view.findViewById(R.id.orderDetailsBtn);
        orderDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), DriverOrderDetailsActivity.class);
                i.putExtra(Constants.ORDER_ID, order.getId());
                getContext().startActivity(i);
            }
        });

        Button acceptOrder = (Button) item_view.findViewById(R.id.acceptOrder);
        if (order.getStatus().equals("AD")) {
            acceptOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getContext(), AppNetworkService.class);
                    i.setAction(Actions.ACCEPT_ORDER_BY_DRIVER);
                    i.putExtra(Constants.DRIVER_ID, order.getDriver());
                    i.putExtra(Constants.ORDER_ID, order.getId());
                    getContext().startService(i);
                }
            });
            acceptOrder.setVisibility(View.VISIBLE);
        }
        return item_view;
    }

    private String getOrderItemsText(DriverOrder order) {
        String str = "";
        for (int i = 0; i < order.getItems().size(); i++) {
            DriverOrderItem item = order.getItems().get(i);
            str += item.getItemName() + " * " + item.getQuantity() + " من " + item.getStoreName();
            if (i != order.getItems().size() - 1) {
                str += "\n";
            }
        }
        return str;
    }
}
