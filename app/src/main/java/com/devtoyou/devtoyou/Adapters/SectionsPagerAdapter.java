package com.devtoyou.devtoyou.Adapters;

/**
 * Created by motaz on 21/12/15.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.devtoyou.devtoyou.Fragments.CartFragment;
import com.devtoyou.devtoyou.Fragments.HomePageFragment;
import com.devtoyou.devtoyou.Fragments.MyOrderFragment;
import com.devtoyou.devtoyou.Fragments.MyProfileFragment;
import com.devtoyou.devtoyou.Fragments.NotificationsFragment;
/**
 * A  FragmentPagerAdapter that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        // each page corresponds to a new fragment
        // I'll return the same fragment for now
        Fragment frag = null;
        if (i == 0) {
            frag = new HomePageFragment();
        }
        if (i == 1) {
            frag = new NotificationsFragment();
        }
        if (i == 2) {
            frag = new CartFragment();
        }
        if (i == 3) {
            frag = new MyOrderFragment();
        }
        if (i == 4) {
            frag = new MyProfileFragment();
        }
        return frag;
    }

    @Override
    public int getCount() {
        // Show 5 total pages.
        return 5;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        // return the page title
        String title = new String();
        if (position == 0) {
            title = "الرئيسية";
        }
        if (position == 1) {
            title = "التنبيهات";
        }
        if (position == 2) {
            title = "انهاء الطلب";
        }
        if (position == 3) {
            title = "طلباتي";
        }
        if (position == 4) {
            title = "ملفي الشخصي";
        }
        return title;
    }

}

