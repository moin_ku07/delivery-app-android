package com.devtoyou.devtoyou.CustomViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devtoyou.devtoyou.R;

/**
 * Created by motaz on 12/01/16.
 */
public class OrderStatusItemView extends TextView {
    public OrderStatusItemView(Context context) {
        super(context);
    }

    public void init(String text) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(48));
        layoutParams.setMargins(0, dpToPx(10), 0, dpToPx(5));
        this.setLayoutParams(layoutParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.setBackgroundColor(getResources().getColor(R.color.order_status_item_active_color, getContext().getTheme()));
        } else {
            this.setBackgroundColor(getResources().getColor(R.color.order_status_item_active_color));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.setTextColor(getResources().getColor(R.color.white, getContext().getTheme()));
        } else {
            this.setTextColor(getResources().getColor(R.color.white));
        }
        this.setText(text);
        this.setGravity(Gravity.CENTER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.setTextDirection(TEXT_DIRECTION_ANY_RTL);
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
