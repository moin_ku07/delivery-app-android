package com.devtoyou.devtoyou.CustomViews;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by motaz on 20/02/16.
 */
public class CustomPlainTextView extends TextView {

    public CustomPlainTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (this.getTypeface() != null && this.getTypeface().getStyle() == Typeface.BOLD) {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "bold.ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "plain.ttf"));
        }
    }

    public CustomPlainTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (this.getTypeface() != null && this.getTypeface().getStyle() == Typeface.BOLD) {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "bold.ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "plain.ttf"));
        }
    }

    public CustomPlainTextView(Context context) {
        super(context);
        if (this.getTypeface() != null && this.getTypeface().getStyle() == Typeface.BOLD) {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "bold.ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "plain.ttf"));
        }
    }
}
