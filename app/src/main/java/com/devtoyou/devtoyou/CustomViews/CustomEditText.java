package com.devtoyou.devtoyou.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by motaz on 20/02/16.
 */
public class CustomEditText extends EditText {
    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (this.getTypeface() != null && this.getTypeface().getStyle() == Typeface.BOLD) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "bold.ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "plain.ttf"));
        }
    }
}
