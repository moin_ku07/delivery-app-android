package com.devtoyou.devtoyou.Api;

import com.devtoyou.devtoyou.models.AcceptOrderByDriverRequest;
import com.devtoyou.devtoyou.models.DeliverOrderRequest;
import com.devtoyou.devtoyou.models.District;
import com.devtoyou.devtoyou.models.DriverLocation;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.FavoriteItem;
import com.devtoyou.devtoyou.models.Location;
import com.devtoyou.devtoyou.models.Order;
import com.devtoyou.devtoyou.models.OrderStatus;
import com.devtoyou.devtoyou.models.OrdersHistory;
import com.devtoyou.devtoyou.models.PlaceOrderRequest;
import com.devtoyou.devtoyou.models.PlaceOrderResponse;
import com.devtoyou.devtoyou.models.SearchResultItem;
import com.devtoyou.devtoyou.models.SocialLoginRequest;
import com.devtoyou.devtoyou.models.Store;
import com.devtoyou.devtoyou.models.StoreItem;
import com.devtoyou.devtoyou.models.AppSettings;
import com.devtoyou.devtoyou.models.StorePhoto;
import com.devtoyou.devtoyou.models.UpdateDriverLocationRequest;
import com.devtoyou.devtoyou.models.UpdateDriverLocationResponse;
import com.devtoyou.devtoyou.models.UpdateGCMRequest;
import com.devtoyou.devtoyou.models.UpdateGCMResponse;
import com.devtoyou.devtoyou.models.UpdateOrderStatusRequest;
import com.devtoyou.devtoyou.models.UpdateOrderStatusResponse;
import com.devtoyou.devtoyou.models.UpdateUserBalanceUpdateRequest;
import com.devtoyou.devtoyou.models.UpdateUserBalanceUpdateResponse;
import com.devtoyou.devtoyou.models.User;
import com.devtoyou.devtoyou.models.UserNotification;
import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by motaz on 11/4/15.
 */
public interface ApiInterface {
    @GET("api/stores/")
    Call<Store[]> getAllStores();

    @GET("api/stores/")
    Call<Store[]> getAllFavouriteStores(@Query("favorite") boolean favourite);

    @GET("api/store-items/")
    Call<StoreItem[]> getAllStoreItems();

    @GET("api/store-items/")
    Call<StoreItem[]> getAllStoreItems(@Query("store") int store_id);

    @GET("api/customers/{user_id}/")
    Call<User> getUserInfo(@Path("user_id") int user_id);

    @GET("api/drivers/{user_id}/")
    Call<User> getDriverInfo(@Path("user_id") int user_id);

    @GET("api/stores/{user_id}/")
    Call<User> getStoreOwnerInfo(@Path("user_id") int user_id);


    @POST("api/orders/")
    Call<PlaceOrderResponse> placeOrder(@Body PlaceOrderRequest placeOrderRequest);

    @POST("api/user-locations/")
    Call<Location> createNewLocation(@Body Location newlocation);

    @DELETE("api/user-locations/{location_id}/")
    Call<Void> deleteLocation(@Header("Authorization") String auth_token, @Path("location_id") int locationID);

    @GET("api/user-notification/")
    Call<UserNotification[]> getAllNotification(@Query("user") int user_id);

    @Multipart
    @PATCH("api/customers/{user_id}/")
    Call<User> updateProfilePicture(@Part("photo\"; filename=\"image.png\" ") RequestBody photo, @Path("user_id") int userId);

    @GET("api/orders/")
    Call<Order[]> getCustomerOrders(@Query("exclude_delivered") boolean excludeDeliveredFlag, @Query("exclude_cancelled") boolean excludeCancelledFlag, @Query("include_store_location") boolean include_store_location, @Query("user") int user_id);

    @GET("api/orders/")
    Call<Order[]> getStoreOwnerOrders(@Query("exclude_delivered") boolean excludeDeliveredFlag, @Query("exclude_cancelled") boolean excludeCancelledFlag, @Query("store") int store_owner_id);

    @GET("api/orders/")
    Call<DriverOrder[]> getDriverOrders(@Query("exclude_delivered") boolean excludeDeliveredFlag, @Query("exclude_cancelled") boolean excludeCancelledFlag,
                                        @Query("driver") int driver_id, @Query("include_address") boolean include_address, @Query("include_store_location") boolean include_store_location);

    @GET("api/app-settings")
    Call<AppSettings[]> getAppSettings();

    @GET("api/orders/")
    Call<OrdersHistory[]> getUserOrdersHistory(@Query("user") int user_id);

    @GET("api/orders/")
    Call<OrdersHistory[]> getStoreOwnerOrdersHistory(@Query("store") int store_owner_id);

    @GET("api/orders/")
    Call<OrdersHistory[]> getDriverOrdersHistory(@Query("driver") int driver_id);

    @GET("api/store-items/?favorite=true")
    Call<FavoriteItem[]> getFavoriteItems();

    @GET("api/track-order/")
    Call<OrderStatus> getOrderStatus(@Query("order") int order_id);

    @GET("api/driver-gps/")
    Call<DriverLocation[]> getCurrentDriverLocation(@Query("driver") int driver);

    @PATCH("api/orders/{order_id}/")
    Call<Order> acceptOrderByDriver(@Path("order_id") int order_id, @Body AcceptOrderByDriverRequest acceptOrderByDriverRequest);

    @GET("api/orders/{order_id}/?exclude_delivered=true&exclude_cancelled=true&include_address=true&include_store_location=true&dis_dls=true")
    Call<DriverOrder> getDriverOrder(@Path("order_id") int order_id);

    @POST("api/update-order/")
    Call<UpdateOrderStatusResponse> updateOrderStatus(@Body UpdateOrderStatusRequest request);

    @POST("api/user-balance/")
    Call<UpdateUserBalanceUpdateResponse> updateStoreBalance(@Body UpdateUserBalanceUpdateRequest updateUserBalanceUpdateRequest);

    @POST("api/user-balance/")
    Call<UpdateUserBalanceUpdateResponse> updateUserBalance(@Body UpdateUserBalanceUpdateRequest updateUserBalanceUpdateRequest);

    @PATCH("api/orders/{order_id}/")
    Call<Order> deliverOrder(@Path("order_id") int order_id, @Body DeliverOrderRequest deliverOrderRequest);

    @POST("api/driver-gps/")
    Call<UpdateDriverLocationResponse> updateDriverLocation(@Body UpdateDriverLocationRequest updateDriverLocationRequest);

    @POST("api/device/gcm/")
    Call<UpdateGCMResponse> updateGCMResponse(@Header("Authorization") String authorization, @Body UpdateGCMRequest updateGCMRequest);

    @DELETE("api/device/gcm/{registration_token}/")
    Call<Void> unRegisterFromGCM(@Header("Authorization") String authorization, @Path("registration_token") String registration_token);

    @GET("api/store-photos/")
    Call<StorePhoto[]> getStorePhotos(@Query("store") int store_id);

    @Multipart
    @POST("api/store-items/")
    Call<StoreItem> submitStoreItem(@Part("photo\"; filename=\"image.png\" ") RequestBody photo,
                                    @Part("name") String item_name,
                                    @Part("description") String descp,
                                    @Part("price") double price,
                                    @Part("store") int store_id,
                                    @Header("Authorization") String auth_token);

    @POST("/auth/social/")
    Call<User> loginUsingSocialAccount(@Body SocialLoginRequest socialLoginRequest);

    @GET("api/districts/")
    Call<District[]> getAllDistricts();

    @GET("api/search/")
    Call<SearchResultItem[]> searchByQuery(@Query("query") String query);

    @PATCH("api/user-locations/{id}/")
    Call<Location> updateLocation(@Path("id") int locationID, @Body Location updatedLocation);
}

