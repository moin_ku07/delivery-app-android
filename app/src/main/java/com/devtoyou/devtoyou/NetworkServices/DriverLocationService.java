package com.devtoyou.devtoyou.NetworkServices;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by motaz on 18/01/16.
 */
public class DriverLocationService extends IntentService implements GoogleApiClient.ConnectionCallbacks {
    GoogleApiClient mGoogleApiClient;

    public DriverLocationService() {
        super(DriverLocationService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if (action.equals(Actions.UPDATE_DRIVER_LOCATION)) {
            int driver_id = intent.getIntExtra(Constants.DRIVER_ID, -1);
            while (true) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(ConnectionResult connectionResult) {
                                Log.d("DriverLocation service ", "error");
                            }
                        })
                        .addApi(LocationServices.API)
                        .build();
                mGoogleApiClient.connect();
                try {
                    // sleep for 2 minutes.
                    Thread.sleep(1000 * 60 * 2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
                if (mLastLocation != null) {
                    Log.d("driver location ", mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude());
                    Intent i = new Intent(getApplicationContext(), AppNetworkService.class);
                    i.setAction(Actions.UPDATE_DRIVER_LOCATION);
                    i.putExtra(Constants.DRIVER_LOCATION, mLastLocation);
                    i.putExtra(Constants.DRIVER_ID, driver_id);
                    startService(i);
                }
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
