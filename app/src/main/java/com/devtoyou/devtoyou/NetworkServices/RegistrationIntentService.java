package com.devtoyou.devtoyou.NetworkServices;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by motaz on 18/01/16.
 */
public class RegistrationIntentService extends IntentService {

    public RegistrationIntentService() {
        super(RegistrationIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Make a call to Instance API
        InstanceID instanceID = InstanceID.getInstance(this);
        String senderId = getResources().getString(R.string.gcm_defaultSenderId_dev_to_you);
        try {



            // request token that will be used by the server to send push notifications
            String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            Log.d("GCMRegistration Token: ", token);

            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String uuid = tManager.getDeviceId();
            Log.d("UUID: ", uuid);
            // pass along this data
            sendRegistrationToServer(token, uuid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendRegistrationToServer(String token, String uuid) {
        // make network request here.
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.GCM_TOKEN, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.GCM_TOKEN, token);
        editor.putString(Constants.UUID, uuid);
        editor.commit();

        Intent i = new Intent(getApplicationContext(), AppNetworkService.class);
        i.setAction(Actions.UPDATE_DEVICE_GCM);
        i.putExtra(Constants.UUID, uuid);
        i.putExtra(Constants.GCM_TOKEN, token);
        i.putExtra(Constants.ACTIVE_FLAG, true);
        i.putExtra(Constants.AUTHENTICATION_TOKEN, AppData.getInstance(getApplicationContext()).auth_token);
        startService(i);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
