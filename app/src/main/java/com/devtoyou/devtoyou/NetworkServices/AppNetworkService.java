package com.devtoyou.devtoyou.NetworkServices;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.devtoyou.devtoyou.Api.ApiInterface;
import com.devtoyou.devtoyou.Activities.RegisterActivity;
import com.devtoyou.devtoyou.models.AcceptOrderByDriverRequest;
import com.devtoyou.devtoyou.models.Actions;
import com.devtoyou.devtoyou.models.AppCache;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.AppSettings;
import com.devtoyou.devtoyou.models.Constants;
import com.devtoyou.devtoyou.models.DeliverOrderRequest;
import com.devtoyou.devtoyou.models.District;
import com.devtoyou.devtoyou.models.DriverLocation;
import com.devtoyou.devtoyou.models.DriverOrder;
import com.devtoyou.devtoyou.models.FavoriteItem;
import com.devtoyou.devtoyou.models.Location;
import com.devtoyou.devtoyou.models.Order;
import com.devtoyou.devtoyou.models.OrderStatus;
import com.devtoyou.devtoyou.models.OrdersHistory;
import com.devtoyou.devtoyou.models.PlaceOrderRequest;
import com.devtoyou.devtoyou.models.PlaceOrderResponse;
import com.devtoyou.devtoyou.models.SearchResultItem;
import com.devtoyou.devtoyou.models.SocialLoginRequest;
import com.devtoyou.devtoyou.models.Store;
import com.devtoyou.devtoyou.models.StoreItem;
import com.devtoyou.devtoyou.models.StorePhoto;
import com.devtoyou.devtoyou.models.UpdateDriverLocationRequest;
import com.devtoyou.devtoyou.models.UpdateDriverLocationResponse;
import com.devtoyou.devtoyou.models.UpdateGCMRequest;
import com.devtoyou.devtoyou.models.UpdateGCMResponse;
import com.devtoyou.devtoyou.models.UpdateOrderStatusRequest;
import com.devtoyou.devtoyou.models.UpdateOrderStatusResponse;
import com.devtoyou.devtoyou.models.UpdateUserBalanceUpdateRequest;
import com.devtoyou.devtoyou.models.UpdateUserBalanceUpdateResponse;
import com.devtoyou.devtoyou.models.User;
import com.devtoyou.devtoyou.models.UserNotification;
import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;

import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by motaz on 11/4/15.
 */
public class AppNetworkService extends IntentService {
    public ApiInterface apiInterface;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public AppNetworkService(String name) {
        super(name);
    }

    public AppNetworkService() {
        super("App Network service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        client.interceptors().add(new AddCookiesInterceptor());
        client.interceptors().add(new ReceivedCookiesInterceptor());

        Retrofit retrofit = new Retrofit.Builder().client(client).
                baseUrl(RegisterActivity.rootURL(getApplicationContext())).
                addConverterFactory(GsonConverterFactory.create()).build();
        apiInterface = retrofit.create(ApiInterface.class);
        String action = intent.getAction();
        if (action.equals(Actions.GET_ALL_STORES)) {
            getAllStores();
        } else if (action.equals(Actions.GET_ALL_FAVOURITE_STORES)) {
            getAllFavouriteStores();
        } else if (action.equals(Actions.GET_ALL_STORE_ITEMS)) {
            int store_id = intent.getIntExtra(Constants.STORE_ID, -1);
            getAllStoresItems(store_id);
        } else if (action.equals(Actions.GET_ALL_NOTIFICATIONS)) {
            getAllNotification();
        } else if (action.equals(Actions.GET_ALL_ORDERS_HISTORY)) {
            getOrdersHistory();
        } else if (action.equals(Actions.GET_USER_INFO)) {
            getUserInfo();
        } else if (action.equals(Actions.PLACE_ORDER)) {
            placeOrder();
        } else if (action.equals(Actions.ADD_NEW_LOCATION)) {
            String newLocation = AppCache.cache.get(Actions.ADD_NEW_LOCATION);
            addNewLocation(newLocation);
        } else if (action.equals(Actions.UPDATE_PROFILE_IMAGE)) {
            updateImage(intent);
        } else if (action.equals(Actions.GET_APP_SETTINGS)) {
            getAppSettings();
        } else if (action.equals(Actions.GET_ALL_ORDERS)) {
            String user_type = intent.getStringExtra(Constants.USER_TYPE);
            getAllOrders(user_type);
        } else if (action.equals(Actions.GET_ALL_FAVORITE_ITEMS)) {
            getAllFavoriteItems();
        } else if (action.equals(Actions.GET_ORDER_STATUS)) {
            int orderID = intent.getIntExtra(Constants.ORDER_ID, -1);
            getOrderStatus(orderID);
        } else if (action.equals(Actions.GET_DRIVER_LOCATION)) {
            int driverID = intent.getIntExtra(Constants.DRIVER_ID, -1);
            getDriverLocation(driverID);
        } else if (action.equals(Actions.ACCEPT_ORDER_BY_DRIVER)) {
            int driverID = intent.getIntExtra(Constants.DRIVER_ID, -1);
            int orderID = intent.getIntExtra(Constants.ORDER_ID, -1);
            acceptOrderByDriver(driverID, orderID);
        } else if (action.equals(Actions.GET_DRIVER_ORDER)) {
            int orderID = intent.getIntExtra(Constants.ORDER_ID, -1);
            getDriverOrder(orderID);
        } else if (action.equals(Actions.UPDATE_STORE_AND_USER_BALANCE)) {
            double amount = intent.getDoubleExtra(Constants.DRIVER_AMOUNT, 0);
            int user_id = intent.getIntExtra(Constants.USER_ID, -1);
            int store_id = intent.getIntExtra(Constants.STORE_ID, -1);
            int driver_id = intent.getIntExtra(Constants.DRIVER_ID, -1);
            updateStoreAndUserBalance(amount, user_id, driver_id, store_id);
        } else if (action.equals(Actions.UPDATE_DRIVER_ORDER_STATUS)) {
            int order_id = intent.getIntExtra(Constants.ORDER_ID, -1);
            int store_id = intent.getIntExtra(Constants.STORE_ID, -1);
            String orderStatus = intent.getStringExtra(Constants.DRIVER_ORDER_STATUS);
            updateOrderStatus(order_id, store_id, orderStatus);
        } else if (action.equals(Actions.DRIVER_DELIVER_ORDER)) {
            double amount = intent.getDoubleExtra(Constants.DRIVER_AMOUNT, 0);
            int user_id = intent.getIntExtra(Constants.USER_ID, -1);
            int driver_id = intent.getIntExtra(Constants.DRIVER_ID, -1);
            int order_id = intent.getIntExtra(Constants.ORDER_ID, -1);
            deliverOrderToCustomer(amount, user_id, driver_id, order_id);
        } else if (action.equals(Actions.UPDATE_DRIVER_LOCATION)) {
            android.location.Location location = intent.getParcelableExtra(Constants.DRIVER_LOCATION);
            int driver_id = intent.getIntExtra(Constants.DRIVER_ID, -1);
            updateDriverLocation(location, driver_id);
        } else if (action.equals(Actions.UPDATE_DEVICE_GCM)) {
            String registrationID = intent.getStringExtra(Constants.GCM_TOKEN);
            String deviceID = intent.getStringExtra(Constants.UUID);
            boolean active = intent.getBooleanExtra(Constants.ACTIVE_FLAG, false);
            String auth_token = intent.getStringExtra(Constants.AUTHENTICATION_TOKEN);
            if (active)
                updateDeviceGCM(registrationID, deviceID, active, auth_token);
            else
                unRegisterDeviceGcm(registrationID, auth_token);
        } else if (action.equals(Actions.GET_STORE_PHOTOS)) {
            int storeID = intent.getIntExtra(Constants.STORE_ID, -1);
            getStorePhotos(storeID);
        } else if (action.equals(Actions.ADD_STORE_ITEM)) {
            String authToken = intent.getStringExtra(Constants.AUTHENTICATION_TOKEN);
            String itemName = intent.getStringExtra(Constants.ITEM_NAME);
            String descp = intent.getStringExtra(Constants.ITEM_DESCP);
            double price = intent.getDoubleExtra(Constants.ITEM_PRICE, 0);
            int store_id = intent.getIntExtra(Constants.STORE_ID, -1);
            String photoPath = intent.getStringExtra(Constants.PHOTO_PATH);
            addStoreItem(authToken, itemName, descp, price, store_id, photoPath);
        } else if (action.equals(Actions.DELETE_USER_LOCATION)) {
            String authToken = intent.getStringExtra(Constants.AUTHENTICATION_TOKEN);
            int locationID = intent.getIntExtra(Constants.LOCATION_ID, -1);
            deleteUserLocation(authToken, locationID);
        } else if (action.equals(Actions.LOGIN_USING_SOCIAL_ACCOUNT)) {
            String access_token = intent.getStringExtra(Constants.ACCESS_TOKEN);
            long profileID = intent.getIntExtra(Constants.PROFILE_ID, -1);
            String serviceName = intent.getStringExtra(Constants.SERVICE_NAME);
            String user_type = intent.getStringExtra(Constants.USER_TYPE);
            loginUsingSocialAccount(access_token, profileID, serviceName, user_type);
        } else if (action.equals(Actions.GET_ALL_DISTRICTS)) {
            getAllDistricts();
        } else if (action.equals(Actions.SEARCH_BY_QUERY)) {
            String query = intent.getStringExtra(Constants.SEARCH_QUERY);
            searchByQuery(query);
        } else if (action.equals(Actions.EDIT_LOCATION)) {
            int locationID = intent.getIntExtra(Constants.LOCATION_ID, -1);
            String locationStr = AppCache.cache.get(Actions.EDIT_LOCATION);
            updateLocation(locationID, locationStr);
        }
    }

    private void updateLocation(int locationID, String locationStr) {
        Log.d("Location", locationStr);
        Location updatedLocation = new Gson().fromJson(locationStr, Location.class);
        try {
            Call<Location> call = apiInterface.updateLocation(locationID, updatedLocation);
            Response<Location> response = call.execute();
            Intent i = new Intent(Actions.EDIT_LOCATION_DONE);
            if (response.isSuccess()) {
                Location location = response.body();
                AppCache.cache.put(Actions.EDIT_LOCATION_DONE, new Gson().toJson(location));
                i.putExtra(Constants.SUCCESS, true);
            } else {
                i.putExtra(Constants.SUCCESS, false);
                logRetrofitError(response);
            }
            sendBroadcast(i);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void searchByQuery(String query) {
        try {
            Call<SearchResultItem[]> call = apiInterface.searchByQuery(query);
            Response<SearchResultItem[]> response = call.execute();
            if (response.isSuccess()) {
                Gson gson = new Gson();
                String searchResultItemsStr = gson.toJson(response.body());
                AppCache.cache.put(Constants.SEARCH_QUERY_RESULT, searchResultItemsStr);
                sendBroadcast(new Intent(Actions.SEARCH_BY_QUERY_DONE));
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllDistricts() {
        try {
            Call<District[]> call = apiInterface.getAllDistricts();
            Response<District[]> response = call.execute();
            if (response.isSuccess()) {
                Gson gson = new Gson();
                AppCache.cache.put(Constants.ALL_DISTRICTS, gson.toJson(response.body()));
                sendBroadcast(new Intent(Actions.GET_ALL_DISTRICTS_DONE));
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loginUsingSocialAccount(String access_token, long profileID, String serviceName, String user_type) {
        SocialLoginRequest request = new SocialLoginRequest();
        request.setAccessToken(access_token);
        request.setProfileId(profileID);
        request.setServiceName(serviceName);
        request.setUserType(user_type);
        try {
            Call<User> call = apiInterface.loginUsingSocialAccount(request);
            Response<User> response = call.execute();
            if (response.isSuccess()) {
                Gson gson = new Gson();
                AppCache.cache.put(Constants.USER, gson.toJson(response.body()));
                Intent i = new Intent(Actions.LOGIN_USING_SOCIAL_ACCOUNT_DONE);
                sendBroadcast(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteUserLocation(String authToken, int locationID) {
        Call<Void> call = apiInterface.deleteLocation(getAuthinticationToken(authToken), locationID);
        Response<Void> response = null;
        try {
            response = call.execute();
            if (response.isSuccess()) {
                Intent i = new Intent(Actions.DELETE_USER_LOCATION_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unRegisterDeviceGcm(String registrationID, String auth_token) {
        Call<Void> call = apiInterface.unRegisterFromGCM(getAuthinticationToken(auth_token), registrationID);
        try {
            Response<Void> response = call.execute();
            if (response.isSuccess()) {
                Intent i = new Intent(Actions.UPDATE_DEVICE_GCM_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addStoreItem(String authToken, String itemName, String descp, double price, int store_id, String photoPath) {
        File photoFile = new File(photoPath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), photoFile);

        Call<StoreItem> call = apiInterface.submitStoreItem(requestBody, itemName, descp, price, store_id, authToken);
        try {
            Response<StoreItem> response = call.execute();
            if (response.isSuccess()) {
                Intent i = new Intent(Actions.ADD_STORE_ITEM_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getStorePhotos(int storeID) {
        Call<StorePhoto[]> call = apiInterface.getStorePhotos(storeID);
        try {
            Response<StorePhoto[]> response = call.execute();
            if (response.isSuccess()) {
                Gson gson = new Gson();
                StorePhoto[] storePhotos = response.body();
                AppCache.cache.put(Actions.GET_STORE_PHOTOS, gson.toJson(storePhotos));
                sendBroadcast(new Intent(Actions.GET_STORE_PHOTOS_DONE));
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void updateDeviceGCM(String registrationID, String deviceID, boolean active, String auth_token) {
        UpdateGCMRequest request = new UpdateGCMRequest();
        request.setActive(active);
        request.setDeviceId(deviceID);
        request.setRegistrationId(registrationID);

        Call<UpdateGCMResponse> call = apiInterface.updateGCMResponse(getAuthinticationToken(auth_token), request);
        try {
            Response<UpdateGCMResponse> response = call.execute();
            if (response.isSuccess()) {
                Intent i = new Intent(Actions.UPDATE_DEVICE_GCM_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getAuthinticationToken(String auth_token) {
        return "Token " + auth_token;
    }

    private void updateDriverLocation(android.location.Location location, int driver_id) {
        UpdateDriverLocationRequest request = new UpdateDriverLocationRequest();
        request.setLatitude(location.getLatitude() + "");
        request.setLongitude(location.getLongitude() + "");
        request.setDriver(driver_id);
        Call<UpdateDriverLocationResponse> call = apiInterface.updateDriverLocation(request);
        try {
            Response<UpdateDriverLocationResponse> response = call.execute();
            if (response.isSuccess()) {
                Log.e("DriverLocation Update: ", "Success");
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deliverOrderToCustomer(double amount, int user_id, int driver_id, int order_id) {
        UpdateUserBalanceUpdateRequest request = new UpdateUserBalanceUpdateRequest();
        request.setAmount(amount);
        request.setUser(user_id);
        request.setGivenBy(driver_id);

        Call<UpdateUserBalanceUpdateResponse> call = apiInterface.updateUserBalance(request);
        try {
            Response<UpdateUserBalanceUpdateResponse> response = call.execute();
            if (response.isSuccess()) {
                updateOrderStatusToBeDelivered(order_id);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void updateOrderStatusToBeDelivered(int order_id) throws IOException {
        DeliverOrderRequest req = new DeliverOrderRequest();
        req.setStatus("DELIV");

        java.text.DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        req.setPickedup(utcFormat.format(Calendar.getInstance().getTime()));

        Call<Order> deliverOrderCall = apiInterface.deliverOrder(order_id, req);
        Response<Order> orderResponse = deliverOrderCall.execute();
        if (orderResponse.isSuccess()) {
            Intent i = new Intent(Actions.DRIVER_DELIVER_ORDER_DONE);
            sendBroadcast(i);
        } else {
            logRetrofitError(orderResponse);
        }
    }

    private void updateOrderStatus(int order_id, int store_id, String orderStatus) {
        UpdateOrderStatusRequest request = new UpdateOrderStatusRequest();
        request.setStatus(orderStatus);
        request.setOrder(order_id);
        request.setStore(store_id);
        Call<UpdateOrderStatusResponse> call = apiInterface.updateOrderStatus(request);
        try {
            Response<UpdateOrderStatusResponse> response = call.execute();
            if (response.isSuccess()) {
                Intent i = new Intent(Actions.UPDATE_DRIVER_ORDER_STATUS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateStoreAndUserBalance(double amount, int user_id, int driver_id, int store_id) {
        UpdateUserBalanceUpdateRequest request = new UpdateUserBalanceUpdateRequest();
        request.setAmount(amount);
        request.setGivenBy(driver_id);
        request.setStore(store_id);

        Call<UpdateUserBalanceUpdateResponse> call = apiInterface.updateStoreBalance(request);
        try {
            Response<UpdateUserBalanceUpdateResponse> response = call.execute();
            if (response.isSuccess()) {
                updateUserAmount(user_id, request, response);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void updateUserAmount(int user_id, UpdateUserBalanceUpdateRequest request, Response<UpdateUserBalanceUpdateResponse> response) throws IOException {
        request.setStore(null);
        request.setUser(user_id);
        Call<UpdateUserBalanceUpdateResponse> updateUserAmountCall = apiInterface.updateUserBalance(request);
        Response<UpdateUserBalanceUpdateResponse> updateUserAmountResponse = updateUserAmountCall.execute();
        if (updateUserAmountResponse.isSuccess()) {
            Intent i = new Intent(Actions.UPDATE_STORE_AND_USER_BALANCE_DONE);
            sendBroadcast(i);
        } else {
            logRetrofitError(response);
        }
    }

    private void getDriverOrder(int orderID) {
        Call<DriverOrder> call = apiInterface.getDriverOrder(orderID);
        try {
            Response<DriverOrder> response = call.execute();
            if (response.isSuccess()) {
                DriverOrder order = response.body();
                AppData.getInstance(getApplicationContext()).current_driver_order = order;
                Intent i = new Intent(Actions.GET_DRIVER_ORDER_DONE);
                sendBroadcast(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void acceptOrderByDriver(int driverID, int orderID) {
        AcceptOrderByDriverRequest request = new AcceptOrderByDriverRequest();
        request.setDriver(driverID);
        request.setStatus("DOTW");
        Call<Order> call = apiInterface.acceptOrderByDriver(orderID, request);
        try {
            Response<Order> response = call.execute();
            if (response.isSuccess()) {
                Order order = response.body();
                Intent i = new Intent(Actions.ACCEPT_ORDER_BY_DRIVER_DONE);
                sendBroadcast(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getDriverLocation(int driverID) {
        Call<DriverLocation[]> call = apiInterface.getCurrentDriverLocation(driverID);
        try {
            Response<DriverLocation[]> response = call.execute();
            if (response.isSuccess()) {
                DriverLocation[] driverLocations = response.body();
                Gson gson = new Gson();
                String currentDriverLocation = gson.toJson(driverLocations[0]);
                Intent i = new Intent();
                i.setAction(Actions.GET_DRIVER_LOCATION_DONE);
                i.putExtra(Constants.DRIVER_LOCATION, currentDriverLocation);
                sendBroadcast(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void updateImage(Intent intent) {
        String file = intent.getStringExtra("photo");
        if (file != null) {
            updateProfilePicture(file);
        }
    }

    private void placeOrder() {
        String placeOrderJsonStr = AppCache.cache.get(Actions.PLACE_ORDER);
        Gson gson = new Gson();
        PlaceOrderRequest placeOrderRequest = gson.fromJson(placeOrderJsonStr, PlaceOrderRequest.class);
        placeOrder(placeOrderRequest);
    }

    private void getAllOrders(String user_type) {
        Log.d("Getting all order user type: ", user_type);
        Call<Order[]> call = null;
        int userId = AppData.getInstance(getApplicationContext()).user_id;
        if (user_type.equals(Constants.CUSTOMER)) {
            call = apiInterface.getCustomerOrders(true, true, true, userId);
        } else if (user_type.equals(Constants.STORE)) {
            call = apiInterface.getStoreOwnerOrders(true, true, userId);
        } else if (user_type.equals(Constants.DRIVER)) {
//            call = apiInterface.getDriverOrders(true, true, userId, true, true);
            getDriverOrderItems(userId);
        }
        if (call != null)
            try {
                Response<Order[]> response = call.execute();
                if (response.isSuccess()) {
                    Order[] orders = response.body();
                    AppData.getInstance(getApplicationContext()).orders = orders;
                    Intent i = new Intent(Actions.GET_ALL_ORDERS_DONE);
                    sendBroadcast(i);
                } else {
                    logRetrofitError(response);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void getDriverOrderItems(int userId) {
        Call<DriverOrder[]> call = apiInterface.getDriverOrders(true, true, userId, true, true);
        try {
            Response<DriverOrder[]> response = call.execute();
            if (response.isSuccess()) {
                AppData.getInstance(getApplicationContext()).driverOrders = response.body();
                Intent i = new Intent(Actions.GET_ALL_ORDERS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateProfilePicture(String file) {
        File photoFile = new File(file);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), photoFile);
        Call<User> call = apiInterface.updateProfilePicture(requestBody, AppData.getInstance(getApplicationContext()).user_id);
        try {
            Response<User> response = call.execute();
            if (response.isSuccess()) {
                User user = response.body();
                AppData.getInstance(getApplicationContext()).user = user;

                Intent i = new Intent();
                i.setAction(Actions.UPDATE_PROFILE_IMAGE_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAppSettings() {
        Call<AppSettings[]> call = apiInterface.getAppSettings();
        try {
            Response<AppSettings[]> response = call.execute();
            if (response.isSuccess()) {
                Log.d("settings: ", response.body().length + "");
                AppData.getInstance(getApplicationContext()).appSettings = response.body();
                Intent i = new Intent(Actions.GET_APP_SETTINGS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addNewLocation(String newLocation) {
        Gson gson = new Gson();
        Location location = gson.fromJson(newLocation, Location.class);
        Call<Location> call = apiInterface.createNewLocation(location);
        try {
            Response<Location> response = call.execute();
            if (response.isSuccess()) {
                Location newlocation = response.body();
                Log.d("new location id: ", newlocation.getId() + "");
                Intent i = new Intent(Actions.ADD_NEW_LOCATION_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void placeOrder(PlaceOrderRequest placeOrderRequest) {
        Call<PlaceOrderResponse> call = apiInterface.placeOrder(placeOrderRequest);
        try {
            Response<PlaceOrderResponse> response = call.execute();
            if (response.isSuccess()) {
                PlaceOrderResponse placeOrderResponse = response.body();
                Log.d("Display_Id: ", placeOrderResponse.getDisplayId());
                Gson gson = new Gson();
                AppCache.cache.put(Actions.PLACE_ORDER_DONE, gson.toJson(placeOrderResponse));
                Intent i = new Intent(Actions.PLACE_ORDER_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getUserInfo() {
        int user_id = AppData.getInstance(getApplicationContext()).user_id;
        Call<User> call = apiInterface.getUserInfo(user_id);
        String user_type = AppData.getInstance(getApplicationContext()).user_type;
        if (user_type != null)
            if (user_type.equals("driver")) {
                call = apiInterface.getDriverInfo(user_id);
            } else if (user_type.equals("store_owner")) {
                call = apiInterface.getStoreOwnerInfo(user_id);
            }

        try {
            Response<User> response = call.execute();
            if (response.isSuccess()) {
                AppData.getInstance(getApplicationContext()).user = response.body();
                Log.d("User email: ", response.body().getEmail());
                Intent i = new Intent(Actions.GET_USER_INFO_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllStoresItems(int store_id) {
        Call<StoreItem[]> call;
        if (store_id == -1)
            call = apiInterface.getAllStoreItems();
        else
            call = apiInterface.getAllStoreItems(store_id);
        try {
            Response<StoreItem[]> response = call.execute();
            if (response.isSuccess()) {
                StoreItem allStoresItems[] = response.body();
                if (store_id == -1)
                    AppData.getInstance(getApplicationContext()).allStoresItems = allStoresItems;
                else
                    AppData.getInstance(getApplicationContext()).specificStoreItems = allStoresItems;
                Intent i = new Intent(Actions.GET_ALL_STORE_ITEMS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getAllStores() {
        Call<Store[]> call = apiInterface.getAllStores();
        try {
            Response<Store[]> response = call.execute();
            if (response.isSuccess()) {
                Log.d("all stores: ", response.body().length + "");
                AppData.getInstance(getApplicationContext()).allStores = response.body();
                Intent i = new Intent(Actions.GET_ALL_STORES_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllFavouriteStores() {
        Call<Store[]> call = apiInterface.getAllFavouriteStores(true);
        try {
            Response<Store[]> response = call.execute();
            if (response.isSuccess()) {
                Log.d("all favourite stores: ", response.body().length + "");
                AppData.getInstance(getApplicationContext()).fvrStores = response.body();
                Intent i = new Intent(Actions.GET_ALL_FAVOURITE_STORES_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllNotification() {
        int user_id = AppData.getInstance(getApplicationContext()).user_id;
        Call<UserNotification[]> call = apiInterface.getAllNotification(user_id);
        try {
            Response<UserNotification[]> response = call.execute();
            if (response.isSuccess()) {
                Log.d("all notification: ", response.body().length + "");
                AppData.getInstance(getApplicationContext()).allNotifications = response.body();
                Intent i = new Intent(Actions.GET_ALL_NOTIFICATIONS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getOrdersHistory() {
        int user_id = AppData.getInstance(getApplicationContext()).user_id;
        String user_type = AppData.getInstance(getApplicationContext()).user_type;
        Call<OrdersHistory[]> call = apiInterface.getUserOrdersHistory(user_id);
        if (user_type != null)
            if (user_type.equals("store_owner")) {
                int storeID = AppData.getInstance(getApplicationContext()).store_id;
                call = apiInterface.getStoreOwnerOrdersHistory(storeID);
            } else if (user_type.equals("driver")) {
                call = apiInterface.getDriverOrdersHistory(user_id);
            }
        try {
            Response<OrdersHistory[]> response = null;
            if (call != null) {
                response = call.execute();
            }
            if (response != null) {
                if (response.isSuccess()) {
                    Log.d("all orders history: ", response.body().length + "");
                    AppData.getInstance(getApplicationContext()).allOrdersHistory = response.body();
                    Intent i = new Intent(Actions.GET_ALL_ORDERS_HISTORY_DONE);
                    sendBroadcast(i);
                } else {
                    logRetrofitError(response);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getAllFavoriteItems() {
        try {
            Call<FavoriteItem[]> call = apiInterface.getFavoriteItems();
            Response<FavoriteItem[]> response = call.execute();
            if (response.isSuccess()) {
                Log.d("All favorite items", response.body().length + "");
                AppData.getInstance(getApplicationContext()).favoriteItems = response.body();
                Intent i = new Intent(Actions.GET_ALL_FAVORITE_ITEMS_DONE);
                sendBroadcast(i);
            } else {
                logRetrofitError(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logRetrofitError(Response<?> response) {
        Log.d("Retrofit Error", response.code() + response.message());
    }

    public void getOrderStatus(int orderId) {
        Call<OrderStatus> call = apiInterface.getOrderStatus(orderId);
        Response<OrderStatus> response = null;
        try {
            response = call.execute();
            if (response.isSuccess()) {
                OrderStatus orderStatus = response.body();
                Gson gson = new Gson();
                String orderStatusStr = gson.toJson(orderStatus);
                AppData.getInstance(getApplicationContext()).orderStatus = orderStatus;
                Intent i = new Intent(Actions.GET_ORDER_STATUS_DONE);
                i.putExtra(Constants.ORDER_STATUS, orderStatusStr);
                sendBroadcast(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
