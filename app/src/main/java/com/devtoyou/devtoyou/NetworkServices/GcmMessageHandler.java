package com.devtoyou.devtoyou.NetworkServices;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.devtoyou.devtoyou.R;
import com.devtoyou.devtoyou.Activities.TabMenuActivity;
import com.devtoyou.devtoyou.models.AppData;
import com.devtoyou.devtoyou.models.Constants;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by motaz on 19/01/16.
 */
public class GcmMessageHandler extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
//        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        Log.d("GCM: " + from, message);
        AppData.getInstance(getApplicationContext()).init();
        createNotification("نوصل لك", message);
    }

    // Creates notification based on title and body received
    private void createNotification(String title, String body) {
        Context context = getBaseContext();

        Intent i = new Intent(this, TabMenuActivity.class);
        i.putExtra(Constants.FROM_NOTIFICATIONS, true);
        i.setAction(Constants.FROM_NOTIFICATIONS);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo).setContentTitle(title)
                .setContentText(body)
                .setContentIntent(PendingIntent.getActivity(this, 0, i, 0));
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = mBuilder.build();

        mNotificationManager.notify(1, notification);
    }

}
